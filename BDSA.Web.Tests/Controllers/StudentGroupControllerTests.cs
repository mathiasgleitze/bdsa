﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MockQueryable.Moq;
using Moq;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Web.Tests.Controllers
{
    public class StudentGroupControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(StudentGroupController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task GetStudents_returns_dtos()
        {
            var studentDto = new StudentDTO { Id=1};
           
            var allStudent = new[] { studentDto }.AsQueryable().BuildMock();
            
            var repository = new Mock<IStudentGroupRepository>();
           
            repository.Setup(s => s.FindStudents(1)).Returns(allStudent.Object);

            var controller = new StudentGroupController(repository.Object);
            var result = await controller.GetStudents(1);
            var students = result.Value;

            Assert.Equal(studentDto, students.FirstOrDefault());
            
        }

        [Fact]
        public async Task GetGroups_returns_dtos()
        {
            var groupDTO = new GroupDTO { Id = 1 };
            var allGroup = new[] { groupDTO }.AsQueryable().BuildMock();
            var repository = new Mock<IStudentGroupRepository>();
            repository.Setup(s => s.FindGroups(1)).Returns(allGroup.Object);
            var controller = new StudentGroupController(repository.Object);
            var result = await controller.GetGroups(1);
            var groups = result.Value;
            Assert.Equal(groupDTO, groups.FirstOrDefault());
        }

        [Fact]
        public async Task Get_given_existing_id_returns_dto()
        {
            var dto = new StudentGroupDTO();
            var repository = new Mock<IStudentGroupRepository>();
            repository.Setup(s => s.FindAsync(1, 2)).ReturnsAsync(dto);

            var controller = new StudentGroupController(repository.Object);

            var get = await controller.Get(1, 2);

            Assert.Equal(dto, get.Value);
        }

        [Fact]
        public async Task Get_given_non_existing_id_returns_NotFound()
        {
            var repository = new Mock<IStudentGroupRepository>();

            var controller = new StudentGroupController(repository.Object);

            var get = await controller.Get(1, 2);

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new StudentGroupDTO();
            var output = new StudentGroupDTO { GroupId = 1, StudentId = 1 };
            var repository = new Mock<IStudentGroupRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(output);

            var controller = new StudentGroupController(repository.Object);

            var post = await controller.Post(input);

            var result = post as CreatedAtActionResult;

            Assert.Equal("Get", result.ActionName);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public async Task Post_given_dto_creates_studentgroup()
        {
            var repository = new Mock<IStudentGroupRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<StudentGroupDTO>())).ReturnsAsync(new StudentGroupDTO());

            var controller = new StudentGroupController(repository.Object);

            var dto = new StudentGroupDTO();

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }

        [Fact]
        public async Task Delete_given_groups_deletes_studentgroup()
        {
            var repository = new Mock<IStudentGroupRepository>();

            var controller = new StudentGroupController(repository.Object);

            await controller.Delete(3, 1);

            repository.Verify(s => s.DeleteAsync(3, 1));
        }

        [Fact]
        public async Task Delete_given_id_exists_returns_NoContent()
        {
            var repository = new Mock<IStudentGroupRepository>();
            repository.Setup(s => s.DeleteAsync(3, 1)).ReturnsAsync(true);
            var controller = new StudentGroupController(repository.Object);

            var delete = await controller.Delete(3, 1);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Delete_given_id_not_exists_returns_NotFoundResult()
        {
            var repository = new Mock<IStudentGroupRepository>();

            var controller = new StudentGroupController(repository.Object);

            var delete = await controller.Delete(1, 1);

            Assert.IsType<NotFoundResult>(delete);
        }
    }
}
