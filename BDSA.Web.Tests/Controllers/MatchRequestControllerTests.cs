﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Web.Tests.Controllers
{
    public class MatchRequestControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(MatchRequestController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task GetFrom_returns_dtos()
        {
            var dto = new MatchRequestDTO { FromGroup = 1, ToGroup = 2 };
            var all = new[] { dto }.AsQueryable().BuildMock();
            var repository = new Mock<IMatchRequestRepository>();
            repository.Setup(s => s.ReadFrom(1)).Returns(all.Object);

            var controller = new MatchRequestController(repository.Object);
            var result = await controller.GetFrom(1);
            

            Assert.Equal(dto, result.Value.FirstOrDefault());
        }

        [Fact]
        public async Task GetTo_returns_dtos()
        {
            var dto = new MatchRequestDTO { FromGroup = 2, ToGroup = 1 };
            var all = new[] { dto }.AsQueryable().BuildMock();
            var repository = new Mock<IMatchRequestRepository>();
            repository.Setup(s => s.ReadTo(1)).Returns(all.Object);

            var controller = new MatchRequestController(repository.Object);
            var result = await controller.GetTo(1);


            Assert.Equal(dto, result.Value.FirstOrDefault());
        }
        [Fact]
        public async Task Get_given_existing_id_returns_dto()
        {
            var dto = new MatchRequestDTO();
            var repository = new Mock<IMatchRequestRepository>();
            repository.Setup(s => s.FindAsync(1, 2)).ReturnsAsync(dto);

            var controller = new MatchRequestController(repository.Object);

            var get = await controller.Get(1, 2);

            Assert.Equal(dto, get.Value);
        }
        [Fact]
        public async Task Get_given_non_existing_id_returns_NotFound()
        {
            var repository = new Mock<IMatchRequestRepository>();

            var controller = new MatchRequestController(repository.Object);

            var get = await controller.Get(1, 2);

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new MatchRequestDTO();
            var output = new MatchRequestDTO { FromGroup = 1, ToGroup = 2};
            var repository = new Mock<IMatchRequestRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(output);

            var controller = new MatchRequestController(repository.Object);

            var post = await controller.Post(input);

            var response = post.Result;

            Assert.IsType<CreatedAtActionResult>(response);
        }

        [Fact]
        public async Task Post_given_dto_creates_matchrequest()
        {
            var repository = new Mock<IMatchRequestRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<MatchRequestDTO>())).ReturnsAsync(new MatchRequestDTO());

            var controller = new MatchRequestController(repository.Object);

            var dto = new MatchRequestDTO();

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }
        [Fact]
        public async Task Delete_given_groups_deletes_matchrequest()
        {
            var repository = new Mock<IMatchRequestRepository>();

            var controller = new MatchRequestController(repository.Object);

            await controller.Delete(3, 1);

            repository.Verify(s => s.DeleteAsync(3, 1));
        }
        [Fact]
        public async Task Delete_returns_NoContent()
        {
            var repository = new Mock<IMatchRequestRepository>();
            repository.Setup(s => s.DeleteAsync(3, 1)).ReturnsAsync(true);
            var controller = new MatchRequestController(repository.Object);

            var delete = await controller.Delete(3, 1);

            Assert.IsType<NoContentResult>(delete);
        }
        [Fact]
        public async Task DeleteAll_returns_NoContent()
        {
            var repository = new Mock<IMatchRequestRepository>();
            repository.Setup(s => s.DeleteAll(1)).ReturnsAsync(true);
            var controller = new MatchRequestController(repository.Object);

            var delete = await controller.DeleteAll(1);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task DeleteAll_returns_Notfound()
        {
            var repository = new Mock<IMatchRequestRepository>();
            repository.Setup(s => s.DeleteAll(1)).ReturnsAsync(false);
            var controller = new MatchRequestController(repository.Object);

            var delete = await controller.DeleteAll(1);

            Assert.IsType<NotFoundResult>(delete);
        }
    }
}
