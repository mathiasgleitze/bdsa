﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Microsoft.AspNetCore.Authorization;

namespace BDSA.Web.Tests.Controllers
{
    public class ChatControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(ChatController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task Get_given_id_exists_returns_dto()
        {
            var id = 1;
            var dto = new ChatDTO();
            var repository = new Mock<IChatRepository>();
            repository.Setup(s => s.FindAsync(id)).ReturnsAsync(dto);

            var controller = new ChatController(repository.Object);

            var get = await controller.Get(id);

            Assert.Equal(dto, get.Value);
        }

        [Fact]
        public async Task Get_given_id_not_exists_returns_not_found()
        {
            var id = 1;
            var repository = new Mock<IChatRepository>();
            var controller = new ChatController(repository.Object);

            var get = await controller.Get(id);

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_creates_chat()
        {
            var repository = new Mock<IChatRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<ChatDTO>())).ReturnsAsync(new ChatDTO());

            var controller = new ChatController(repository.Object);

            var dto = new ChatDTO();

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new ChatDTO();
            var output = new ChatDTO { Id = 1 };
            var repository = new Mock<IChatRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(output);

            var controller = new ChatController(repository.Object);

            var post = await controller.Post(input);

            //var result = post as CreatedAtActionResult;

            //Assert.Equal("Get", result.ActionName);
            //Assert.Equal(output, result.Value);
        }

        [Fact]
        public async Task Delete_given_id_exists_deletes_chat()
        {
            var repository = new Mock<IChatRepository>();
            
            var controller = new ChatController(repository.Object);

            await controller.Delete(1);

            repository.Verify(s => s.DeleteAsync(1));
        }

        [Fact]
        public async Task Delete_given_id_exists_returns_NoContentResult()
        {
            var repository = new Mock<IChatRepository>();
            repository.Setup(s => s.DeleteAsync(1)).ReturnsAsync(true);

            var controller = new ChatController(repository.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Delete_given_id_not_exists_returns_NotFoundResult()
        {
            var repository = new Mock<IChatRepository>();
            
            var controller = new ChatController(repository.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NotFoundResult>(delete);
        }
    }
}
