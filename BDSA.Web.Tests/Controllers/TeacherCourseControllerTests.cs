﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MockQueryable.Moq;
using Moq;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Web.Tests.Controllers
{
    public class TeacherCourseControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(TeacherCourseController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task Get_Courses_returns_dtos()
        {
            var courseDTO = new CourseDTO { Id = 1 };
            var allCourses = new[] { courseDTO }.AsQueryable().BuildMock();
            var repository = new Mock<ITeacherCourseRepository>();
            repository.Setup(s => s.FindCourses(1)).Returns(allCourses.Object);

            var controller = new TeacherCourseController(repository.Object);
            var result = await controller.GetCourses(1);
            var courses = result.Value;

            Assert.Equal(courseDTO, courses.FirstOrDefault());
        }
       
        [Fact]
        public async Task Get_Teachers_returns_dtos()
        {
            var teacherDTO = new TeacherDTO { Id = 1 };
            var allTeachers = new[] { teacherDTO }.AsQueryable().BuildMock();
            var repository = new Mock<ITeacherCourseRepository>();
            repository.Setup(s => s.FindTeachers(1)).Returns(allTeachers.Object);

            var controller = new TeacherCourseController(repository.Object);
            var result = await controller.GetTeachers(1);
            var teachers = result.Value;

            Assert.Equal(teacherDTO,teachers.FirstOrDefault());
        }

        [Fact]
        public async Task Get_given_nothing_returns_dtos()
        {
            var dto = new TeacherCourseDTO { TeacherId = 1, CourseId = 2 };
            var all = new[] { dto }.AsQueryable().BuildMock();
            var repository = new Mock<ITeacherCourseRepository>();
            repository.Setup(s => s.Read()).Returns(all.Object);

            var controller = new TeacherCourseController(repository.Object);
            var result = await controller.Get();
            
            Assert.Equal(dto, result.Value.FirstOrDefault());
        }

        [Fact]
        public async Task Get_given_existing_id_returns_dto()
        {
            var dto = new TeacherCourseDTO();
            var repository = new Mock<ITeacherCourseRepository>();
            repository.Setup(s => s.FindAsync(1, 2)).ReturnsAsync(dto);

            var controller = new TeacherCourseController(repository.Object);

            var get = await controller.Get(1, 2);

            Assert.Equal(dto, get.Value);
        }

        [Fact]
        public async Task Get_given_non_existing_id_returns_NotFound()
        {
            var repository = new Mock<ITeacherCourseRepository>();

            var controller = new TeacherCourseController(repository.Object);

            var get = await controller.Get(1, 2);

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new TeacherCourseDTO();
            var output = new TeacherCourseDTO { CourseId = 1, TeacherId = 1};
            var repository = new Mock<ITeacherCourseRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(output);

            var controller = new TeacherCourseController(repository.Object);

            var post = await controller.Post(input);

            var result = post as CreatedAtActionResult;

            Assert.Equal("Get", result.ActionName);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public async Task Post_given_dto_creates_teachercourse()
        {
            var repository = new Mock<ITeacherCourseRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<TeacherCourseDTO>())).ReturnsAsync(new TeacherCourseDTO());

            var controller = new TeacherCourseController(repository.Object);

            var dto = new TeacherCourseDTO();

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }

        [Fact]
        public async Task Delete_given_groups_deletes_teachercourse()
        {
            var repository = new Mock<ITeacherCourseRepository>();

            var controller = new TeacherCourseController(repository.Object);

            await controller.Delete(3, 1);

            repository.Verify(s => s.DeleteAsync(3, 1));
        }
        [Fact]
        public async Task Delete_given_id_exists_returns_NoContent()
        {
            var repository = new Mock<ITeacherCourseRepository>();
            repository.Setup(s => s.DeleteAsync(3, 1)).ReturnsAsync(true);
            var controller = new TeacherCourseController(repository.Object);

            var delete = await controller.Delete(3, 1);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Delete_given_id_not_exists_returns_NotFoundResult()
        {
            var repository = new Mock<ITeacherCourseRepository>();

            var controller = new TeacherCourseController(repository.Object);

            var delete = await controller.Delete(1, 1);

            Assert.IsType<NotFoundResult>(delete);
        }
    }
}
