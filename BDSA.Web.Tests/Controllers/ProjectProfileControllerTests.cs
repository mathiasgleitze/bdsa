﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Web.Tests.Controllers
{
    public class ProjectProfileControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(ProjectProfileController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task Get_given_id_exists_returns_dto()
        {
            var studentId = 1;
            var projectId = 3;
            var dto = new ProjectProfileDTO();
            var repository = new Mock<IProjectProfileRepository>();
            repository.Setup(s => s.FindAsync(studentId, projectId)).ReturnsAsync(dto);

            var controller = new ProjectProfileController(repository.Object);

            var get = await controller.Get(studentId, projectId);

            Assert.Equal(dto, get.Value);
        }

        [Fact]
        public async Task Get_given_id_not_exists_returns_not_found()
        {
            var studentId = 1;
            var projectId = 2;
            var repository = new Mock<IProjectProfileRepository>();
            var controller = new ProjectProfileController(repository.Object);

            var get = await controller.Get(studentId, projectId);

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_creates_projectProfile()
        {
            var dto = new ProjectProfileDTO();
            var repository = new Mock<IProjectProfileRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<ProjectProfileDTO>())).ReturnsAsync(new ProjectProfileDTO());

            var controller = new ProjectProfileController(repository.Object);

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new ProjectProfileDTO();
            var output = new ProjectProfileDTO { StudentId = 1, ProjectId = 2};
            var repository = new Mock<IProjectProfileRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(output);

            var controller = new ProjectProfileController(repository.Object);

            var post = await controller.Post(input);

            var result = post as CreatedAtActionResult;

            Assert.Equal("Get", result.ActionName);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public async Task Put_given_dto_exists_updates_projectProfile()
        {
            var dto = new ProjectProfileDTO();
            var studentId = 1;
            var projectId = 2;
            var repository = new Mock<IProjectProfileRepository>();
            var controller = new ProjectProfileController(repository.Object);

            await controller.Put(studentId, projectId, dto);

            repository.Verify(s => s.UpdateAsync(dto));
        }

        [Fact]
        public async Task Put_given_dto_exists_returns_NoContentResult()
        {
            var dto = new ProjectProfileDTO();
            var studentId = 1;
            var projectId = 2;
            var repository = new Mock<IProjectProfileRepository>();
            repository.Setup(s => s.UpdateAsync(dto)).ReturnsAsync(true);

            var controller = new ProjectProfileController(repository.Object);

            var put = await controller.Put(studentId, projectId, dto);

            Assert.IsType<NoContentResult>(put);
        }

        [Fact]
        public async Task Put_given_dto_not_exists_returns_NotFoundResult()
        {
            var dto = new ProjectProfileDTO();
            var studentId = 1;
            var projectId = 2;
            var repository = new Mock<IProjectProfileRepository>();

            var controller = new ProjectProfileController(repository.Object);

            var put = await controller.Put(studentId, projectId, dto);

            Assert.IsType<NotFoundResult>(put);
        }

        [Fact]
        public async Task Delete_given_id_exists_deletes_projectProfile()
        {
            var repository = new Mock<IProjectProfileRepository>();

            var controller = new ProjectProfileController(repository.Object);

            await controller.Delete(1, 2);

            repository.Verify(s => s.DeleteAsync(1, 2));
        }

        [Fact]
        public async Task Delete_given_id_exists_returns_NoContentResult()
        {
            var repository = new Mock<IProjectProfileRepository>();
            repository.Setup(s => s.DeleteAsync(1, 2)).ReturnsAsync(true);

            var controller = new ProjectProfileController(repository.Object);

            var delete = await controller.Delete(1, 2);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Delete_given_id_not_exists_returns_NotFoundResult()
        {
            var repository = new Mock<IProjectProfileRepository>();

            var controller = new ProjectProfileController(repository.Object);

            var delete = await controller.Delete(1, 2);

            Assert.IsType<NotFoundResult>(delete);
        }
    }
}
