﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Web.Tests.Controllers
{ 
    public class TeacherControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(TeacherController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task Get_given_id_exists_returns_dto()
        {
            var id = 1;
            var dto = new TeacherDTO();
            var repository = new Mock<ITeacherRepository>();
            repository.Setup(s => s.FindAsync(id)).ReturnsAsync(dto);

            var controller = new TeacherController(repository.Object);

            var get = await controller.Get(id);

            Assert.Equal(dto, get.Value);
        }

        [Fact]
        public async Task Get_given_id_not_exists_returns_not_found()
        {
            var id = 1;
            var repository = new Mock<ITeacherRepository>();
            var controller = new TeacherController(repository.Object);

            var get = await controller.Get(id);

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_creates_teacher()
        {
            var dto = new TeacherDTO();
            var repository = new Mock<ITeacherRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<TeacherDTO>())).ReturnsAsync(new TeacherDTO());

            var controller = new TeacherController(repository.Object);

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new TeacherDTO();
            var output = new TeacherDTO { Id = 1 };
            var repository = new Mock<ITeacherRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(output);

            var controller = new TeacherController(repository.Object);

            var post = await controller.Post(input);

            var result = post as CreatedAtActionResult;

            Assert.Equal("Get", result.ActionName);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public async Task Put_given_dto_exists_updates_teacher()
        {
            var dto = new TeacherDTO();
            var id = 1;
            var repository = new Mock<ITeacherRepository>();
            var controller = new TeacherController(repository.Object);

            await controller.Put(id, dto);

            repository.Verify(s => s.UpdateAsync(dto));
        }

        [Fact]
        public async Task Put_given_dto_exists_returns_NoContentResult()
        {
            var dto = new TeacherDTO();
            var id = 1;
            var repository = new Mock<ITeacherRepository>();
            repository.Setup(s => s.UpdateAsync(dto)).ReturnsAsync(true);

            var controller = new TeacherController(repository.Object);

            var put = await controller.Put(id, dto);

            Assert.IsType<NoContentResult>(put);
        }

        [Fact]
        public async Task Put_given_dto_not_exists_returns_NotFoundResult()
        {
            var dto = new TeacherDTO();
            var id = 1;
            var repository = new Mock<ITeacherRepository>();

            var controller = new TeacherController(repository.Object);

            var put = await controller.Put(id, dto);

            Assert.IsType<NotFoundResult>(put);
        }

        [Fact]
        public async Task Delete_given_id_exists_deletes_teacher()
        {
            var repository = new Mock<ITeacherRepository>();

            var controller = new TeacherController(repository.Object);

            await controller.Delete(1);

            repository.Verify(s => s.DeleteAsync(1));
        }

        [Fact]
        public async Task Delete_given_id_exists_returns_NoContentResult()
        {
            var repository = new Mock<ITeacherRepository>();
            repository.Setup(s => s.DeleteAsync(1)).ReturnsAsync(true);

            var controller = new TeacherController(repository.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Delete_given_id_not_exists_returns_NotFoundResult()
        {
            var repository = new Mock<ITeacherRepository>();

            var controller = new TeacherController(repository.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NotFoundResult>(delete);
        }
    }
}
