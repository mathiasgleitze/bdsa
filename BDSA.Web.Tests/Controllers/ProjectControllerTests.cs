﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Web.Tests.Controllers
{
    public class ProjectControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(ProjectController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task Get_given_existing_id_returns_dto()
        {
            var dto = new ProjectDTO { Id = 1 };
            var repository = new Mock<IProjectRepository>();
            repository.Setup(s => s.FindAsync(dto.Id)).ReturnsAsync(dto);

            var controller = new ProjectController(repository.Object);

            var get = await controller.Get(dto.Id);

            Assert.Equal(dto, get.Value);
        }

        [Fact]
        public async Task Get_given_non_existing_id_returns_not_found()
        {
            var repository = new Mock<IProjectRepository>();

            var controller = new ProjectController(repository.Object);

            var get = await controller.Get(1);

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_creates_project()
        {
            var dto = new ProjectDTO();
            var repository = new Mock<IProjectRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<ProjectDTO>())).ReturnsAsync(dto);

            var controller = new ProjectController(repository.Object);

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new ProjectDTO();
            var output = new ProjectDTO { Id = 1 };
            var repository = new Mock<IProjectRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(output);

            var controller = new ProjectController(repository.Object);

            var post = await controller.Post(input);

            var result = post as CreatedAtActionResult;

            Assert.Equal("Get", result.ActionName);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public async Task Put_given_existing_dto_updates_project()
        {
            var dto = new ProjectDTO { Id = 1 };
            var repository = new Mock<IProjectRepository>();

            var controller = new ProjectController(repository.Object);

            await controller.Put(dto.Id, dto);

            repository.Verify(s => s.UpdateAsync(dto));
        }

        [Fact]
        public async Task Put_given_existing_dto_returns_no_content()
        {
            var dto = new ProjectDTO { Id = 1 };
            var repository = new Mock<IProjectRepository>();
            repository.Setup(s => s.UpdateAsync(dto)).ReturnsAsync(true);

            var controller = new ProjectController(repository.Object);

            var put = await controller.Put(dto.Id, dto);

            Assert.IsType<NoContentResult>(put);
        }

        [Fact]
        public async Task Put_given_non_existing_dto_returns_no_content()
        {
            var dto = new ProjectDTO { Id = 1 };
            var repository = new Mock<IProjectRepository>();
            
            var controller = new ProjectController(repository.Object);

            var put = await controller.Put(dto.Id, dto);

            Assert.IsType<NotFoundResult>(put);
        }

        [Fact]
        public async Task Delete_given_existing_dto_updates_project()
        {
            var repository = new Mock<IProjectRepository>();

            var controller = new ProjectController(repository.Object);

            await controller.Delete(1);

            repository.Verify(s => s.DeleteAsync(1));
        }

        [Fact]
        public async Task Delete_given_existing_dto_returns_no_content()
        {
            var repository = new Mock<IProjectRepository>();
            repository.Setup(s => s.DeleteAsync(1)).ReturnsAsync(true);

            var controller = new ProjectController(repository.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Delete_given_non_existing_dto_returns_not_found()
        {
            var repository = new Mock<IProjectRepository>();

            var controller = new ProjectController(repository.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NotFoundResult>(delete);
        }
    }
}
