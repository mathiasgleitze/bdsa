﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Web.Tests.Controllers
{
    public class GroupRequestControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(GroupRequestController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task GetTo_returns_dtos()
        {
            var dto = new GroupRequestDTO { FromGroup = 1, ToGroup = 2 };
            var all = new[] { dto }.AsQueryable().BuildMock();
            var repository = new Mock<IGroupRequestRepository>();
            repository.Setup(s =>  s.ReadToGroup(1)).Returns(all.Object);
            

            var controller = new GroupRequestController(repository.Object);

            var result = await controller.GetTo(1);
            
            var toList = result.Value;

            Assert.Equal(dto, toList.FirstOrDefault());
        }
        [Fact]
        public async Task GetFrom_returns_dtos()
        {
            var dto = new GroupRequestDTO { FromGroup = 1, ToGroup = 2 };
            var all = new[] { dto }.AsQueryable().BuildMock();
            var repository = new Mock<IGroupRequestRepository>();
            repository.Setup(s => s.ReadFromGroup(1)).Returns(all.Object);

            var controller = new GroupRequestController(repository.Object);

            var result = await controller.GetFrom(1);
            var fromList = result.Value;

            Assert.Equal(dto, fromList.FirstOrDefault());
        }
        [Fact]
        public async Task Get_given_existing_id_returns_dto()
        {
            var dto = new GroupRequestDTO();
            var repository = new Mock<IGroupRequestRepository>();
            repository.Setup(s => s.FindAsync(1, 2)).ReturnsAsync(dto);

            var controller = new GroupRequestController(repository.Object);

            var get = await controller.Get(1, 2);

            Assert.Equal(dto, get.Value);
        }
        [Fact]
        public async Task Get_given_non_existing_id_returns_NotFound()
        {
            var repository = new Mock<IGroupRequestRepository>();

            var controller = new GroupRequestController(repository.Object);

            var get = await controller.Get(1, 2);

            Assert.IsType<NotFoundResult>(get.Result);
        }
        [Fact]
        public async Task Post_given_dto_creates_grouprequest()
        {
            var repository = new Mock<IGroupRequestRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<GroupRequestDTO>())).ReturnsAsync(new GroupRequestDTO());

            var controller = new GroupRequestController(repository.Object);

            var dto = new GroupRequestDTO();

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new GroupRequestDTO();
            var output = new GroupRequestDTO{ToGroup = 1, FromGroup = 2};
            var repository = new Mock<IGroupRequestRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(output);

            var controller = new GroupRequestController(repository.Object);

            var post = await controller.Post(input);

            var result = post as CreatedAtActionResult;

            Assert.Equal("Get", result.ActionName);
            Assert.Equal(output, result.Value);
        }
        [Fact]
        public async Task Delete_given_groups_deletes_message()
        {
            var repository = new Mock<IGroupRequestRepository>();

            var controller = new GroupRequestController(repository.Object);

            await controller.Delete(3, 1);

            repository.Verify(s => s.DeleteAsync(3, 1));
        }
        [Fact]
        public async Task Delete_returns_NoContent()
        {
            var repository = new Mock<IGroupRequestRepository>();
            repository.Setup(s => s.DeleteAsync(3, 1)).ReturnsAsync(true);
            var controller = new GroupRequestController(repository.Object);

            var delete = await controller.Delete(3, 1);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Put_given_dto_updates_actor()
        {
            var repository = new Mock<IGroupRequestRepository>();

            var controller = new GroupRequestController(repository.Object);

            var dto = new GroupRequestDTO();

            await controller.Put(1, 2);

            repository.Verify(s => s.UpdateAsync(1, 2));
        }

        [Fact]
        public async Task Put_returns_NoContent()
        {
            var repository = new Mock<IGroupRequestRepository>();
            repository.Setup(s => s.UpdateAsync(3, 1)).ReturnsAsync(true);
            var controller = new GroupRequestController(repository.Object);

            var updated = await controller.Put(3, 1);

            Assert.IsType<NoContentResult>(updated);
        }
        [Fact]
        public async Task Put_returns_NotFound()
        {
            var repository = new Mock<IGroupRequestRepository>();
            repository.Setup(s => s.UpdateAsync(3, 1)).ReturnsAsync(false);
            var controller = new GroupRequestController(repository.Object);

            var updated = await controller.Put(3, 1);

            Assert.IsType<NotFoundResult>(updated);
        }
    }
}
