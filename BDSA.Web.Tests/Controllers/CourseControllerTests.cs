﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Web.Tests.Controllers
{
    public class CourseControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(CourseController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task Get_given_id_exists_returns_dto()
        {
            var id = 1;
            var dto = new CourseDTO();
            var repository = new Mock<ICourseRepository>();
            repository.Setup(s => s.FindAsync(id)).ReturnsAsync(dto);

            var controller = new CourseController(repository.Object);

            var get = await controller.Get(id);

            Assert.Equal(dto, get.Value);
        }

        [Fact]
        public async Task Get_given_id_not_exists_returns_not_found()
        {
            var id = 1;
            var repository = new Mock<ICourseRepository>();
            var controller = new CourseController(repository.Object);

            var get = await controller.Get(id);

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_creates_course()
        {
            var dto = new CourseDTO();
            var repository = new Mock<ICourseRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<CourseDTO>())).ReturnsAsync(new CourseDTO());

            var controller = new CourseController(repository.Object);
            
            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new CourseDTO();
            var output = new CourseDTO { Id = 1 };
            var repository = new Mock<ICourseRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(output);

            var controller = new CourseController(repository.Object);

            var post = await controller.Post(input);

            var result = post as CreatedAtActionResult;

            Assert.Equal("Get", result.ActionName);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public async Task Put_given_dto_exists_updates_course()
        {
            var dto = new CourseDTO();
            var id = 1;
            var repository = new Mock<ICourseRepository>();
            var controller = new CourseController(repository.Object);

            await controller.Put(id, dto);

            repository.Verify(s => s.UpdateAsync(dto));
        }

        [Fact]
        public async Task Put_given_dto_exists_returns_NoContentResult()
        {
            var dto = new CourseDTO();
            var id = 1;
            var repository = new Mock<ICourseRepository>();
            repository.Setup(s => s.UpdateAsync(dto)).ReturnsAsync(true);

            var controller = new CourseController(repository.Object);

            var put = await controller.Put(id, dto);

            Assert.IsType<NoContentResult>(put);
        }

        [Fact]
        public async Task Put_given_dto_not_exists_returns_NotFoundResult()
        {
            var dto = new CourseDTO();
            var id = 1;
            var repository = new Mock<ICourseRepository>();

            var controller = new CourseController(repository.Object);
            
            var put = await controller.Put(id, dto);

            Assert.IsType<NotFoundResult>(put);
        }

        [Fact]
        public async Task Delete_given_id_exists_deletes_course()
        {
            var repository = new Mock<ICourseRepository>();
            
            var controller = new CourseController(repository.Object);

            await controller.Delete(1);

            repository.Verify(s => s.DeleteAsync(1));
        }

        [Fact]
        public async Task Delete_given_id_exists_returns_NoContentResult()
        {
            var repository = new Mock<ICourseRepository>();
            repository.Setup(s => s.DeleteAsync(1)).ReturnsAsync(true);

            var controller = new CourseController(repository.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Delete_given_id_not_exists_returns_NotFoundResult()
        {
            var repository = new Mock<ICourseRepository>();
            
            var controller = new CourseController(repository.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NotFoundResult>(delete);
        }
    }
}
