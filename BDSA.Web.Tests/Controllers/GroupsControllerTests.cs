using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MockQueryable.Moq;
using Moq;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Web.Tests.Controllers
{
    public class GroupControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(GroupController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task Get_given_nothing_return_dtos()
        {
            var dtos = new[]{ new GroupDTO(), new GroupDTO()}.AsQueryable().BuildMock();
            var repository = new Mock<IGroupRepository>();
            repository.Setup(s => s.Read()).Returns(dtos.Object);
            var controller = new GroupController(repository.Object);

            var result = await controller.Get();

            Assert.Equal(0, result.Value.FirstOrDefault().Id);
        }
        [Fact]
        public async Task Get_given_existing_id_returns_dto()
        {
            var dto = new GroupDTO();
            var repository = new Mock<IGroupRepository>();
            repository.Setup(s => s.FindAsync(42)).ReturnsAsync(dto);

            var controller = new GroupController(repository.Object);

            var get = await controller.Get(42);

            Assert.Equal(dto, get.Value);
        }

        [Fact]
        public async Task Get_given_non_existing_id_returns_NotFound()
        {
            var repository = new Mock<IGroupRepository>();

            var controller = new GroupController(repository.Object);

            var get = await controller.Get(1234);

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new GroupDTO();
            var output = true;
            var repository = new Mock<IGroupRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(true);

            var controller = new GroupController(repository.Object);

            var post = await controller.Post(input);

            var result = post as CreatedAtActionResult;

            Assert.Equal("Get", result.ActionName);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public async Task Post_given_dto_creates_group()
        {
            var repository = new Mock<IGroupRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<GroupDTO>())).ReturnsAsync(true);

            var controller = new GroupController(repository.Object);

            var dto = new GroupDTO();

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }

        [Fact]
        public async Task Put_given_dto_updates_group()
        {
            var repository = new Mock<IGroupRepository>();

            var controller = new GroupController(repository.Object);

            var dto = new GroupDTO();

            await controller.Put(42, dto);

            repository.Verify(s => s.UpdateAsync(dto));
        }

        [Fact]
        public async Task Put_returns_NoContent()
        {
            var dto = new GroupDTO();
            var repository = new Mock<IGroupRepository>();
            repository.Setup(s => s.UpdateAsync(dto)).ReturnsAsync(true);
            var controller = new GroupController(repository.Object);

            var put = await controller.Put(42, dto);

            Assert.IsType<NoContentResult>(put);
        }

        [Fact]
        public async Task Put_given_repository_returns_false_returns_NotFound()
        {
            var repository = new Mock<IGroupRepository>();

            var controller = new GroupController(repository.Object);

            var dto = new GroupDTO();

            var put = await controller.Put(42, dto);

            Assert.IsType<NotFoundResult>(put);
        }

        [Fact]
        public async Task Delete_given_id_deletes_group()
        {
            var repository = new Mock<IGroupRepository>();

            var controller = new GroupController(repository.Object);

            await controller.Delete(42);

            repository.Verify(s => s.DeleteAsync(42));
        }

        [Fact]
        public async Task Delete_returns_NoContent()
        {
            var repository = new Mock<IGroupRepository>();
            repository.Setup(s => s.DeleteAsync(42)).ReturnsAsync(true);
            var controller = new GroupController(repository.Object);

            var delete = await controller.Delete(42);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Delete_given_repository_returns_false_returns_NotFound()
        {
            var repository = new Mock<IGroupRepository>();

            var controller = new GroupController(repository.Object);

            var delete = await controller.Delete(42);

            Assert.IsType<NotFoundResult>(delete);
        }
    }
}
