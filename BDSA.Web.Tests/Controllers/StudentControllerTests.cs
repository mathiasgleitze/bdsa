﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using MockQueryable.Moq;
using Microsoft.AspNetCore.Authorization;

namespace BDSA.Web.Tests.Controllers
{
    public class StudentControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(StudentController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task Get_given_id_exists_returns_dto()
        {
            var id = 1;
            var dto = new StudentDTO();
            var repository = new Mock<IStudentRepository>();
            repository.Setup(s => s.FindAsync(id)).ReturnsAsync(dto);

            var controller = new StudentController(repository.Object);

            var get = await controller.Get(id);

            Assert.Equal(dto, get.Value);
        }

        [Fact]
        public async Task Get_given_id_not_exists_returns_not_found()
        {
            var id = 1;
            var repository = new Mock<IStudentRepository>();
            var controller = new StudentController(repository.Object);

            var get = await controller.Get(id);

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task GetName_given_existing_name_returns_ids()
        {
            var dto = new StudentDTO { Id = 1, Name = "Emil" };
            var dtos = new[] { dto }.AsQueryable().BuildMock();
            var repository = new Mock<IStudentRepository>();
            repository.Setup(s => s.FindStudentsByName(dto.Name)).Returns(dtos.Object);

            var controller = new StudentController(repository.Object);

            var get = await controller.GetName(dto.Name);

            Assert.Equal(dto.Id, get.Value.SingleOrDefault().Id);
        }

        [Fact]
        public async Task GetName_given_non_existing_name_returns_not_found()
        {
            var repository = new Mock<IStudentRepository>();

            var controller = new StudentController(repository.Object);

            var get = await controller.GetName("Emil");

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task GetMail_given_existing_mail_returns_id()
        {
            var dto = new StudentDTO { Id = 1, Mail = "emil@itu.dk" };
            var repository = new Mock<IStudentRepository>();
            repository.Setup(s => s.FindIdByMailAsync(dto.Mail)).ReturnsAsync(dto.Id);

            var controller = new StudentController(repository.Object);

            var get = await controller.GetMail(dto.Mail);

            Assert.Equal(dto.Id, get.Value);
        }
        
        [Fact]
        public async Task GetMail_given_non_existing_mail_returns_not_found()
        {
            var repository = new Mock<IStudentRepository>();

            var controller = new StudentController(repository.Object);

            var get = await controller.GetMail("emil@itu.dk");

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_creates_student()
        {
            var dto = new StudentDTO();
            var repository = new Mock<IStudentRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<StudentDTO>())).ReturnsAsync(new StudentDTO());

            var controller = new StudentController(repository.Object);

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new StudentDTO();
            var output = new StudentDTO { Id = 1 };
            var repository = new Mock<IStudentRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(output);

            var controller = new StudentController(repository.Object);

            var post = await controller.Post(input);

            var result = post as CreatedAtActionResult;

            Assert.Equal("Get", result.ActionName);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public async Task Put_given_dto_exists_updates_student()
        {
            var dto = new StudentDTO();
            var id = 1;
            var repository = new Mock<IStudentRepository>();
            var controller = new StudentController(repository.Object);

            await controller.Put(id, dto);

            repository.Verify(s => s.UpdateAsync(dto));
        }

        [Fact]
        public async Task Put_given_dto_exists_returns_NoContentResult()
        {
            var dto = new StudentDTO();
            var id = 1;
            var repository = new Mock<IStudentRepository>();
            repository.Setup(s => s.UpdateAsync(dto)).ReturnsAsync(true);

            var controller = new StudentController(repository.Object);

            var put = await controller.Put(id, dto);

            Assert.IsType<NoContentResult>(put);
        }

        [Fact]
        public async Task Put_given_dto_not_exists_returns_NotFoundResult()
        {
            var dto = new StudentDTO();
            var id = 1;
            var repository = new Mock<IStudentRepository>();

            var controller = new StudentController(repository.Object);

            var put = await controller.Put(id, dto);

            Assert.IsType<NotFoundResult>(put);
        }

        [Fact]
        public async Task Delete_given_id_exists_deletes_student()
        {
            var repository = new Mock<IStudentRepository>();

            var controller = new StudentController(repository.Object);

            await controller.Delete(1);

            repository.Verify(s => s.DeleteAsync(1));
        }

        [Fact]
        public async Task Delete_given_id_exists_returns_NoContentResult()
        {
            var repository = new Mock<IStudentRepository>();
            repository.Setup(s => s.DeleteAsync(1)).ReturnsAsync(true);

            var controller = new StudentController(repository.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Delete_given_id_not_exists_returns_NotFoundResult()
        {
            var repository = new Mock<IStudentRepository>();

            var controller = new StudentController(repository.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NotFoundResult>(delete);
        }
    }
}
