﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MockQueryable.Moq;
using Moq;
using System.Linq;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Web.Tests.Controllers
{
    public class StudentCourseControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(StudentCourseController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task GetStudents_returns_dtos()
        {
            var studentDto = new StudentDTO { Id = 1 };

            var allStudent = new[] { studentDto }.AsQueryable().BuildMock();

            var repository = new Mock<IStudentCourseRepository>();

            repository.Setup(s => s.FindStudents(1)).Returns(allStudent.Object);

            var controller = new StudentCourseController(repository.Object);
            var result = await controller.GetStudents(1);
            var students = result.Value;

            Assert.Equal(studentDto, students.FirstOrDefault());

        }
        [Fact]
        public async Task GetCourses_returns_dtos()
        {
            var groupDTO = new CourseDTO { Id = 1 };
            var allCourse = new[] { groupDTO }.AsQueryable().BuildMock();
            var repository = new Mock<IStudentCourseRepository>();
            repository.Setup(s => s.FindCourses(1)).Returns(allCourse.Object);
            var controller = new StudentCourseController(repository.Object);
            var result = await controller.GetCourses(1);
            var groups = result.Value;
            Assert.Equal(groupDTO, groups.FirstOrDefault());
        }

        [Fact]
        public async Task Get_returns_dtos()
        {
            var dto = new StudentCourseDTO { StudentId = 1, CourseId = 2 };
            var all = new[] { dto }.AsQueryable().BuildMock();
            var repository = new Mock<IStudentCourseRepository>();
            repository.Setup(s => s.Read()).Returns(all.Object);

            var controller = new StudentCourseController(repository.Object);
            var result = await controller.Get();
            

            Assert.Equal(dto, result.Value.FirstOrDefault());
        }

        [Fact]
        public async Task Get_given_existing_id_returns_dto()
        {
            var dto = new StudentCourseDTO();
            var repository = new Mock<IStudentCourseRepository>();
            repository.Setup(s => s.FindAsync(1, 2)).ReturnsAsync(dto);

            var controller = new StudentCourseController(repository.Object);

            var get = await controller.Get(1, 2);

            Assert.Equal(dto, get.Value);
        }

        [Fact]
        public async Task Get_given_non_existing_id_returns_NotFound()
        {
            var repository = new Mock<IStudentCourseRepository>();

            var controller = new StudentCourseController(repository.Object);

            var get = await controller.Get(1, 2);

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var input = new StudentCourseDTO();
            var output = new StudentCourseDTO { CourseId = 1, StudentId = 1};
            var repository = new Mock<IStudentCourseRepository>();
            repository.Setup(s => s.CreateAsync(input)).ReturnsAsync(output);

            var controller = new StudentCourseController(repository.Object);

            var post = await controller.Post(input);

            var result = post as CreatedAtActionResult;

            Assert.Equal("Get", result.ActionName);
            Assert.Equal(output, result.Value);
        }

        [Fact]
        public async Task Post_given_dto_creates_studentcourse()
        {
            var repository = new Mock<IStudentCourseRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<StudentCourseDTO>())).ReturnsAsync(new StudentCourseDTO());

            var controller = new StudentCourseController(repository.Object);

            var dto = new StudentCourseDTO();

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }

        [Fact]
        public async Task Delete_given_groups_deletes_studentcourse()
        {
            var repository = new Mock<IStudentCourseRepository>();

            var controller = new StudentCourseController(repository.Object);

            await controller.Delete(3, 1);

            repository.Verify(s => s.DeleteAsync(3, 1));
        }

        [Fact]
        public async Task Delete_given_id_exists_returns_NoContent()
        {
            var repository = new Mock<IStudentCourseRepository>();
            repository.Setup(s => s.DeleteAsync(3, 1)).ReturnsAsync(true);
            var controller = new StudentCourseController(repository.Object);

            var delete = await controller.Delete(3, 1);

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Delete_given_id_not_exists_returns_NotFoundResult()
        {
            var repository = new Mock<IStudentCourseRepository>();

            var controller = new StudentCourseController(repository.Object);

            var delete = await controller.Delete(1, 1);

            Assert.IsType<NotFoundResult>(delete);
        }
    }
}
