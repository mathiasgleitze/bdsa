﻿using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Controllers;
using BDSA.Web.Hubs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using MockQueryable.Moq;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Web.Tests.Controllers
{
    public class MessageControllerTests
    {
        //This method is taken from Rasmus Lecture 10
        //Link: https://github.com/ondfisk/BDSA2018/blob/master/BDSA2018.Lecture10.Web.Tests/Controllers/ActorsControllerTests.cs
        [Fact]
        public void Controller_has_AuthorizeAttribute()
        {
            var controller = typeof(MessageController);

            var attributes = controller.GetCustomAttributes(false).Select(a => a.GetType());

            Assert.Contains(typeof(AuthorizeAttribute), attributes);
        }

        [Fact]
        public async Task Get_returns_dtos()
        {
            var dto = new MessageDTO { ChatId=1};
            var all = new[] { dto }.AsQueryable().BuildMock();
            var repository = new Mock<IMessageRepository>();
            var hub = new Mock<IHubContext<MessageHub>> { DefaultValue = DefaultValue.Mock };
            repository.Setup(s => s.Read(1)).Returns(all.Object);

            var controller = new MessageController(repository.Object,hub.Object);

            var result = await controller.Get(1);

            Assert.Equal(dto, result.Value.Single());
        }

        [Fact]
        public async Task Get_given_existing_id_returns_dto()
        {
            var dto = new MessageDTO();
            var repository = new Mock<IMessageRepository>();
            var hub = new Mock<IHubContext<MessageHub>> { DefaultValue = DefaultValue.Mock };
            repository.Setup(s => s.FindAsync(1,new DateTime(10000))).ReturnsAsync(dto);

            var controller = new MessageController(repository.Object,hub.Object);

            var get = await controller.Get(1, new DateTime(10000));

            Assert.Equal(dto, get.Value);
        }

        [Fact]
        public async Task Get_given_non_existing_id_returns_NotFound()
        {
            var repository = new Mock<IMessageRepository>();
            var hub = new Mock<IHubContext<MessageHub>> { DefaultValue = DefaultValue.Mock };

            var controller = new MessageController(repository.Object,hub.Object);

            var get = await controller.Get(1,new DateTime(100));

            Assert.IsType<NotFoundResult>(get.Result);
        }

        [Fact]
        public async Task Post_given_dto_returns_created_at_action()
        {
            var output = new MessageDTO { ChatId = 1, Time = new DateTime(100)};
            var hub = new Mock<IHubContext<MessageHub>> { DefaultValue = DefaultValue.Mock };
            var repository = new Mock<IMessageRepository>();
            repository.Setup(s => s.CreateAsync(output)).ReturnsAsync(output);

            var controller = new MessageController(repository.Object, hub.Object);

            var post = await controller.Post(output);

            var response = post.Result;

            Assert.IsType<CreatedAtActionResult>(response);
        }

        [Fact]
        public async Task Post_given_dto_creates_message()
        {
            var repository = new Mock<IMessageRepository>();
            repository.Setup(s => s.CreateAsync(It.IsAny<MessageDTO>())).ReturnsAsync(new MessageDTO());
            var hub = new Mock<IHubContext<MessageHub>> { DefaultValue = DefaultValue.Mock };

            var controller = new MessageController(repository.Object,hub.Object);

            var dto = new MessageDTO();

            await controller.Post(dto);

            repository.Verify(s => s.CreateAsync(dto));
        }
        [Fact]
        public async Task Delete_given_chatid_and_time_deletes_message()
        {
            var repository = new Mock<IMessageRepository>();
            var hub = new Mock<IHubContext<MessageHub>> { DefaultValue = DefaultValue.Mock };

            var controller = new MessageController(repository.Object,hub.Object);

            await controller.Delete(3,new DateTime(100));

            repository.Verify(s => s.DeleteAsync(3,new DateTime(100)));
        }

        [Fact]
        public async Task Delete_returns_NoContent()
        {
            var repository = new Mock<IMessageRepository>();
            var hub = new Mock<IHubContext<MessageHub>> { DefaultValue = DefaultValue.Mock };
            repository.Setup(s => s.DeleteAsync(2,new DateTime(10))).ReturnsAsync(true);
            var controller = new MessageController(repository.Object,hub.Object);

            var delete = await controller.Delete(2, new DateTime(10));

            Assert.IsType<NoContentResult>(delete);
        }

        [Fact]
        public async Task Delete_given_repository_returns_false_returns_NotFound()
        {
            var repository = new Mock<IMessageRepository>();
            var hub = new Mock<IHubContext<MessageHub>> { DefaultValue = DefaultValue.Mock };

            var controller = new MessageController(repository.Object,hub.Object);

            var delete = await controller.Delete(3,new DateTime(10));

            Assert.IsType<NotFoundResult>(delete);
        }

        [Fact]
        public async Task Delete_given_chatid_deletes_messages()
        {
            var repository = new Mock<IMessageRepository>();
            var hub = new Mock<IHubContext<MessageHub>> { DefaultValue = DefaultValue.Mock };

            var controller = new MessageController(repository.Object,hub.Object);

            await controller.Delete(1);

            repository.Verify(s => s.DeleteMessagesAsync(1));
        }

        [Fact]
        public async Task Delete_given_chatid_returns_NoContent()
        {
            var repository = new Mock<IMessageRepository>();
            repository.Setup(s => s.DeleteMessagesAsync(1)).ReturnsAsync(true);
            var hub = new Mock<IHubContext<MessageHub>> { DefaultValue = DefaultValue.Mock };
            var controller = new MessageController(repository.Object,hub.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NoContentResult>(delete);
        }
        
        [Fact]
        public async Task Delete_given_non_existing_chatid_returns_NotFound()
        {
            var repository = new Mock<IMessageRepository>();
            var hub = new Mock<IHubContext<MessageHub>> { DefaultValue = DefaultValue.Mock };

            var controller = new MessageController(repository.Object,hub.Object);

            var delete = await controller.Delete(1);

            Assert.IsType<NotFoundResult>(delete);
        }
    }

}
