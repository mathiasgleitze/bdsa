# Remarks

When logging on to acquire your authentication token, your email will be used to gain an id of a student with the same email, however, as that chance is low, you will just get id 1. 
This happens at BDSA/BDSA.GroupITApp/BDSA.GroupITApp/ViewModels/ActionViewModel.cs line 174, you can change that id, if you want to play around with multiple students. 



Security

"AzureAd": {
  "ClientSecret": "uTeOfOku4OkaHDpfpnU+oqnCc9v4G2wf3bw+kKuWjfI="
}
