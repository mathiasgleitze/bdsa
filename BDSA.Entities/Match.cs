﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Entities
{
    public class Match
    {
        public int GroupOneId { get; set; }
        public int GroupTwoId { get; set; }
        public int ChatId { get; set; }
        public Group GroupOne { get; set; }
        public Group GroupTwo { get; set; }
        public Chat Chat { get; set; }
        public byte[] RowVersion { get; set; }

    }
}
