﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace BDSA.Entities.Migrations
{
    public partial class update : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Chats",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Chats", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Courses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 100, nullable: true),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Courses", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Students",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: false),
                    Mail = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    About = table.Column<string>(nullable: true),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Students", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Teachers",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(maxLength: 50, nullable: true),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Teachers", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Projects",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    Name = table.Column<string>(nullable: true),
                    CourseId = table.Column<int>(nullable: false),
                    MaxSize = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Projects", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Projects_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Messages",
                columns: table => new
                {
                    Time = table.Column<DateTime>(nullable: false),
                    ChatId = table.Column<int>(nullable: false),
                    Text = table.Column<string>(nullable: true),
                    StudentId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Messages", x => new { x.ChatId, x.Time });
                    table.ForeignKey(
                        name: "FK_Messages_Chats_ChatId",
                        column: x => x.ChatId,
                        principalTable: "Chats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Messages_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentCourses",
                columns: table => new
                {
                    StudentId = table.Column<int>(nullable: false),
                    CourseId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCourses", x => new { x.StudentId, x.CourseId });
                    table.ForeignKey(
                        name: "FK_StudentCourses_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentCourses_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "TeacherCourses",
                columns: table => new
                {
                    TeacherId = table.Column<int>(nullable: false),
                    CourseId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TeacherCourses", x => new { x.TeacherId, x.CourseId });
                    table.ForeignKey(
                        name: "FK_TeacherCourses_Courses_CourseId",
                        column: x => x.CourseId,
                        principalTable: "Courses",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_TeacherCourses_Teachers_TeacherId",
                        column: x => x.TeacherId,
                        principalTable: "Teachers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Groups",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    ProjectId = table.Column<int>(nullable: false),
                    ChatId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Groups", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Groups_Chats_ChatId",
                        column: x => x.ChatId,
                        principalTable: "Chats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Groups_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ProjectProfiles",
                columns: table => new
                {
                    StudentId = table.Column<int>(nullable: false),
                    ProjectId = table.Column<int>(nullable: false),
                    About = table.Column<string>(nullable: true),
                    Ambitions = table.Column<int>(nullable: false),
                    MeetingDisciplin = table.Column<int>(nullable: false),
                    SkillLevel = table.Column<int>(nullable: false),
                    Flexibility = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ProjectProfiles", x => new { x.StudentId, x.ProjectId });
                    table.ForeignKey(
                        name: "FK_ProjectProfiles_Projects_ProjectId",
                        column: x => x.ProjectId,
                        principalTable: "Projects",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ProjectProfiles_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "GroupRequests",
                columns: table => new
                {
                    FromGroup = table.Column<int>(nullable: false),
                    ToGroup = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_GroupRequests", x => new { x.FromGroup, x.ToGroup });
                    table.ForeignKey(
                        name: "FK_GroupRequests_Groups_FromGroup",
                        column: x => x.FromGroup,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_GroupRequests_Groups_ToGroup",
                        column: x => x.ToGroup,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "Matches",
                columns: table => new
                {
                    GroupOneId = table.Column<int>(nullable: false),
                    GroupTwoId = table.Column<int>(nullable: false),
                    ChatId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Matches", x => new { x.GroupOneId, x.GroupTwoId });
                    table.ForeignKey(
                        name: "FK_Matches_Chats_ChatId",
                        column: x => x.ChatId,
                        principalTable: "Chats",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Matches_Groups_GroupOneId",
                        column: x => x.GroupOneId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_Matches_Groups_GroupTwoId",
                        column: x => x.GroupTwoId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "MatchRequests",
                columns: table => new
                {
                    FromGroup = table.Column<int>(nullable: false),
                    ToGroup = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MatchRequests", x => new { x.FromGroup, x.ToGroup });
                    table.ForeignKey(
                        name: "FK_MatchRequests_Groups_FromGroup",
                        column: x => x.FromGroup,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_MatchRequests_Groups_ToGroup",
                        column: x => x.ToGroup,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "StudentGroups",
                columns: table => new
                {
                    StudentId = table.Column<int>(nullable: false),
                    GroupId = table.Column<int>(nullable: false),
                    RowVersion = table.Column<byte[]>(rowVersion: true, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentGroups", x => new { x.StudentId, x.GroupId });
                    table.ForeignKey(
                        name: "FK_StudentGroups_Groups_GroupId",
                        column: x => x.GroupId,
                        principalTable: "Groups",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentGroups_Students_StudentId",
                        column: x => x.StudentId,
                        principalTable: "Students",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Chats",
                columns: new[] { "Id", "RowVersion" },
                values: new object[,]
                {
                    { 1, null },
                    { 30, null },
                    { 29, null },
                    { 28, null },
                    { 27, null },
                    { 25, null },
                    { 24, null },
                    { 23, null },
                    { 22, null },
                    { 21, null },
                    { 20, null },
                    { 19, null },
                    { 18, null },
                    { 17, null },
                    { 16, null },
                    { 26, null },
                    { 14, null },
                    { 15, null },
                    { 2, null },
                    { 3, null },
                    { 5, null },
                    { 6, null },
                    { 7, null },
                    { 4, null },
                    { 9, null },
                    { 10, null },
                    { 11, null },
                    { 12, null },
                    { 13, null },
                    { 8, null }
                });

            migrationBuilder.InsertData(
                table: "Courses",
                columns: new[] { "Id", "Name", "RowVersion" },
                values: new object[,]
                {
                    { 1, "Analysis, Design and Software Architecture", null },
                    { 2, "Introduction to Database Design", null },
                    { 3, "Mobile and Distributed Systems", null }
                });

            migrationBuilder.InsertData(
                table: "Students",
                columns: new[] { "Id", "About", "Address", "Mail", "Name", "RowVersion" },
                values: new object[,]
                {
                    { 15, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", "Copenhagen", "alhi@itu.com", "Alayah Hill", null },
                    { 10, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", "Copenhagen", "beki@itu.com", "Benjamin King", null },
                    { 14, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", "Copenhagen", "brwa@itu.com", "Brian Ward", null },
                    { 13, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", "Copenhagen", "rara@itu.com", "Rachel Ramirez", null },
                    { 12, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", "Copenhagen", "nabe@itu.com", "Nancy Bennett", null },
                    { 11, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", "Copenhagen", "anru@itu.com", "Anna Russell", null },
                    { 9, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", "Copenhagen", "ruma@itu.com", "Ruth Martin", null },
                    { 1, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", "Copenhagen", "jsam@itu.dk", "Jesper Amorsen", null },
                    { 7, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", "Copenhagen", "bopr@itu.com", "Bobby Price", null },
                    { 6, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", "Copenhagen", "mymo@itu.dk", "My Maja Mosthaf", null },
                    { 5, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", "Copenhagen", "mglh@itu.dk", "Mathias Gleitze Hoffmann", null },
                    { 4, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", "Copenhagen", "mdrh@itu.dk", "Martin Drøger Hansen", null },
                    { 3, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", "Copenhagen", "manb@itu.dk", "Mai An Krause Bernt", null },
                    { 2, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", "Copenhagen", "lukh@itu.dk", "Lukas Burmølle Holst", null },
                    { 8, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", "Copenhagen", "jeba@itu.com", "Jessica Barnes", null }
                });

            migrationBuilder.InsertData(
                table: "Teachers",
                columns: new[] { "Id", "Name", "RowVersion" },
                values: new object[,]
                {
                    { 2, "Johanna Blackburn", null },
                    { 1, "Graham Harrell", null },
                    { 3, "Theodore Dodson", null }
                });

            migrationBuilder.InsertData(
                table: "Projects",
                columns: new[] { "Id", "CourseId", "MaxSize", "Name", "RowVersion" },
                values: new object[,]
                {
                    { 1, 1, 5, "Exam Project", null },
                    { 2, 2, 4, "Database Project", null }
                });

            migrationBuilder.InsertData(
                table: "StudentCourses",
                columns: new[] { "StudentId", "CourseId", "RowVersion" },
                values: new object[,]
                {
                    { 9, 2, null },
                    { 9, 3, null },
                    { 10, 1, null },
                    { 10, 2, null },
                    { 10, 3, null },
                    { 11, 1, null },
                    { 11, 2, null },
                    { 11, 3, null },
                    { 12, 1, null },
                    { 12, 2, null },
                    { 12, 3, null },
                    { 13, 1, null },
                    { 13, 2, null },
                    { 13, 3, null },
                    { 14, 1, null },
                    { 14, 2, null },
                    { 14, 3, null },
                    { 15, 1, null },
                    { 15, 2, null },
                    { 15, 3, null },
                    { 9, 1, null },
                    { 8, 3, null },
                    { 8, 2, null },
                    { 8, 1, null },
                    { 1, 1, null },
                    { 1, 2, null },
                    { 1, 3, null },
                    { 2, 1, null },
                    { 2, 2, null },
                    { 2, 3, null },
                    { 3, 1, null },
                    { 3, 2, null },
                    { 3, 3, null },
                    { 4, 2, null },
                    { 4, 1, null },
                    { 5, 1, null },
                    { 5, 2, null },
                    { 5, 3, null },
                    { 6, 1, null },
                    { 6, 2, null },
                    { 6, 3, null },
                    { 7, 1, null },
                    { 7, 2, null },
                    { 7, 3, null },
                    { 4, 3, null }
                });

            migrationBuilder.InsertData(
                table: "TeacherCourses",
                columns: new[] { "TeacherId", "CourseId", "RowVersion" },
                values: new object[,]
                {
                    { 1, 2, null },
                    { 1, 1, null },
                    { 1, 3, null }
                });

            migrationBuilder.InsertData(
                table: "Groups",
                columns: new[] { "Id", "ChatId", "ProjectId", "RowVersion" },
                values: new object[,]
                {
                    { 1, 1, 1, null },
                    { 26, 26, 2, null },
                    { 25, 25, 2, null },
                    { 24, 24, 2, null },
                    { 23, 23, 2, null },
                    { 22, 22, 2, null },
                    { 21, 21, 2, null },
                    { 20, 20, 2, null },
                    { 19, 19, 2, null },
                    { 18, 18, 2, null },
                    { 17, 17, 2, null },
                    { 16, 16, 2, null },
                    { 29, 29, 2, null },
                    { 30, 30, 2, null },
                    { 27, 27, 2, null },
                    { 28, 28, 2, null },
                    { 13, 13, 1, null },
                    { 2, 2, 1, null },
                    { 3, 3, 1, null },
                    { 4, 4, 1, null },
                    { 5, 5, 1, null },
                    { 15, 15, 1, null },
                    { 6, 6, 1, null },
                    { 7, 7, 1, null },
                    { 8, 8, 1, null },
                    { 9, 9, 1, null },
                    { 10, 10, 1, null },
                    { 11, 11, 1, null },
                    { 14, 14, 1, null },
                    { 12, 12, 1, null }
                });

            migrationBuilder.InsertData(
                table: "ProjectProfiles",
                columns: new[] { "StudentId", "ProjectId", "About", "Ambitions", "Flexibility", "MeetingDisciplin", "RowVersion", "SkillLevel" },
                values: new object[,]
                {
                    { 12, 2, "Something nice", 10, 4, 6, null, 9 },
                    { 1, 2, "Something nice", 3, 1, 3, null, 1 },
                    { 2, 2, "Something nice", 10, 4, 2, null, 4 },
                    { 3, 2, "Something nice", 1, 4, 7, null, 3 },
                    { 4, 2, "Something nice", 8, 7, 8, null, 4 },
                    { 5, 2, "Something nice", 10, 5, 9, null, 8 },
                    { 6, 2, "Something nice", 6, 9, 9, null, 3 },
                    { 8, 2, "Something nice", 2, 7, 8, null, 5 },
                    { 9, 2, "Something nice", 2, 3, 7, null, 4 },
                    { 10, 2, "Something nice", 10, 7, 3, null, 4 },
                    { 11, 2, "Something nice", 7, 7, 10, null, 8 },
                    { 13, 2, "Something nice", 1, 7, 2, null, 5 },
                    { 7, 2, "Something nice", 4, 3, 7, null, 5 },
                    { 15, 1, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", 8, 2, 2, null, 6 },
                    { 14, 1, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", 9, 9, 9, null, 9 },
                    { 13, 1, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", 5, 7, 6, null, 8 },
                    { 12, 1, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", 1, 7, 6, null, 6 },
                    { 11, 1, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", 7, 7, 8, null, 8 },
                    { 10, 1, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", 10, 3, 5, null, 9 },
                    { 9, 1, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", 4, 5, 6, null, 7 },
                    { 8, 1, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", 7, 7, 7, null, 7 },
                    { 7, 1, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", 6, 7, 3, null, 4 },
                    { 6, 1, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", 1, 1, 1, null, 1 },
                    { 5, 1, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", 6, 6, 6, null, 6 },
                    { 4, 1, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", 3, 7, 8, null, 6 },
                    { 3, 1, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", 10, 10, 10, null, 10 },
                    { 2, 1, "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.", 5, 5, 5, null, 5 },
                    { 1, 1, "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.", 7, 7, 2, null, 1 },
                    { 14, 2, "Something nice", 8, 10, 5, null, 1 },
                    { 15, 2, "Something nice", 5, 8, 6, null, 7 }
                });

            migrationBuilder.InsertData(
                table: "StudentGroups",
                columns: new[] { "StudentId", "GroupId", "RowVersion" },
                values: new object[,]
                {
                    { 1, 1, null },
                    { 13, 28, null },
                    { 12, 27, null },
                    { 11, 26, null },
                    { 10, 25, null },
                    { 9, 24, null },
                    { 8, 23, null },
                    { 7, 22, null },
                    { 6, 21, null },
                    { 5, 20, null },
                    { 4, 19, null },
                    { 3, 18, null },
                    { 2, 17, null },
                    { 1, 16, null },
                    { 15, 15, null },
                    { 14, 14, null },
                    { 13, 13, null },
                    { 12, 12, null },
                    { 11, 11, null },
                    { 10, 10, null },
                    { 9, 9, null },
                    { 8, 8, null },
                    { 7, 7, null },
                    { 6, 6, null },
                    { 5, 5, null },
                    { 4, 4, null },
                    { 3, 3, null },
                    { 2, 2, null },
                    { 14, 29, null },
                    { 15, 30, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_GroupRequests_ToGroup",
                table: "GroupRequests",
                column: "ToGroup");

            migrationBuilder.CreateIndex(
                name: "IX_Groups_ChatId",
                table: "Groups",
                column: "ChatId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Groups_ProjectId",
                table: "Groups",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Matches_ChatId",
                table: "Matches",
                column: "ChatId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Matches_GroupTwoId",
                table: "Matches",
                column: "GroupTwoId");

            migrationBuilder.CreateIndex(
                name: "IX_MatchRequests_ToGroup",
                table: "MatchRequests",
                column: "ToGroup");

            migrationBuilder.CreateIndex(
                name: "IX_Messages_StudentId",
                table: "Messages",
                column: "StudentId");

            migrationBuilder.CreateIndex(
                name: "IX_ProjectProfiles_ProjectId",
                table: "ProjectProfiles",
                column: "ProjectId");

            migrationBuilder.CreateIndex(
                name: "IX_Projects_CourseId",
                table: "Projects",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourses_CourseId",
                table: "StudentCourses",
                column: "CourseId");

            migrationBuilder.CreateIndex(
                name: "IX_StudentGroups_GroupId",
                table: "StudentGroups",
                column: "GroupId");

            migrationBuilder.CreateIndex(
                name: "IX_TeacherCourses_CourseId",
                table: "TeacherCourses",
                column: "CourseId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "GroupRequests");

            migrationBuilder.DropTable(
                name: "Matches");

            migrationBuilder.DropTable(
                name: "MatchRequests");

            migrationBuilder.DropTable(
                name: "Messages");

            migrationBuilder.DropTable(
                name: "ProjectProfiles");

            migrationBuilder.DropTable(
                name: "StudentCourses");

            migrationBuilder.DropTable(
                name: "StudentGroups");

            migrationBuilder.DropTable(
                name: "TeacherCourses");

            migrationBuilder.DropTable(
                name: "Groups");

            migrationBuilder.DropTable(
                name: "Students");

            migrationBuilder.DropTable(
                name: "Teachers");

            migrationBuilder.DropTable(
                name: "Chats");

            migrationBuilder.DropTable(
                name: "Projects");

            migrationBuilder.DropTable(
                name: "Courses");
        }
    }
}
