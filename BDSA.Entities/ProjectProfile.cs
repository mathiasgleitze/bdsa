﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Entities
{
    public class ProjectProfile  
    {
        public int StudentId { get; set; }
        public int ProjectId { get; set; }
        public string About { get; set; }
        public int Ambitions { get; set; }
        public int MeetingDisciplin { get; set; }
        public int SkillLevel { get; set; }
        public int Flexibility { get; set; }
        public Student Student { get; set; }
        public Project Project { get; set; }
        public byte[] RowVersion { get; set; }

    }
}
