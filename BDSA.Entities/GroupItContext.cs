﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDSA.Entities
{
    public class GroupItContext : DbContext,IGroupItContext
    {
        public virtual DbSet<Chat> Chats { get; set; }
        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<StudentCourse> StudentCourses { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<GroupRequest> GroupRequests { get; set; }
        public virtual DbSet<TeacherCourse> TeacherCourses { get; set; }
        public virtual DbSet<Match> Matches { get; set; }
        public virtual DbSet<MatchRequest> MatchRequests { get; set; }
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<Project> Projects { get; set; }
        public virtual DbSet<ProjectProfile> ProjectProfiles { get; set; }
        public virtual DbSet<Student> Students { get; set; }
        public virtual DbSet<StudentGroup> StudentGroups { get; set; }
        public virtual DbSet<Teacher> Teachers { get; set; }

        
        public GroupItContext(DbContextOptions<GroupItContext> options) : base(options)
        {

        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            { 

            }
        }

        


        protected override void OnModelCreating(ModelBuilder modelbuilder)
        {
           
            SetUpKeys(modelbuilder);
            SetUpRowVersion(modelbuilder);
            //Only run the first time after migration, and not doing test
            //Seed(modelbuilder);
        }

        private void SetUpKeys(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<TeacherCourse>().HasKey(e => new { e.TeacherId, e.CourseId });
            modelbuilder.Entity<TeacherCourse>()
                .HasOne(es => es.Course)
                .WithMany(e => e.Teachers)
                .HasForeignKey(es => es.CourseId);
            modelbuilder.Entity<TeacherCourse>()
                .HasOne(es => es.Teacher)
                .WithMany(e => e.Courses)
                .HasForeignKey(es => es.TeacherId);

            

            modelbuilder.Entity<Project>()
                .HasMany(g => g.Groups)
                .WithOne(p => p.Project);


            modelbuilder.Entity<StudentCourse>().HasKey(e => new { e.StudentId, e.CourseId });
            modelbuilder.Entity<StudentCourse>()
                .HasOne(es => es.Student)
                .WithMany(e => e.Courses)
                .HasForeignKey(es => es.StudentId);
            modelbuilder.Entity<StudentCourse>()
                .HasOne(es => es.Course)
                .WithMany(e => e.Students)
                .HasForeignKey(es => es.CourseId);

            modelbuilder.Entity<StudentGroup>().HasKey(e => new { e.StudentId, e.GroupId });
            modelbuilder.Entity<StudentGroup>()
                .HasOne(es => es.Student)
                .WithMany(e => e.Groups)
                .HasForeignKey(es => es.StudentId);
            modelbuilder.Entity<StudentGroup>()
                .HasOne(es => es.Group)
                .WithMany(e => e.Students)
                .HasForeignKey(es => es.GroupId);

            modelbuilder.Entity<MatchRequest>().HasKey(e => new { e.FromGroup, e.ToGroup });
            modelbuilder.Entity<MatchRequest>()
                .HasOne(es => es.From)
                .WithMany(e => e.ToMatchRequest)
                .IsRequired()
                .HasForeignKey(es => es.FromGroup)
                .OnDelete(DeleteBehavior.Restrict);
            modelbuilder.Entity<MatchRequest>()
                .HasOne(es => es.To)
                .WithMany(e => e.FromMatchRequest)
                .IsRequired()
                .HasForeignKey(es => es.ToGroup)
                .OnDelete(DeleteBehavior.Restrict);


            modelbuilder.Entity<Match>().HasKey(e => new { e.GroupOneId, e.GroupTwoId });
            modelbuilder.Entity<Match>()
                .HasOne(es => es.GroupOne)
                .WithMany(e => e.Two)
                .IsRequired()
                .HasForeignKey(es => es.GroupOneId)
                .OnDelete(DeleteBehavior.Restrict);
            modelbuilder.Entity<Match>()
                .HasOne(es => es.GroupTwo)
                .WithMany(e => e.One)
                .IsRequired()
                .HasForeignKey(es => es.GroupTwoId)
                .OnDelete(DeleteBehavior.Restrict);


            modelbuilder.Entity<GroupRequest>().HasKey(e => new { e.FromGroup, e.ToGroup });
            modelbuilder.Entity<GroupRequest>()
                .HasOne(es => es.From)
                .WithMany(e => e.ToGroupRequest)
                .HasForeignKey(es => es.FromGroup)
                .OnDelete(DeleteBehavior.Restrict);
            modelbuilder.Entity<GroupRequest>()
                .HasOne(es => es.To)
                .WithMany(e => e.FromGroupRequest)
                .HasForeignKey(es => es.ToGroup)
                .OnDelete(DeleteBehavior.Restrict);

            modelbuilder.Entity<Message>().HasKey(e => new { e.ChatId, e.Time });
            modelbuilder.Entity<Message>()
                .HasOne(e => e.Chat)
                .WithMany(es => es.Messages)
                .HasForeignKey(e => e.ChatId);


            modelbuilder.Entity<ProjectProfile>().HasKey(e => new { e.StudentId, e.ProjectId });
            modelbuilder.Entity<ProjectProfile>()
                .HasOne(s => s.Student)
                .WithMany(p => p.ProjectProfiles)
                .HasForeignKey(p => p.StudentId);
            modelbuilder.Entity<ProjectProfile>()
                .HasOne(pr => pr.Project)
                .WithMany(p => p.ProjectProfiles)
                .HasForeignKey(p => p.ProjectId);
        }

        private void SetUpRowVersion(ModelBuilder modelbuilder)
        {
            modelbuilder.Entity<Chat>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<Course>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<Group>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<GroupRequest>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<Match>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<MatchRequest>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<Message>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<Project>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<ProjectProfile>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<Student>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<StudentCourse>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<StudentGroup>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<Teacher>()
                .Property(r => r.RowVersion).IsRowVersion();

            modelbuilder.Entity<TeacherCourse>()
                .Property(r => r.RowVersion).IsRowVersion();
        }

        public void Seed(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chat>().HasData(
                new Chat { Id = 1 },
                new Chat { Id = 2 },
                new Chat { Id = 3 },
                new Chat { Id = 4 },
                new Chat { Id = 5 },
                new Chat { Id = 6 },
               
                new Chat { Id = 16 },
                new Chat { Id = 17 },
                new Chat { Id = 18 },
                new Chat { Id = 19 },
                new Chat { Id = 20 },
                new Chat { Id = 21 }
            );

            modelBuilder.Entity<Course>().HasData(
                new Course
                {
                    Id = 1,
                    Name = "Analysis, Design and Software Architecture"
                },
                new Course
                {
                    Id = 2,
                    Name = "Introduction to Database Design"
                },
                new Course
                {
                    Id = 3,
                    Name = "Mobile and Distributed Systems"
                }
            );

            modelBuilder.Entity<Teacher>().HasData(
                new Teacher
                {
                    Id = 1,
                    Name = "Graham Harrell"
                },
                new Teacher
                {
                    Id = 2,
                    Name = "Johanna Blackburn"
                },
                new Teacher
                {
                    Id = 3,
                    Name = "Theodore Dodson"
                }
            );

            modelBuilder.Entity<Project>().HasData(
                new Project
                {
                    Id = 1,
                    CourseId = 1,
                    MaxSize = 5,
                    Name = "Exam Project"
                },
                new Project
                {
                    Id = 2,
                    CourseId = 2,
                    MaxSize = 4,
                    Name = "Database Project"
                }
            );

            var about1 = "Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is advertised as the most popular gun in American crime. Do you believe that shit? It actually says that in the little book that comes with it: the most popular gun in American crime. Like they're actually proud of that shit.";
            var about2 = "Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense! In a comic, you know how you can tell who the arch-villain's going to be? He's the exact opposite of the hero. And most times they're friends, like you and me! I should've known way back when... You know why, David? Because of the kids. They called me Mr Glass.";

            modelBuilder.Entity<Student>().HasData(
                new Student {
                    Id = 1,
                    Name = "Jesper Amorsen",
                    Mail = "jsam@itu.dk",
                    Address = "Copenhagen",
                    About = about1
                },
                new Student {
                    Id = 2,
                    Name = "Lukas Burmølle Holst",
                    Mail = "lukh@itu.dk",
                    Address = "Copenhagen",
                    About = about2
                },
                new Student {
                    Id = 3,
                    Name = "Mai An Krause Bernt",
                    Mail = "manb@itu.dk",
                    Address = "Copenhagen",
                    About = about1
                },
                new Student {
                    Id = 4,
                    Name = "Martin Drøger Hansen",
                    Mail = "mdrh@itu.dk",
                    Address = "Copenhagen",
                    About = about2
                },
                new Student {
                    Id = 5,
                    Name = "Mathias Gleitze Hoffmann",
                    Mail = "mglh@itu.dk",
                    Address = "Copenhagen",
                    About = about1
                },
                new Student {
                    Id = 6,
                    Name = "My Maja Mosthaf",
                    Mail = "mymo@itu.dk",
                    Address = "Copenhagen",
                    About = about2
                }
            );
            

            

            modelBuilder.Entity<StudentCourse>().HasData(
                new StudentCourse { StudentId = 1, CourseId = 1 },
                new StudentCourse { StudentId = 2, CourseId = 1 },
                new StudentCourse { StudentId = 3, CourseId = 1 },
                new StudentCourse { StudentId = 4, CourseId = 1 },
                new StudentCourse { StudentId = 5, CourseId = 1 },
                new StudentCourse { StudentId = 6, CourseId = 1 },
               
                new StudentCourse { StudentId = 1, CourseId = 2 },
                new StudentCourse { StudentId = 2, CourseId = 2 },
                new StudentCourse { StudentId = 3, CourseId = 2 },
                new StudentCourse { StudentId = 4, CourseId = 2 },
                new StudentCourse { StudentId = 5, CourseId = 2 },
                new StudentCourse { StudentId = 6, CourseId = 2 },
               
                new StudentCourse { StudentId = 1, CourseId = 3 },
                new StudentCourse { StudentId = 2, CourseId = 3 },
                new StudentCourse { StudentId = 3, CourseId = 3 },
                new StudentCourse { StudentId = 4, CourseId = 3 },
                new StudentCourse { StudentId = 5, CourseId = 3 },
                new StudentCourse { StudentId = 6, CourseId = 3 }

            );

            modelBuilder.Entity<Group>().HasData(
                new Group { Id = 1, ProjectId = 1, ChatId = 1 },
                new Group { Id = 2, ProjectId = 1, ChatId = 2 },
                new Group { Id = 3, ProjectId = 1, ChatId = 3 },
                new Group { Id = 4, ProjectId = 1, ChatId = 4 },
                new Group { Id = 5, ProjectId = 1, ChatId = 5 },
                new Group { Id = 6, ProjectId = 1, ChatId = 6 },
                
                new Group { Id = 16, ProjectId = 2, ChatId = 16 },
                new Group { Id = 17, ProjectId = 2, ChatId = 17 },
                new Group { Id = 18, ProjectId = 2, ChatId = 18 },
                new Group { Id = 19, ProjectId = 2, ChatId = 19 },
                new Group { Id = 20, ProjectId = 2, ChatId = 20 },
                new Group { Id = 21, ProjectId = 2, ChatId = 21 }
                
            );

            modelBuilder.Entity<StudentGroup>().HasData(
                new StudentGroup { GroupId = 1, StudentId = 1},
                new StudentGroup { GroupId = 2, StudentId = 2},
                new StudentGroup { GroupId = 3, StudentId = 3},
                new StudentGroup { GroupId = 4, StudentId = 4},
                new StudentGroup { GroupId = 5, StudentId = 5},
                new StudentGroup { GroupId = 6, StudentId = 6},
               
                new StudentGroup { GroupId = 16, StudentId = 1 },
                new StudentGroup { GroupId = 17, StudentId = 2 },
                new StudentGroup { GroupId = 18, StudentId = 3 },
                new StudentGroup { GroupId = 19, StudentId = 4 },
                new StudentGroup { GroupId = 20, StudentId = 5 },
                new StudentGroup { GroupId = 21, StudentId = 6 }
                
            );

            modelBuilder.Entity<TeacherCourse>().HasData(
                new TeacherCourse { TeacherId=1,CourseId=1},
                new TeacherCourse { TeacherId=1,CourseId=2},
                new TeacherCourse { TeacherId=1,CourseId=3}
            );

            modelBuilder.Entity<ProjectProfile>().HasData(
                new ProjectProfile
                {
                    StudentId = 1,
                    ProjectId = 1,
                    Ambitions = 7,
                    Flexibility = 7,
                    MeetingDisciplin = 2,
                    SkillLevel = 1,
                    About = about1
                },
                new ProjectProfile
                {
                    StudentId = 2,
                    ProjectId = 1,
                    Ambitions = 5,
                    Flexibility = 5,
                    MeetingDisciplin = 5,
                    SkillLevel = 5,
                    About = about2
                },
                new ProjectProfile
                {
                    StudentId = 3,
                    ProjectId = 1,
                    Ambitions = 10,
                    Flexibility = 10,
                    MeetingDisciplin = 10,
                    SkillLevel = 10,
                    About = about1
                },
                new ProjectProfile
                {
                    StudentId = 4,
                    ProjectId = 1,
                    Ambitions = 3,
                    Flexibility = 7,
                    MeetingDisciplin = 8,
                    SkillLevel = 6,
                    About = about2
                },
                new ProjectProfile
                {
                    StudentId = 5,
                    ProjectId = 1,
                    Ambitions = 6,
                    Flexibility = 6,
                    MeetingDisciplin = 6,
                    SkillLevel = 6,
                    About = about1
                }, 
                new ProjectProfile
                {
                    StudentId = 6,
                    ProjectId = 1,
                    Ambitions = 1,
                    Flexibility = 1,
                    MeetingDisciplin = 1,
                    SkillLevel = 1,
                    About = about2
                },
                new ProjectProfile
                {
                    StudentId = 7,
                    ProjectId = 1,
                    Ambitions = 6,
                    Flexibility = 7,
                    MeetingDisciplin = 3,
                    SkillLevel = 4,
                    About = about1
                },
                
                new ProjectProfile
                {
                    StudentId = 1,
                    ProjectId = 2,
                    Ambitions = 3,
                    Flexibility = 1,
                    MeetingDisciplin = 3,
                    SkillLevel = 1,
                    About = "Something nice"
                },
                new ProjectProfile
                {
                    StudentId = 2,
                    ProjectId = 2,
                    Ambitions = 10,
                    Flexibility = 4,
                    MeetingDisciplin = 2,
                    SkillLevel = 4,
                    About = "Something nice"
                },
                new ProjectProfile
                {
                    StudentId = 3,
                    ProjectId = 2,
                    Ambitions = 1,
                    Flexibility = 4,
                    MeetingDisciplin = 7,
                    SkillLevel = 3,
                    About = "Something nice"
                },
                new ProjectProfile
                {
                    StudentId = 4,
                    ProjectId = 2,
                    Ambitions = 8,
                    Flexibility = 7,
                    MeetingDisciplin = 8,
                    SkillLevel = 4,
                    About = "Something nice"
                },
                new ProjectProfile
                {
                    StudentId = 5,
                    ProjectId = 2,
                    Ambitions = 10,
                    Flexibility = 5,
                    MeetingDisciplin = 9,
                    SkillLevel = 8,
                    About = "Something nice"
                },
                new ProjectProfile
                {
                    StudentId = 6,
                    ProjectId = 2,
                    Ambitions = 6,
                    Flexibility = 9,
                    MeetingDisciplin = 9,
                    SkillLevel = 3,
                    About = "Something nice"
                }
            );

            modelBuilder.Entity<MatchRequest>().HasData(
                new MatchRequest
                {
                    FromGroup = 2,
                    ToGroup = 1
                },
                new MatchRequest
                {
                    FromGroup = 4,
                    ToGroup = 1
                }
            );

        }

    }
}
