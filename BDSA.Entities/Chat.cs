﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BDSA.Entities
{
    public class Chat
    {
        [Key]
        public int Id { get; set; }
        public Group Group { get; set; }
        public Match Match { get; set; }
        public ICollection<Message> Messages { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
