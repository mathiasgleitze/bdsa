﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BDSA.Entities
{
    public class Group
    {
        [Key]
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int ChatId { get; set; }
        public Project Project { get; set; }
        public Chat Chat { get; set; }
        public ICollection<StudentGroup> Students { get; set; }
        public ICollection<MatchRequest> FromMatchRequest { get; set; }
        public ICollection<MatchRequest> ToMatchRequest { get; set; }
        public ICollection<GroupRequest> FromGroupRequest { get; set; }
        public ICollection<GroupRequest> ToGroupRequest { get; set; }
        public ICollection<Match> One { get; set; }
        public ICollection<Match> Two { get; set; }
        public byte[] RowVersion { get; set; }

        public Group()
        {
            Students = new HashSet<StudentGroup>();
            FromGroupRequest = new HashSet<GroupRequest>();
            ToGroupRequest = new HashSet<GroupRequest>();
            FromMatchRequest = new HashSet<MatchRequest>();
            ToMatchRequest = new HashSet<MatchRequest>();


        }
    }
}
