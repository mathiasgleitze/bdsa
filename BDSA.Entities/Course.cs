﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BDSA.Entities
{
    public class Course
    {
        [Key]
        public int Id { get; set; }
        [StringLength(100)]
        public string Name { get; set; }
        public ICollection<StudentCourse> Students { get; set; }
        public ICollection<TeacherCourse> Teachers { get; set; }
        public byte[] RowVersion { get; set; }

        public Course()
        {
            Students = new HashSet<StudentCourse>();
            Teachers = new HashSet<TeacherCourse>();
        }
    }
}
