﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BDSA.Entities
{
    public interface IGroupItContext : IDisposable
    {
        DbSet<Chat> Chats { get; set; }
        DbSet<Course> Courses { get; set; }
        DbSet<StudentCourse> StudentCourses { get; set; }
        DbSet<Group> Groups { get; set; }
        DbSet<GroupRequest> GroupRequests { get; set; }
        DbSet<TeacherCourse> TeacherCourses { get; set; }
        DbSet<Match> Matches { get; set; }
        DbSet<MatchRequest> MatchRequests { get; set; }
        DbSet<Message> Messages { get; set; }
        DbSet<Project> Projects { get; set; }
        DbSet<ProjectProfile> ProjectProfiles { get; set; }
        DbSet<Student> Students { get; set; }
        DbSet<StudentGroup> StudentGroups { get; set; }
        DbSet<Teacher> Teachers { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken));
        void Seed(ModelBuilder modelbuilder);
    }
}
