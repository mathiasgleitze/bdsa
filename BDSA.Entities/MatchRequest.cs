﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Entities
{
    public class MatchRequest
    {
        public int FromGroup { get; set; }
        public int ToGroup { get; set; }
        public Group From { get; set; }
        public Group To { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
