﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BDSA.Entities
{
    public class Student
    {
        [Key]
        public int Id { get; set; }

        [StringLength(50)][Required]
        public string Name { get; set; }
        public string Mail { get; set; }
        public string Address { get; set; }
        public string About { get; set; }
        public ICollection<StudentCourse> Courses { get; set; }
        public ICollection<StudentGroup> Groups { get; set; }
        public ICollection<ProjectProfile> ProjectProfiles  { get; set; }
        public byte[] RowVersion { get; set; }

        public Student()
        {
            Courses = new HashSet<StudentCourse>();
            Groups = new HashSet<StudentGroup>();
        }

    }
}
