﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace BDSA.Entities
{
    public class GroupRequest
    {
        [Required]
        public int FromGroup { get; set; }
        [Required]
        public int ToGroup { get; set; }
        public Group From { get; set; }
        public Group To { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
