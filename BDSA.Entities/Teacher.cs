﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BDSA.Entities
{
    public class Teacher
    {
        [Key]
        public int Id { get; set; }
        [StringLength(50)]
        public string Name { get; set; }
        public ICollection<TeacherCourse> Courses { get; set; }
        public byte[] RowVersion { get; set; }

        public Teacher()
        {
            Courses = new HashSet<TeacherCourse>();
        }
    }
}
