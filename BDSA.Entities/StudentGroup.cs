﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Entities
{
    public class StudentGroup
    {
        public int StudentId { get; set; }
        public int GroupId { get; set; }
        public Student Student { get; set; }
        public Group Group { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
