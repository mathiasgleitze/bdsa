﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BDSA.Entities
{
    public class Message
    {
        public DateTime Time { get; set; }
        public int ChatId { get; set; }
        public string Text { get; set; }
        public int StudentId { get; set; }
        public Chat Chat { get; set; }
        public Student Student { get; set; }
        public byte[] RowVersion { get; set; }

    }
}
