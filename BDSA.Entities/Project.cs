﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BDSA.Entities
{
    public class Project
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public int CourseId{get;set;}
        public int MaxSize { get; set; }
        public Course Course { get; set; }
        public ICollection<Group> Groups { get; set; }
        public ICollection<ProjectProfile> ProjectProfiles { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
