﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Entities
{
    public class TeacherCourse
    {
        public int TeacherId { get; set; }
        public int CourseId { get; set; }
        public Teacher Teacher { get; set; }
        public Course Course { get; set; }
        public byte[] RowVersion { get; set; }
    }
}
