﻿using BDSA.Entities;
using BDSA.Shared;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Models.Tests
{
    public class GroupRequestRepositoryTests
    {
        public IGroupItContext GetInMemoryDB(string dbName)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                    .UseInMemoryDatabase(databaseName: dbName);
            return new GroupItContext(builder.Options);
        }
        [Fact]
        public async Task CreateAsync_given_DTO_create_it()
        {
            var context = GetInMemoryDB(nameof(CreateAsync_given_DTO_create_it));
            var repository = new GroupRequestRepository(context);

                var request = new GroupRequestDTO
                {
                   FromGroup=1,
                   ToGroup=2
                };

                var key = await repository.CreateAsync(request);

                var entity = await context.GroupRequests.FindAsync(key.FromGroup, key.ToGroup);
                Assert.Equal(1, entity.FromGroup);
                Assert.Equal(2, entity.ToGroup);
            
        }
        [Fact]
        public async Task FindAsync_return_postive()
        {
            var context = GetInMemoryDB(nameof(FindAsync_return_postive));
            var repository = new GroupRequestRepository(context);
                var request = new GroupRequest
                {
                    FromGroup=1,
                    ToGroup=2
                };
                context.GroupRequests.Add(request);
                await context.SaveChangesAsync();
                var enitity = await repository.FindAsync(request.FromGroup,request.ToGroup);
                Assert.Equal(1, enitity.FromGroup);
                Assert.Equal(2, enitity.ToGroup);
            
        }
        [Fact]
        public async Task FindAsync_return_negative()
        {
            var context = GetInMemoryDB(nameof(FindAsync_return_negative));
            var repository = new GroupRequestRepository(context);
                var enitity = await repository.FindAsync(3, 1);
                Assert.Null(enitity);
            
        }
        [Fact]
        public async Task FindAsync_given_unordered_keys_return_positive()
        {
            var context = GetInMemoryDB(nameof(FindAsync_given_unordered_keys_return_positive));
            var repository = new GroupRequestRepository(context);
                var request = new GroupRequest
                {
                    FromGroup = 1,
                    ToGroup = 2
                };
                context.GroupRequests.Add(request);
                await context.SaveChangesAsync();
                var entity = await repository.FindAsync(2, 1);
                var dto = new GroupRequestDTO { FromGroup = 1, ToGroup = 2 };
                Assert.Equal(dto.ToGroup, entity.ToGroup);
                Assert.Equal(dto.FromGroup, entity.FromGroup);
                Assert.NotNull(entity);
        }
        
        [Fact]
        public async Task DeleteAsync_given_existing_fromGroup_and_toGroup_deletes_it()
        {
            var context = GetInMemoryDB(nameof(DeleteAsync_given_existing_fromGroup_and_toGroup_deletes_it));
            var entity = new GroupRequest { FromGroup=1,ToGroup=2 };

                context.GroupRequests.Add(entity);
                await context.SaveChangesAsync();

                

                var repository = new GroupRequestRepository(context);

                var deleted = await repository.DeleteAsync(entity.FromGroup, entity.ToGroup);

                Assert.True(deleted);

                var deletedEntity = await context.GroupRequests.FindAsync(entity.FromGroup, entity.ToGroup);

                Assert.Null(deletedEntity);
            
        }
        [Fact]
        public async Task DeleteAsync_given_non_existing_fromGroup_and_toGroup_returns_false()
        {
            var context = GetInMemoryDB(nameof(DeleteAsync_given_non_existing_fromGroup_and_toGroup_returns_false));
            var repository = new GroupRequestRepository(context);

                var success = await repository.DeleteAsync(1,2);

                Assert.False(success);
            
        }
        [Fact]
        public void ReadFromGroup_return_empty()
        {
            var context = GetInMemoryDB(nameof(ReadFromGroup_return_empty));
            var repository = new GroupRequestRepository(context);
            var result = repository.ReadFromGroup(1);

            Assert.Empty(result);
        }
        [Fact]
        public void ReadToGroup_return_empty()
        {
            var context = GetInMemoryDB(nameof(ReadToGroup_return_empty));
            var repository = new GroupRequestRepository(context);
            var result = repository.ReadToGroup(1);

            Assert.Empty(result);
        }
        [Fact]
        public async Task ReadFromGroup_given_groupid_returns_all_request_for_that_group()
        {
            var context = GetInMemoryDB(nameof(ReadFromGroup_given_groupid_returns_all_request_for_that_group));
            var request1 = new GroupRequest { FromGroup=1,ToGroup=2 };
                var request2 = new GroupRequest { FromGroup=3,ToGroup=1 };
                var request3 = new GroupRequest { FromGroup=1,ToGroup=4 };
                
                context.GroupRequests.AddRange(request1,request2,request3);


                await context.SaveChangesAsync();

                var repository = new GroupRequestRepository(context);

                var groupRequestDTOs = repository.ReadFromGroup(1);

                var request = await groupRequestDTOs.FirstOrDefaultAsync();

                Assert.Equal(2, groupRequestDTOs.Count());
                Assert.Equal(1, request.FromGroup);
            
        }
        [Fact]
        public async Task ReadToGroup_given_groupid_returns_all_request_for_that_group()
        {
            var context = GetInMemoryDB(nameof(ReadToGroup_given_groupid_returns_all_request_for_that_group));
            var request1 = new GroupRequest { FromGroup = 1, ToGroup = 2 };
                var request2 = new GroupRequest { FromGroup = 3, ToGroup = 1 };
                var request3 = new GroupRequest { FromGroup = 1, ToGroup = 4 };

                context.GroupRequests.AddRange(request1, request2, request3);


                await context.SaveChangesAsync();

                var repository = new GroupRequestRepository(context);

                var groupRequestDTOs = repository.ReadToGroup(1);

                var request = await groupRequestDTOs.FirstOrDefaultAsync();

                Assert.Equal(1, groupRequestDTOs.Count());
                Assert.Equal(1, request.ToGroup);
            
        }
        [Fact]
        public async Task UpdateGroupRequest_return_true()
        {
            var context = GetInMemoryDB(nameof(UpdateGroupRequest_return_true));
            var request1 = new GroupRequest { FromGroup = 1, ToGroup = 2 };
            var request2 = new GroupRequest { FromGroup = 3, ToGroup = 1 };
            var request3 = new GroupRequest { FromGroup = 1, ToGroup = 4 };

            context.GroupRequests.AddRange(request1, request2, request3);
            await context.SaveChangesAsync();

            var repository = new GroupRequestRepository(context);

            var result = await repository.UpdateAsync(1, 5);

            var request = await context.GroupRequests.FindAsync(5, 2);

            Assert.True(result);
            Assert.Equal(5, request.FromGroup);
        }
        [Fact]
        public async Task UpdateGroupRequest_return_false()
        {
            var context = GetInMemoryDB(nameof(UpdateGroupRequest_return_false));
            var repository = new GroupRequestRepository(context);

            var result = await repository.UpdateAsync(1, 5);

            Assert.False(result);
        }

       
    }
}
