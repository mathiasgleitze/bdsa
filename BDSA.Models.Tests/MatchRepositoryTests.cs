﻿using BDSA.Entities;
using BDSA.Shared;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using System.Linq;

namespace BDSA.Models.Tests
{
    public class MatchRepositoryTests
    {
        public IGroupItContext GetInMemoryDB(string dbName)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                    .UseInMemoryDatabase(databaseName: dbName);
            return new GroupItContext(builder.Options);
        }

        [Fact]
        public async Task CreateAsync_given_DTO_create_it()
        {

            var context = GetInMemoryDB(nameof(CreateAsync_given_DTO_create_it));
            var repository = new MatchRepository(context);
                var match = new MatchDTO
                {
                    GroupOne = 1,
                    GroupTwo = 2,
                    ChatId=2
                };

                var key = await repository.CreateAsync(match);

                var entity = await context.Matches.FindAsync(key.GroupOne, key.GroupTwo);
                Assert.Equal(1, entity.GroupOneId);
                Assert.Equal(2, entity.GroupTwoId);
            
        }
        [Fact]
        public async Task FindAsync_return_postive()
        {
            var context = GetInMemoryDB(nameof(FindAsync_return_postive));
            context.Matches.Add(new Match { GroupOneId = 1, GroupTwoId = 3 });
            await context.SaveChangesAsync();
                var repository = new MatchRepository(context);
                
                
                var entity = await repository.FindAsync(1,3);
                Assert.Equal(1, entity.GroupOne);
                Assert.Equal(3, entity.GroupTwo);
            
        }
        [Fact]
        public async Task FindAsync_return_negative()
        {
            var context = GetInMemoryDB(nameof(FindAsync_return_negative));
            
                var repository = new MatchRepository(context);
                var entity = await repository.FindAsync(3, 1);
                Assert.Null(entity);
            
        }
        [Fact]
        public async Task FindAsync_given_unordered_keys_return_positive()
        {
            var context = GetInMemoryDB(nameof(FindAsync_given_unordered_keys_return_positive));
            
                var repository = new MatchRepository(context);
                var match = new Match
                {
                    GroupOneId = 1,
                    GroupTwoId = 2
                };
                context.Matches.Add(match);
                await context.SaveChangesAsync();
                var entity = await repository.FindAsync(2, 1);
                var dto = new Match { GroupOneId = 1, GroupTwoId = 2 };
                Assert.Equal(dto.GroupOneId, entity.GroupOne);
                Assert.Equal(dto.GroupTwoId, entity.GroupTwo);
                Assert.NotNull(entity);

            
        }
        [Fact]
        public async Task Read_given_groupid_returns_all_match_for_that_group()
        {
            var context = GetInMemoryDB(nameof(Read_given_groupid_returns_all_match_for_that_group));
            
                var match1 = new Match { GroupOneId = 1, GroupTwoId = 2 };
                var match2 = new Match { GroupOneId = 1, GroupTwoId = 3 };
                var match3 = new Match { GroupOneId = 2, GroupTwoId = 3 };
                

                context.Matches.AddRange(match1,match2,match3);


                await context.SaveChangesAsync();

                var repository = new MatchRepository(context);

                var matchDTOs = repository.FindAllMatches(1);

                var match = await matchDTOs.FirstOrDefaultAsync();

                Assert.Equal(2, matchDTOs.Count());
                Assert.Equal(1, match.GroupOne);
            
        }
        [Fact]
        public async Task DeleteAsync_given_existing_fromGroup_and_toGroup_deletes_it()
        {
            var context = GetInMemoryDB(nameof(DeleteAsync_given_existing_fromGroup_and_toGroup_deletes_it));
            

                var entity = new Match { GroupOneId = 1, GroupTwoId = 2,ChatId=1 };

                context.Matches.Add(entity);
                await context.SaveChangesAsync();

                var repository = new MatchRepository(context);

                var deleted = await repository.DeleteAsync(entity.GroupOneId, entity.GroupTwoId);

                Assert.True(deleted);

                var deletedEntity = await context.Matches.FindAsync(entity.GroupOne, entity.GroupTwo);

                Assert.Null(deletedEntity);
            
        }
        [Fact]
        public async Task DeleteAsync_given_non_existing_fromGroup_and_toGroup_returns_false()
        {
            var context = GetInMemoryDB(nameof(DeleteAsync_given_non_existing_fromGroup_and_toGroup_returns_false));
            var repository = new MatchRepository(context);

                var success = await repository.DeleteAsync(1, 2);

                Assert.False(success);
            
        }
        [Fact]
        public async Task UpdateMatch_return_true()
        {
            var context = GetInMemoryDB(nameof(UpdateMatch_return_true));
            var match1 = new Match { GroupOneId = 1, GroupTwoId = 2 };
            var match2 = new Match { GroupOneId = 3, GroupTwoId = 1 };
            var match3 = new Match { GroupOneId = 1, GroupTwoId = 4 };

            context.Matches.AddRange(match1, match2, match3);
            await context.SaveChangesAsync();

            var repository = new MatchRepository(context);

            var result = await repository.UpdateAsync(1, 5);

            var match = await context.Matches.FindAsync(5, 2);

            Assert.True(result);
            Assert.Equal(5, match.GroupOneId);
        }
        [Fact]
        public async Task UpdateMatch_return_false()
        {
            var context = GetInMemoryDB(nameof(UpdateMatch_return_false));
            var repository = new MatchRepository(context);

            var result = await repository.UpdateAsync(1, 5);

            Assert.False(result);
        }


    }
}
