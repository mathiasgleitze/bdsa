﻿using BDSA.Entities;
using BDSA.Shared;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Models.Tests
{
    public class StudentCourseRepositoryTests
    {
        [Fact]
        public async Task FindAsync_given_id_exists_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var student = new Student()
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                var course = new Course();
                context.Courses.Add(course);

                await context.SaveChangesAsync();

                var studentCourse = new StudentCourse
                {
                    CourseId = 1,
                    StudentId = 1
                };

                


                context.StudentCourses.Add(studentCourse);
                await context.SaveChangesAsync();

                var repository = new StudentCourseRepository(context);
                

                var result = await repository.FindAsync(1,1);

                Assert.Equal(studentCourse.StudentId, result.StudentId);

            }
        }

        [Fact]
        public async Task FindAsync_given_id_not_exists_returns_null()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentCourseRepository(context);
                

                var result = await repository.FindAsync(1,2);

                Assert.Null(result);
            }
        }
        [Fact]
        public async Task Create_given_dto_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var student = new Student()
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                var course = new Course();
                context.Courses.Add(course);

                await context.SaveChangesAsync();

                var dto = new StudentCourseDTO
                {
                    CourseId = 1,
                    StudentId = 1
                };

                var repository = new StudentCourseRepository(context);

                var result = await repository.CreateAsync(dto);

                Assert.Equal(dto.CourseId, result.CourseId);
                Assert.Equal(dto.StudentId, result.StudentId);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_exists_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course
                {
                    Name = "Course"
                };
                context.Courses.Add(course);

                var student = new Student
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                await context.SaveChangesAsync();

                var enrolled = new StudentCourse
                {
                    CourseId = 1,
                    StudentId = 1
                };

                context.StudentCourses.Add(enrolled);
                await context.SaveChangesAsync();

                var repository = new StudentCourseRepository(context);
                var courseId = 1;
                var studentId = 1;

                var result = await repository.DeleteAsync(studentId, courseId);

                Assert.True(result);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_not_exists_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentCourseRepository(context);
                var courseId = 1;
                var studentId = 1;

                var result = await repository.DeleteAsync(studentId, courseId);

                Assert.False(result);
            }
        }
        [Fact]
        public async Task Read_return_all_as_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var student1 = new Student()
                {
                    Name = "Student"
                };
                var student2 = new Student()
                {
                    Name = "Student2"
                };
                context.Students.AddRange(student1,student2);

                var course = new Course();
                context.Courses.Add(course);

                await context.SaveChangesAsync();

                var studentCourse1 = new StudentCourse
                {
                    CourseId = course.Id,
                    StudentId = student1.Id
                };
                var studentCourse2 = new StudentCourse
                {
                    CourseId = course.Id,
                    StudentId = student2.Id
                };

                context.StudentCourses.AddRange(studentCourse1,studentCourse2);
                await context.SaveChangesAsync();

                var repository = new StudentCourseRepository(context);
                var result = repository.Read();

                Assert.Equal(2, await result.CountAsync());
                Assert.Equal(1, (await result.FirstAsync()).StudentId);
            }
        }

        [Fact]
        public async Task FindCourses_given_id_exists_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var student = new Student()
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                var course = new Course();
                context.Courses.Add(course);

                await context.SaveChangesAsync();

                var enrolled = new StudentCourse
                {
                    CourseId = course.Id,
                    StudentId = student.Id
                };

                context.StudentCourses.Add(enrolled);
                await context.SaveChangesAsync();

                var repository = new StudentCourseRepository(context);
                var studentId = 1;

                var query = repository.FindCourses(studentId);

                var result = await query.SingleOrDefaultAsync();
                
                Assert.Equal(course.Id, result.Id);
                Assert.Null(result.Name);
            }
        }

        [Fact]
        public async Task FindCourses_given_id_not_exists_returns_empty_collection()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentCourseRepository(context);
                var id = 1;

                var result = repository.FindCourses(id);

                Assert.Empty(result);
            }
        }

        [Fact]
        public async Task FindStudents_given_id_exists_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var student = new Student()
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                var course = new Course();
                context.Courses.Add(course);

                await context.SaveChangesAsync();

                var enrolled = new StudentCourse
                {
                    CourseId = course.Id,
                    StudentId = student.Id
                };

                context.StudentCourses.Add(enrolled);
                await context.SaveChangesAsync();

                var repository = new StudentCourseRepository(context);
                var courseId = 1;

                var query = repository.FindStudents(courseId);

                var result = await query.SingleOrDefaultAsync();

                Assert.Equal(student.Id, result.Id);
                Assert.Equal(student.Name, result.Name);
            }
        }

        [Fact]
        public async Task FindStudents_given_id_not_exists_returns_empty_collection()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentCourseRepository(context);
                var id = 1;

                var result = repository.FindStudents(id);

                Assert.Empty(result);
            }
        }
        
        private async Task<DbConnection> CreateConnectionAsync()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            await connection.OpenAsync();

            return connection;
        }

        private async Task<IGroupItContext> CreateContextAsync(DbConnection connection)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                              .UseSqlite(connection);

            var context = new GroupItContext(builder.Options);
            await context.Database.EnsureCreatedAsync();

            return context;
        }
    }
}