﻿using BDSA.Entities;
using BDSA.Shared;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data.Common;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Models.Tests
{
    public class CourseRepositoryTests
    {
        [Fact]
        public async Task Create_given_dto_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var dto = new CourseDTO();

                var repository = new CourseRepository(context);

                var result = await repository.CreateAsync(dto);

                Assert.Equal(1, result.Id);
                Assert.Equal(dto.Name, result.Name);
            }
        }

        [Fact]
        public async Task FindAsync_given_id_exists_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course();

                context.Courses.Add(course);
                await context.SaveChangesAsync();

                var repository = new CourseRepository(context);
                var id = 1;

                var result = await repository.FindAsync(id);

                Assert.Equal(course.Id, result.Id);
                Assert.Equal(course.Name, result.Name);
            }
        }

        [Fact]
        public async Task FindAsync_given_id_not_exists_returns_null()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new CourseRepository(context);
                var id = 1;

                var result = await repository.FindAsync(id);

                Assert.Null(result);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_exists_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course();

                context.Courses.Add(course);
                await context.SaveChangesAsync();

                var repository = new CourseRepository(context);
                var id = 1;

                var result = await repository.DeleteAsync(id);

                Assert.True(result);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_not_exists_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new CourseRepository(context);
                var id = 1;

                var result = await repository.DeleteAsync(id);

                Assert.False(result);
            }
        }

        [Fact]
        public async Task UpdateAsync_given_id_exists_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course
                {
                    Name = "Course"
                };

                context.Courses.Add(course);
                await context.SaveChangesAsync();

                var dto = new CourseDTO
                {
                    Id = 1,
                    Name = "Awesome course"
                };

                var repository = new CourseRepository(context);

                var result = await repository.UpdateAsync(dto);

                Assert.True(result);

                var entity = await repository.FindAsync(dto.Id);

                Assert.Equal(dto.Id, entity.Id);
                Assert.Equal(dto.Name, entity.Name);
            }
        }

        [Fact]
        public async Task Update_given_id_not_exists_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var dto = new CourseDTO
                {
                    Id = 1,
                    Name = "Awesome course"
                };

                var repository = new CourseRepository(context);

                var result = await repository.UpdateAsync(dto);

                Assert.False(result);
            }
        }

        private async Task<DbConnection> CreateConnectionAsync()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            await connection.OpenAsync();

            return connection;
        }

        private async Task<IGroupItContext> CreateContextAsync(DbConnection connection)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                              .UseSqlite(connection);

            var context = new GroupItContext(builder.Options);
            await context.Database.EnsureCreatedAsync();

            return context;
        }
    }
}
