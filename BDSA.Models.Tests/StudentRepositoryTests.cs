﻿using BDSA.Shared;
using BDSA.Models;
using System;
using System.Threading.Tasks;
using Xunit;
using BDSA.Entities;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;
using System.Linq;

namespace BDSA.Models.Tests
{
    public class StudentRepositoryTests
    {
        [Fact]
        public async Task CreateAsync_given_dto_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentRepository(context);
                var dto = new StudentDTO
                {
                    Name = "Emil"
                };

                var Result = await repository.CreateAsync(dto);

                Assert.Equal(context.Students.First().Name, dto.Name);
                Assert.True(Result != null);
            }
        }

        [Fact]
        public async Task Find_given_studentId_returns_StudentDTO()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentRepository(context);
                var student = new Student
                {
                    Name = "Emil"
                };
                context.Students.Add(student);
                await context.SaveChangesAsync();

                var id = context.Students.First().Id;

                var result = await repository.FindAsync(id);

                Assert.Equal(result.Name, student.Name);
            }
        }

        [Fact]
        public async Task FindStudentsByName_given_existing_name_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentRepository(context);
                var student = new Student
                {
                    Name = "Emil"
                };

                context.Students.Add(student);
                await context.SaveChangesAsync();

                var ids = repository.FindStudentsByName("Emil");

                var result = await ids.FirstOrDefaultAsync();

                Assert.Equal(student.Id, result.Id);
            }
        }

        [Fact]
        public async Task FindStudentsByName_given_uncomplete_name_returns_dtos()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentRepository(context);

                var firstStudent = new Student
                {
                    Name = "Emily"
                };

                var secondStudent = new Student
                {
                    Name = "Emil"
                };

                context.Students.AddRange(firstStudent, secondStudent);
                await context.SaveChangesAsync();

                var ids = repository.FindStudentsByName("Emi");

                var first = await ids.FirstOrDefaultAsync();

                Assert.Equal(secondStudent.Id, first.Id);
                Assert.Equal(secondStudent.Name, first.Name);
            }
        }

        [Fact]
        public async Task FindStudentsByName_given_non_existing_name_returns_null()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentRepository(context);

                var result = repository.FindStudentsByName("Emi");

                Assert.Empty(result);
            }
        }

        [Fact]
        public async Task FindIdByMail_given_existing_mail_returns_id()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentRepository(context);
                var student = new Student
                {
                    Name = "Emil",
                    Mail = "emil@itu.dk"
                };
                context.Students.Add(student);
                await context.SaveChangesAsync();

                var result = await repository.FindIdByMailAsync(student.Mail);

                Assert.Equal(student.Id, result);
            }
        }

        [Fact]
        public async Task FindIdByMail_given_non_existing_Mail_returns_0()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentRepository(context);

                var result = await repository.FindIdByMailAsync("emil@itu.dk");

                Assert.Equal(0, result);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_studentId_deletes_student()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new StudentRepository(context);
                var student = new Student
                {
                    Name = "Emil"
                };
                context.Students.Add(student);
                await context.SaveChangesAsync();

                var id = context.Students.First().Id;

                var result = await repository.DeleteAsync(id);

                Assert.True(result);
            }
        }

        private async Task<DbConnection> CreateConnectionAsync()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            await connection.OpenAsync();

            return connection;
        }

        private async Task<IGroupItContext> CreateContextAsync(DbConnection connection)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>().UseSqlite(connection);

            var context = new GroupItContext(builder.Options);
            await context.Database.EnsureCreatedAsync();

            return context;
        }
    }
}