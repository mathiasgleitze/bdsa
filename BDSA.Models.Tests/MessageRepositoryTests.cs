﻿using BDSA.Shared;
using BDSA.Models;
using System;
using System.Threading.Tasks;
using Xunit;
using BDSA.Entities;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;
using System.Linq;

namespace BDSA.Models.Tests
{
    public class MessageRepositoryTests
    {
        [Fact]
        public async Task CreateAsync_given_DTO_create_it()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new MessageRepository(context);

                var chat = new Chat();
                context.Chats.Add(chat);

                var student = new Student
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                await context.SaveChangesAsync();

                var message = new MessageDTO
                {
                    ChatId = chat.Id,
                    StudentId = student.Id,
                    Time = new DateTime(100000),
                    Text = "Welcome"

                };

                var key = await repository.CreateAsync(message);

                var entity = await context.Messages.FindAsync(key.ChatId, key.Time);
                Assert.Equal(student.Id, entity.StudentId);
                Assert.Equal(new DateTime(100000), entity.Time);
                Assert.Equal("Welcome", entity.Text);
            }
        }
        [Fact]
        public async Task FindAsync_return_postive()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new MessageRepository(context);

                var chat = new Chat();
                context.Chats.Add(chat);

                var student = new Student
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                await context.SaveChangesAsync();

                var message = new Message
                {
                    ChatId = chat.Id,
                    StudentId = student.Id,
                    Time = new DateTime(100000),
                    Text = "Welcome"

                };

                context.Messages.Add(message);

                await context.SaveChangesAsync();

                var enitity = await repository.FindAsync(message.ChatId, message.Time);

                Assert.Equal(message.ChatId, enitity.ChatId);
                Assert.Equal(message.StudentId, enitity.StudentId);
                Assert.Equal(message.Time, enitity.Time);
                Assert.Equal(message.Text, enitity.Text);
            }
        }
        [Fact]
        public async Task FindAsync_return_negative()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new MessageRepository(context);

                var enitity = await repository.FindAsync(1, new DateTime(100000));

                Assert.Null(enitity);
            }
        }
        
        [Fact]
        public async Task DeleteAsync_given_existing_chatId_and_time_deletes_it()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var chat = new Chat();
                context.Chats.Add(chat);

                var student = new Student
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                await context.SaveChangesAsync();

                var entity = new Message
                {
                    ChatId = chat.Id,
                    StudentId = student.Id,
                    Time = new DateTime(100000)
                };

                context.Messages.Add(entity);
                await context.SaveChangesAsync();

                var chatId = entity.ChatId;
                var time = entity.Time;

                var repository = new MessageRepository(context);

                var deleted = await repository.DeleteAsync(chatId, time);

                Assert.True(deleted);

                var deletedEntity = await context.Messages.FindAsync(chatId, time);

                Assert.Null(deletedEntity);
            }
        }
        [Fact]
        public async Task DeleteAsync_given_non_existing_chatId_and_time_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new MessageRepository(context);

                var success = await repository.DeleteAsync(1, new DateTime(100000));

                Assert.False(success);
            }
        }

        [Fact]
        public async Task DeleteMessagesAsync_given_existing_chat_id_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var chat = new Chat();
                context.Chats.Add(chat);

                var student = new Student
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                await context.SaveChangesAsync();

                var entity = new Message
                {
                    ChatId = chat.Id,
                    StudentId = student.Id,
                    Time = new DateTime(100000)
                };

                context.Messages.Add(entity);
                await context.SaveChangesAsync();

                var repository = new MessageRepository(context);

                var result = await repository.DeleteMessagesAsync(chat.Id);

                Assert.True(result);
            }
        }

        [Fact]
        public async Task DeleteMessagesAsync_given_non_existing_chat_id_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new MessageRepository(context);

                var result = await repository.DeleteMessagesAsync(1);

                Assert.False(result);
            }
        }

        [Fact]
        public async Task Read_given_chatid_returns_all_message_in_that_chat()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var chat = new Chat();
                context.Chats.Add(chat);

                var firstStudent = new Student
                {
                    Name = "First student"
                };

                var secondStudent = new Student
                {
                    Name = "Second student"
                };

                context.Students.AddRange(firstStudent, secondStudent);

                await context.SaveChangesAsync();

                var message1 = new Message
                {
                    ChatId = chat.Id,
                    StudentId = firstStudent.Id,
                    Time = new DateTime(100000)
                };

                var message2 = new Message
                {
                    ChatId = chat.Id,
                    StudentId = secondStudent.Id,
                    Time = new DateTime(100001)
                };

                context.Messages.AddRange(message1, message2);

                await context.SaveChangesAsync();

                var repository = new MessageRepository(context);

                var messageDTOs = repository.Read(1);

                var message = await messageDTOs.FirstOrDefaultAsync();

                Assert.Equal(2, messageDTOs.Count());
                Assert.Equal(message1.ChatId, message.ChatId);
            }
        }



        private async Task<DbConnection> CreateConnectionAsync()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            await connection.OpenAsync();

            return connection;
        }

        private async Task<IGroupItContext> CreateContextAsync(DbConnection connection)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                              .UseSqlite(connection);

            var context = new GroupItContext(builder.Options);
            await context.Database.EnsureCreatedAsync();

            return context;
        }
    }
}