﻿using BDSA.Entities;
using BDSA.Shared;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Models.Tests
{
    public class ProjectRepositoryTests
    {
        [Fact]
        public async Task Create_given_dto_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course
                {
                    Name = "Course"
                };
                context.Courses.Add(course);
                await context.SaveChangesAsync();

                var dto = new ProjectDTO
                {
                    CourseId = 1
                };

                var repository = new ProjectRepository(context);

                var result = await repository.CreateAsync(dto);

                Assert.Equal(1, result.Id);
                Assert.Equal(dto.CourseId, result.CourseId);
            }
        }

        [Fact]
        public async Task FindAsync_given_id_exists_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course
                {
                    Name = "Course"
                };
                context.Courses.Add(course);
                

                var project = new Project
                {
                    CourseId = 1
                };
                context.Projects.Add(project);

                await context.SaveChangesAsync();

                var repository = new ProjectRepository(context);
                var id = 1;

                var result = await repository.FindAsync(id);

                Assert.Equal(project.Id, result.Id);
                Assert.Equal(project.CourseId, result.CourseId);
            }
        }

        [Fact]
        public async Task FindAsync_given_id_not_exists_returns_null()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new ProjectRepository(context);
                var id = 1;

                var result = await repository.FindAsync(id);

                Assert.Null(result);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_exists_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course
                {
                    Name = "Course"
                };
                context.Courses.Add(course);

                var project = new Project
                {
                    CourseId = 1
                };
                context.Projects.Add(project);

                await context.SaveChangesAsync();

                var repository = new ProjectRepository(context);
                var id = 1;

                var result = await repository.DeleteAsync(id);

                Assert.True(result);

                var entity = await repository.FindAsync(id);

                Assert.Null(entity);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_not_exists_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new ProjectRepository(context);
                var id = 1;

                var result = await repository.DeleteAsync(id);

                Assert.False(result);
            }
        }

        [Fact]
        public async Task UpdateAsync_given_id_exists_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var firstCourse = new Course
                {
                    Name = "First course"
                };

                var secondCourse = new Course
                {
                    Name = "Second course"
                };

                context.Courses.AddRange(firstCourse, secondCourse);

                var project = new Project
                {
                    CourseId = 1
                };
                context.Projects.Add(project);

                await context.SaveChangesAsync();

                var dto = new ProjectDTO
                {
                    Id = 1,
                    CourseId = 2
                };

                var repository = new ProjectRepository(context);

                var result = await repository.UpdateAsync(dto);

                Assert.True(result);

                var entity = await repository.FindAsync(dto.Id);

                Assert.Equal(dto.Id, entity.Id);
                Assert.Equal(dto.CourseId, entity.CourseId);
            }
        }

        [Fact]
        public async Task Update_given_id_not_exists_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var dto = new ProjectDTO
                {
                    Id = 1,
                    CourseId = 1
                };

                var repository = new ProjectRepository(context);

                var result = await repository.UpdateAsync(dto);

                Assert.False(result);
            }
        }

        private async Task<DbConnection> CreateConnectionAsync()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            await connection.OpenAsync();

            return connection;
        }

        private async Task<IGroupItContext> CreateContextAsync(DbConnection connection)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                              .UseSqlite(connection);

            var context = new GroupItContext(builder.Options);
            await context.Database.EnsureCreatedAsync();

            return context;
        }
    }
}
