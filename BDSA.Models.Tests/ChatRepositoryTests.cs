﻿using BDSA.Entities;
using BDSA.Shared;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Data.Common;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Models.Tests
{
    public class ChatRepositoryTests
    {
        [Fact]
        public async Task Create_given_dto_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var dto = new ChatDTO
                {
                    Id = 1
                };

                var repository = new ChatRepository(context);

                var result = await repository.CreateAsync(dto);

                Assert.Equal(1, result.Id);
             
            }
        }

        [Fact]
        public async Task FindAsync_given_id_exists_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var chat = new Chat();
                

                context.Chats.Add(chat);
                await context.SaveChangesAsync();

                var repository = new ChatRepository(context);
                var id = 1;

                var result = await repository.FindAsync(id);

                Assert.Equal(chat.Id, result.Id);
  
            }
        }

        [Fact]
        public async Task FindAsync_given_id_not_exists_returns_null()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new ChatRepository(context);
                var id = 1;

                var result = await repository.FindAsync(id);

                Assert.Null(result);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_exists_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var chat = new Chat();
                

                context.Chats.Add(chat);
                await context.SaveChangesAsync();

                var repository = new ChatRepository(context);
                var id = 1;

                var result = await repository.DeleteAsync(id);

                Assert.True(result);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_not_exists_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new ChatRepository(context);
                var id = 1;

                var result = await repository.DeleteAsync(id);

                Assert.False(result);
            }
        }

        

        private async Task<DbConnection> CreateConnectionAsync()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            await connection.OpenAsync();

            return connection;
        }

        private async Task<IGroupItContext> CreateContextAsync(DbConnection connection)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                              .UseSqlite(connection);

            var context = new GroupItContext(builder.Options);
            await context.Database.EnsureCreatedAsync();

            return context;
        }
    }
}
