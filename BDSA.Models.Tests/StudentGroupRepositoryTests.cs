﻿using BDSA.Entities;
using BDSA.Shared;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Models.Tests
{
    public class StudentGroupRepositoryTests
    {
        public IGroupItContext GetInMemoryDB(string dbName)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                    .UseInMemoryDatabase(databaseName: dbName);
            return new GroupItContext(builder.Options);
        }
        [Fact]
        public async Task FindAsync_given_id_exists_returns_dto()
        {
            var context = GetInMemoryDB(nameof(FindAsync_given_id_exists_returns_dto));
            var student = new Student()
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                var group = new Group();
                context.Groups.Add(group);

                await context.SaveChangesAsync();

                var studentGroup = new StudentGroup
                {
                    GroupId = 1,
                    StudentId = 1
                };

                


                context.StudentGroups.Add(studentGroup);
                await context.SaveChangesAsync();

                var repository = new StudentGroupRepository(context);
                

                var result = await repository.FindAsync(1,1);

                Assert.Equal(studentGroup.StudentId, result.StudentId);

            
        }

        [Fact]
        public async Task FindAsync_given_id_not_exists_returns_null()
        {
            var context = GetInMemoryDB(nameof(FindAsync_given_id_not_exists_returns_null));
            var repository = new StudentGroupRepository(context);
                

                var result = await repository.FindAsync(1,2);

                Assert.Null(result);
            
        }
        [Fact]
        public async Task Create_given_dto_returns_dto()
        {
            var context = GetInMemoryDB(nameof(Create_given_dto_returns_dto));
            var student = new Student()
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                var group = new Group();
                context.Groups.Add(group);

                await context.SaveChangesAsync();

                var dto = new StudentGroupDTO
                {
                    GroupId = 1,
                    StudentId = 1
                };

                var repository = new StudentGroupRepository(context);

                var result = await repository.CreateAsync(dto);

                Assert.Equal(dto.GroupId, result.GroupId);
                Assert.Equal(dto.StudentId, result.StudentId);
            
        }

        [Fact]
        public async Task DeleteAsync_given_id_exists_returns_true()
        {
            var context = GetInMemoryDB(nameof(DeleteAsync_given_id_exists_returns_true));
            var group = new Group();
                
                context.Groups.Add(group);

                var student = new Student
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                await context.SaveChangesAsync();

                var enrolled = new StudentGroup
                {
                    GroupId = 1,
                    StudentId = 1
                };

                context.StudentGroups.Add(enrolled);
                await context.SaveChangesAsync();

                var repository = new StudentGroupRepository(context);
                var groupId = 1;
                var studentId = 1;

                var result = await repository.DeleteAsync(studentId, groupId);

                Assert.True(result);
            
        }

        [Fact]
        public async Task DeleteAsync_given_id_not_exists_returns_false()
        {
            var context = GetInMemoryDB(nameof(DeleteAsync_given_id_not_exists_returns_false));
            var repository = new StudentGroupRepository(context);
                var groupId = 1;
                var studentId = 1;

                var result = await repository.DeleteAsync(studentId, groupId);

                Assert.False(result);
            
        }
        [Fact]
        public async Task Read_return_all_as_dto()
        {
            var context = GetInMemoryDB(nameof(Read_return_all_as_dto));
           

                var studentGroup1 = new StudentGroup
                {
                    GroupId = 1,
                    StudentId = 2
                };
                var studentGroup2 = new StudentGroup
                {
                    GroupId =2,
                    StudentId = 3
                };

                context.StudentGroups.AddRange(studentGroup1,studentGroup2);
                await context.SaveChangesAsync();

                var repository = new StudentGroupRepository(context);
                var result = repository.Read();

                Assert.Equal(2, await result.CountAsync());
                Assert.Equal(2, (await result.FirstAsync()).StudentId);
            
        }

        [Fact]
        public async Task FindGroups_given_id_exists_returns_dtos()
        {
            var context = GetInMemoryDB(nameof(FindGroups_given_id_exists_returns_dtos));
            var student = new Student()
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                var group = new Group();
                context.Groups.Add(group);

                await context.SaveChangesAsync();

                var enrolled = new StudentGroup
                {
                    GroupId = group.Id,
                    StudentId = student.Id
                };

                context.StudentGroups.Add(enrolled);
                await context.SaveChangesAsync();

                var repository = new StudentGroupRepository(context);
                var studentId = 1;

                var query = repository.FindGroups(studentId);

                var result = await query.SingleOrDefaultAsync();
                
                Assert.Equal(group.Id, result.Id);
            
        }

        [Fact]
        public async  Task FindGroups_given_id_not_exists_returns_empty_collection()
        {
            var context = GetInMemoryDB(nameof(FindGroups_given_id_not_exists_returns_empty_collection));
            var repository = new StudentGroupRepository(context);
                var id = 1;

                var result = repository.FindGroups(id);

                Assert.Empty(result);
            
        }

        [Fact]
        public async Task FindStudents_given_id_exists_returns_dtos()
        {
            var context = GetInMemoryDB(nameof(FindStudents_given_id_exists_returns_dtos));
            var student = new Student()
                {
                    Id=1,
                    Name = "Student"
                };
                context.Students.Add(student);

                var group = new Group();
                context.Groups.Add(group);

                await context.SaveChangesAsync();

                var enrolled = new StudentGroup
                {
                    GroupId = 1,
                    StudentId =1
                };

                context.StudentGroups.Add(enrolled);
                await context.SaveChangesAsync();

                var repository = new StudentGroupRepository(context);
                var groupId = 1;

                var query = repository.FindStudents(groupId);

                var result = await query.FirstOrDefaultAsync();

                Assert.Equal(1, result.Id);
                Assert.Equal(student.Name, result.Name);
            
        }

        [Fact]
        public async Task FindStudents_given_id_not_exists_returns_empty_collection()
        {
            var context = GetInMemoryDB(nameof(FindStudents_given_id_not_exists_returns_empty_collection));
            var repository = new StudentGroupRepository(context);
                var id = 1;

                var result = repository.FindStudents(id);

                Assert.Empty(result);
            
        }
        
        
    }
}