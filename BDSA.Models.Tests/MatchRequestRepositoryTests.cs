﻿using BDSA.Entities;
using BDSA.Shared;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Models.Tests
{
    public class MatchRequestRepositoryTests
    {
        public IGroupItContext GetInMemoryDB(string dbName)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                    .UseInMemoryDatabase(databaseName: dbName);
            return new GroupItContext(builder.Options);
        }
        [Fact]
        public async Task CreateAsync_given_DTO_create_it()
        {
            var context = GetInMemoryDB(nameof(CreateAsync_given_DTO_create_it));
            var repository = new MatchRequestRepository(context);

                var request = new MatchRequestDTO
                {
                    FromGroup = 1,
                    ToGroup = 2
                };

                var key = await repository.CreateAsync(request);

                var entity = await context.MatchRequests.FindAsync(key.FromGroup, key.ToGroup);
                Assert.Equal(1, entity.FromGroup);
                Assert.Equal(2, entity.ToGroup);
            
        }
        [Fact]
        public async Task FindAsync_return_postive()
        {
            var context = GetInMemoryDB(nameof(FindAsync_return_postive));
            var repository = new MatchRequestRepository(context);
                var request = new MatchRequest
                {
                    FromGroup = 1,
                    ToGroup = 2
                };
                context.MatchRequests.Add(request);
                await context.SaveChangesAsync();
                var entity = await repository.FindAsync(request.FromGroup, request.ToGroup);
                Assert.Equal(1, entity.FromGroup);
                Assert.Equal(2, entity.ToGroup);
            
        }
        [Fact]
        public async Task FindAsync_return_negative()
        {
            var context = GetInMemoryDB(nameof(FindAsync_return_negative));
            var repository = new MatchRequestRepository(context);
                var entity = await repository.FindAsync(3, 1);
                Assert.Null(entity);
            
        }
       

        [Fact]
        public async Task DeleteAll_returns_false()
        {
            var context = GetInMemoryDB(nameof(DeleteAll_returns_false));
            var repository = new MatchRequestRepository(context);
            var deleted = await repository.DeleteAll(1);

            Assert.False(deleted);
        }
        [Fact]
        public async Task DeleteAll_returns_true()
        {
            var context = GetInMemoryDB(nameof(DeleteAll_returns_true));
            context.MatchRequests.AddRange(new MatchRequest { FromGroup = 1, ToGroup = 2 },new MatchRequest {FromGroup = 3,ToGroup = 1});
            await context.SaveChangesAsync();

            var repository = new MatchRequestRepository(context);

            var deleted = await repository.DeleteAll(1);

            Assert.True(deleted);
        }

        [Fact]
        public async Task DeleteAsync_given_existing_fromGroup_and_toGroup_deletes_it()
        {
            var context = GetInMemoryDB(nameof(DeleteAsync_given_existing_fromGroup_and_toGroup_deletes_it));
            var entity = new MatchRequest { FromGroup = 1, ToGroup = 2 };

                context.MatchRequests.Add(entity);
                await context.SaveChangesAsync();
               
                var repository = new MatchRequestRepository(context);

                var deleted = await repository.DeleteAsync(entity.FromGroup, entity.ToGroup);

                Assert.True(deleted);

                var deletedEntity = await context.MatchRequests.FindAsync(entity.FromGroup, entity.ToGroup);

                Assert.Null(deletedEntity);
            
        }
        [Fact]
        public async Task DeleteAsync_given_non_existing_fromGroup_and_toGroup_returns_false()
        {
            var context = GetInMemoryDB(nameof(DeleteAsync_given_non_existing_fromGroup_and_toGroup_returns_false));
            var repository = new MatchRequestRepository(context);

                var success = await repository.DeleteAsync(1, 2);

                Assert.False(success);
            
        }
        [Fact]
        public async Task ReadFrom_given_groupid_returns_all_request_for_that_group()
        {
            var context = GetInMemoryDB(nameof(ReadFrom_given_groupid_returns_all_request_for_that_group));
            var request1 = new MatchRequest { FromGroup = 1, ToGroup = 2 };
                var request2 = new MatchRequest { FromGroup = 3, ToGroup = 1 };
                var request3 = new MatchRequest { FromGroup = 1, ToGroup = 4 };

                context.MatchRequests.AddRange(request1, request2, request3);


                await context.SaveChangesAsync();

                var repository = new MatchRequestRepository(context);

                var matchRequestDTOs = repository.ReadFrom(1);

                var request = await matchRequestDTOs.FirstOrDefaultAsync();

                Assert.Equal(2, matchRequestDTOs.Count());
                Assert.Equal(1, request.FromGroup);
            
        }
        [Fact]
        public async Task ReadTo_given_groupid_returns_all_request_for_that_group()
        {
            var context = GetInMemoryDB(nameof(ReadTo_given_groupid_returns_all_request_for_that_group));
            var request1 = new MatchRequest { FromGroup = 1, ToGroup = 2 };
            var request2 = new MatchRequest { FromGroup = 3, ToGroup = 1 };
            var request3 = new MatchRequest { FromGroup = 1, ToGroup = 4 };

            context.MatchRequests.AddRange(request1, request2, request3);


            await context.SaveChangesAsync();

            var repository = new MatchRequestRepository(context);

            var matchRequestDTOs = repository.ReadTo(1);

            var request = await matchRequestDTOs.FirstOrDefaultAsync();

            Assert.Equal(1, matchRequestDTOs.Count());
            Assert.Equal(1, request.ToGroup);

        }
        [Fact]
        public void ReadFrom_returns_null()
        {
            var context = GetInMemoryDB(nameof(ReadFrom_returns_null));
            var repository = new MatchRequestRepository(context);
            var result = repository.ReadFrom(1);

            Assert.Empty(result);
        }
        [Fact]
        public void ReadTo_returns_null()
        {
            var context = GetInMemoryDB(nameof(ReadTo_returns_null));
            var repository = new MatchRequestRepository(context);
            var result = repository.ReadTo(1);

            Assert.Empty(result);
        }

    }
    
}
