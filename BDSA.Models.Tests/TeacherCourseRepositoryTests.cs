﻿using BDSA.Entities;
using BDSA.Shared;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Models.Tests
{
    public class TeacherCourseRepositoryTests
    {
        [Fact]
        public async Task Read_return_all_as_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var teacher1 = new Teacher()
                {
                    Name = "Teacher"
                };
                var teacher2 = new Teacher()
                {
                    Name = "Teacher2"
                };
                context.Teachers.AddRange(teacher1, teacher2);

                var course = new Course();
                context.Courses.Add(course);

                await context.SaveChangesAsync();

                var teacherCourse1 = new TeacherCourse
                {
                    CourseId = course.Id,
                    TeacherId = teacher1.Id
                };
                var teacherCourse2 = new TeacherCourse
                {
                    CourseId = course.Id,
                    TeacherId = teacher2.Id
                };

                context.TeacherCourses.AddRange(teacherCourse1, teacherCourse2);
                await context.SaveChangesAsync();

                var repository = new TeacherCourseRepository(context);
                var result = repository.Read();

                Assert.Equal(2, await result.CountAsync());
                Assert.Equal(1, (await result.FirstAsync()).TeacherId);
            }
        }
        [Fact]
        public async Task FindAsync_given_id_exists_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var teacher = new Teacher()
                {
                    Name = "Teacher"
                };
                context.Teachers.Add(teacher);

                var course = new Course();
                context.Courses.Add(course);

                await context.SaveChangesAsync();

                var teacherCourse = new TeacherCourse
                {
                    CourseId = 1,
                    TeacherId = 1
                };




                context.TeacherCourses.Add(teacherCourse);
                await context.SaveChangesAsync();

                var repository = new TeacherCourseRepository(context);


                var result = await repository.FindAsync(1, 1);

                Assert.Equal(teacherCourse.TeacherId, result.TeacherId);

            }
        }

        [Fact]
        public async Task FindAsync_given_id_not_exists_returns_null()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new TeacherCourseRepository(context);


                var result = await repository.FindAsync(1, 2);

                Assert.Null(result);
            }
        }


        [Fact]
        public async Task Create_given_dto_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var teacher = new Teacher();
                context.Teachers.Add(teacher);

                var course = new Course();
                context.Courses.Add(course);

                await context.SaveChangesAsync();

                var dto = new TeacherCourseDTO
                {
                    TeacherId = teacher.Id,
                    CourseId = course.Id
                };

                var repository = new TeacherCourseRepository(context);

                var result = await repository.CreateAsync(dto);

                Assert.Equal(dto.TeacherId, result.TeacherId);
                Assert.Equal(dto.CourseId, result.CourseId);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_existing_id_deletes_it()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var teacher = new Teacher();
                context.Teachers.Add(teacher);

                var course = new Course();
                context.Courses.Add(course);

                var teacherCourse = new TeacherCourse
                {
                    TeacherId = teacher.Id,
                    CourseId = course.Id
                };
                context.TeacherCourses.Add(teacherCourse);

                await context.SaveChangesAsync();

                var repository = new TeacherCourseRepository(context);

                var result = await repository.DeleteAsync(teacher.Id, course.Id);

                Assert.True(result);

                var entity = await context.TeacherCourses.FindAsync(teacher.Id, course.Id);

                Assert.Null(entity);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_id_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new TeacherCourseRepository(context);

                var result = await repository.DeleteAsync(1, 1);

                Assert.False(result);
            }
        } 

        [Fact]
        public async Task FindCourses_given_id_exists_returns_dtos()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var teacher = new Teacher();
                context.Teachers.Add(teacher);

                var course = new Course
                {
                    Name = "Course"
                };
                context.Courses.Add(course);

                var teacherCourse = new TeacherCourse
                {
                    TeacherId = teacher.Id,
                    CourseId = course.Id
                };
                context.TeacherCourses.Add(teacherCourse);

                await context.SaveChangesAsync();

                var repository = new TeacherCourseRepository(context);

                var query = repository.FindCourses(teacher.Id);

                var result = await query.SingleOrDefaultAsync();

                Assert.Equal(course.Id, result.Id);
                Assert.Equal(course.Name, result.Name);
            }
        }

        [Fact]
        public async Task FindCourses_given_non_existing_id_returns_empty_collection()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new TeacherCourseRepository(context);

                var result = repository.FindCourses(1);

                Assert.Empty(result);
            }
        }

        [Fact]
        public async Task FindTeachers_given_id_exists_returns_dtos()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var teacher = new Teacher()
                {
                    Name = "Teacher"
                };
                context.Teachers.Add(teacher);

                var course = new Course();
                context.Courses.Add(course);

                var teacherCourse = new TeacherCourse
                {
                    TeacherId = teacher.Id,
                    CourseId = course.Id
                };
                context.TeacherCourses.Add(teacherCourse);

                await context.SaveChangesAsync();

                var repository = new TeacherCourseRepository(context);

                var query = repository.FindTeachers(course.Id);

                var result = await query.SingleOrDefaultAsync();

                Assert.Equal(teacher.Id, result.Id);
                Assert.Equal(teacher.Name, result.Name);
            }
        }

        [Fact]
        public async Task FindTeachers_given_non_existing_id_returns_empty_collection()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new TeacherCourseRepository(context);

                var result = repository.FindTeachers(1);

                Assert.Empty(result);
            }
        }

        private async Task<DbConnection> CreateConnectionAsync()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            await connection.OpenAsync();

            return connection;
        }

        private async Task<IGroupItContext> CreateContextAsync(DbConnection connection)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                              .UseSqlite(connection);

            var context = new GroupItContext(builder.Options);
            await context.Database.EnsureCreatedAsync();

            return context;
        }
    }
}
