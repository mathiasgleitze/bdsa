using BDSA.Shared;
using BDSA.Models;
using System;
using System.Threading.Tasks;
using Xunit;
using BDSA.Entities;
using System.Data.Common;
using Microsoft.EntityFrameworkCore;
using Microsoft.Data.Sqlite;
using System.Linq;

namespace BDSA.Models.Tests
{
    public class GroupRepositoryTests
    {
        [Fact]
        public async Task CreateAsync_given_dto_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course();
                context.Courses.Add(course);

                var project = new Project
                {
                    CourseId = course.Id
                };
                context.Projects.Add(project);

                var chat = new Chat();
                context.Chats.Add(chat);

                await context.SaveChangesAsync();

                var repository = new GroupRepository(context);
                var dto = new GroupDTO
                {
                    Id = 12,
                    ProjectId = project.Id,
                    ChatId = chat.Id
                };

                var group = await repository.CreateAsync(dto);

                Assert.True(group);
            }
        }

        [Fact]
        public async Task UpdateAsync_given_non_existing_dto_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new GroupRepository(context);
                var dto = new GroupDTO
                {
                    Id = 123,
                    ProjectId = 42
                };

                var updated = await repository.UpdateAsync(dto);

                Assert.False(updated);
            }
        }

        [Fact]
        public async Task UpdateAsync_given_existing_dto_updates_entity()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course();
                context.Courses.Add(course);

                var project = new Project
                {
                    CourseId = course.Id
                };
                context.Projects.Add(project);

                var firstChat = new Chat();
                var secondChat = new Chat();
                context.Chats.AddRange(firstChat, secondChat);

                await context.SaveChangesAsync();

                var group = new Group {
                    Id = 123,
                    ProjectId = project.Id,
                    ChatId = firstChat.Id
                };

                context.Groups.Add(group);
                await context.SaveChangesAsync();

                var repository = new GroupRepository(context);
                var dto = new GroupDTO
                {
                    Id = 123,
                    ProjectId = project.Id,
                    ChatId = secondChat.Id
                };

                var updated = await repository.UpdateAsync(dto);

                Assert.True(updated);

                var entity = await context.Groups.FindAsync(123);

                Assert.Equal(secondChat.Id, entity.ChatId);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_existing_groupId_deletes_and_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course();
                context.Courses.Add(course);

                var project = new Project
                {
                    CourseId = course.Id
                };
                context.Projects.Add(project);

                var chat = new Chat();
                context.Chats.Add(chat);

                await context.SaveChangesAsync();

                var entity = new Group {
                    Id = 123,
                    ProjectId = project.Id,
                    ChatId = chat.Id
                };
                context.Groups.Add(entity);
                await context.SaveChangesAsync();

                var id = entity.Id;

                var repository = new GroupRepository(context);

                var deleted = await repository.DeleteAsync(id);

                Assert.True(deleted);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_actorId_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new GroupRepository(context);

                var deleted = await repository.DeleteAsync(42);

                Assert.False(deleted);
            }
        }
        [Fact]
        public async Task FindAsync_given_id_which_not_exists_returns_null()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new GroupRepository(context);


                var result = await repository.FindAsync(123);

                Assert.Null(result);
            }
        }
        [Fact]
        public async Task FindAsync_given_id_exists_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course();
                context.Courses.Add(course);

                var project = new Project
                {
                    CourseId = course.Id
                };
                context.Projects.Add(project);

                var chat = new Chat();
                context.Chats.Add(chat);

                await context.SaveChangesAsync();

                var entity = new Group
                {
                    Id = 123,
                    ProjectId = project.Id,
                    ChatId = chat.Id
                };

                context.Groups.Add(entity);
                await context.SaveChangesAsync();

                var repository = new GroupRepository(context);
               

                var result = await repository.FindAsync(123);

                Assert.Equal(entity.Id, result.Id);
                
            }
        }
        [Fact]
        public async Task Read_returns_empty()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new GroupRepository(context);
                var result = repository.Read();
                Assert.Empty(result);
            }
        }
        [Fact]
        public async Task Read_returns_dtos()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course1 = new Course();
                var course2 = new Course();
                context.Courses.AddRange(course1,course2);

                var project1 = new Project { CourseId = course1.Id };
                var project2 = new Project { CourseId = course2.Id };
                context.Projects.AddRange(project1,project2);

                var chat1 = new Chat();
                var chat2 = new Chat();
                context.Chats.AddRange(chat1,chat2);

                await context.SaveChangesAsync();

                var group1 = new Group
                {
                    Id = 123,
                    ProjectId = project1.Id,
                    ChatId = chat1.Id
                };
                var group2 = new Group
                {
                    Id = 154,
                    ProjectId = project2.Id,
                    ChatId = chat2.Id
                };

                context.Groups.AddRange(group1,group2);
                await context.SaveChangesAsync();

                var repository = new GroupRepository(context);


                var result = repository.Read();

                Assert.Equal(2,result.Count());
                Assert.Equal(123, result.FirstOrDefault().Id);

            }
        }

        private async Task<DbConnection> CreateConnectionAsync()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            await connection.OpenAsync();

            return connection;
        }

        private async Task<IGroupItContext> CreateContextAsync(DbConnection connection)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>().UseSqlite(connection);

            var context = new GroupItContext(builder.Options);
            await context.Database.EnsureCreatedAsync();

            return context;
        }
    }
}
