﻿using BDSA.Entities;
using BDSA.Shared;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Models.Tests
{
    public class ProjectProfileRepositoryTests
    {
        [Fact]
        public async Task Create_given_dto_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course
                {
                    Name = "Course"
                };
                context.Courses.Add(course);

                var project = new Project
                {
                    CourseId = 1
                };
                context.Projects.Add(project);

                var student = new Student
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                await context.SaveChangesAsync();

                var dto = new ProjectProfileDTO
                {
                    ProjectId = 1,
                    StudentId = 1
                };

                var repository = new ProjectProfileRepository(context);

                var result = await repository.CreateAsync(dto);

                Assert.Equal(dto.ProjectId, result.ProjectId);
                Assert.Equal(dto.StudentId, result.StudentId);
            }
        }

        [Fact]
        public async Task FindAsync_given_id_exists_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course
                {
                    Name = "Course"
                };
                context.Courses.Add(course);

                var project = new Project
                {
                    CourseId = course.Id
                };
                context.Projects.Add(project);

                var student = new Student
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                await context.SaveChangesAsync();

                var projectProfile = new ProjectProfile
                {
                    ProjectId = course.Id,
                    StudentId = student.Id
                };
                context.ProjectProfiles.Add(projectProfile);

                await context.SaveChangesAsync();

                var repository = new ProjectProfileRepository(context);

                var result = await repository.FindAsync(student.Id, project.Id);

                Assert.Equal(projectProfile.ProjectId, result.ProjectId);
                Assert.Equal(projectProfile.StudentId, result.StudentId);
            }
        }

        [Fact]
        public async Task FindAsync_given_id_not_exists_returns_null()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new ProjectProfileRepository(context);
                var projectId = 1;
                var studentId = 1;

                var result = await repository.FindAsync(studentId, projectId);

                Assert.Null(result);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_exists_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course
                {
                    Name = "Course"
                };
                context.Courses.Add(course);

                var project = new Project
                {
                    CourseId = course.Id
                };
                context.Projects.Add(project);

                var student = new Student
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                var projectProfile = new ProjectProfile
                {
                    ProjectId = project.Id,
                    StudentId = student.Id
                };
                context.ProjectProfiles.Add(projectProfile);

                await context.SaveChangesAsync();

                var repository = new ProjectProfileRepository(context);

                var result = await repository.DeleteAsync(student.Id, project.Id);

                Assert.True(result);

                var entity = await repository.FindAsync(student.Id, project.Id);

                Assert.Null(entity);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_not_exists_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new ProjectProfileRepository(context);
                var projectId = 1;
                var studentId = 1;

                var result = await repository.DeleteAsync(studentId, projectId);

                Assert.False(result);
            }
        }

        [Fact]
        public async Task FindProfileAsync_given_id_exists_returns_project_profiles()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course
                {
                    Name = "Course"
                };
                context.Courses.Add(course);

                var project = new Project
                {
                    CourseId = course.Id
                };
                context.Projects.Add(project);

                var student = new Student
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                var projectProfile = new ProjectProfile
                {
                    ProjectId = project.Id,
                    StudentId = student.Id
                };
                context.ProjectProfiles.Add(projectProfile);

                await context.SaveChangesAsync();

                var repository = new ProjectProfileRepository(context);

                var projectProfiles = repository.FindProjectProfilesAsync(student.Id);

                var result = await projectProfiles.SingleOrDefaultAsync();

                Assert.Equal(projectProfile.ProjectId, result.ProjectId);
                Assert.Equal(projectProfile.StudentId, result.StudentId);
            }
        }

        [Fact]
        public async Task FindProfileAsync_given_id_not_exists_returns_empty_collection()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var studentId = 1;

                var repository = new ProjectProfileRepository(context);

                var projectProfiles = repository.FindProjectProfilesAsync(studentId);

                Assert.Empty(projectProfiles);
            }
        }

        [Fact]
        public async Task UpdateAsync_given_dto_exists_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var course = new Course
                {
                    Name = "Course"
                };
                context.Courses.Add(course);

                var project = new Project
                {
                    CourseId = course.Id
                };
                context.Projects.Add(project);

                var student = new Student
                {
                    Name = "Student"
                };
                context.Students.Add(student);

                var projectProfile = new ProjectProfile
                {
                    ProjectId = project.Id,
                    StudentId = student.Id,
                    Ambitions = 1
                };
                context.ProjectProfiles.Add(projectProfile);

                await context.SaveChangesAsync();

                var dto = new ProjectProfileDTO
                {
                    ProjectId = 1,
                    StudentId = 1,
                    Ambitions = 10
                };

                var repository = new ProjectProfileRepository(context);

                var result = await repository.UpdateAsync(dto);

                Assert.True(result);

                var entity = await repository.FindAsync(dto.StudentId, dto.ProjectId);

                Assert.Equal(dto.Ambitions, entity.Ambitions);
            }
        }

        [Fact]
        public async Task Update_given_dto_not_exists_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var dto = new ProjectProfileDTO
                {
                    ProjectId = 1,
                    StudentId = 1,
                    Ambitions = 10
                };

                var repository = new ProjectProfileRepository(context);

                var result = await repository.UpdateAsync(dto);

                Assert.False(result);
            }
        }

        private async Task<DbConnection> CreateConnectionAsync()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            await connection.OpenAsync();

            return connection;
        }

        private async Task<IGroupItContext> CreateContextAsync(DbConnection connection)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                              .UseSqlite(connection);

            var context = new GroupItContext(builder.Options);
            await context.Database.EnsureCreatedAsync();

            return context;
        }
    }
}