﻿using BDSA.Entities;
using BDSA.Shared;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace BDSA.Models.Tests
{
    public class TeacherRepositoryTests
    {
        [Fact]
        public async Task Create_given_dto_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var dto = new TeacherDTO
                {
                    Name = "Teacher"
                };

                var repository = new TeacherRepository(context);

                var result = await repository.CreateAsync(dto);

                Assert.Equal(1, result.Id);
                Assert.Equal(dto.Name, result.Name);
            }
        }

        [Fact]
        public async Task FindAsync_given_id_exists_returns_dto()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var teacher = new Teacher
                {
                    Name = "Teacher"
                };

                context.Teachers.Add(teacher);
                await context.SaveChangesAsync();

                var repository = new TeacherRepository(context);

                var result = await repository.FindAsync(teacher.Id);

                Assert.Equal(teacher.Id, result.Id);
                Assert.Equal(teacher.Name, result.Name);
            }
        }

        [Fact]
        public async Task FindAsync_given_id_not_exists_returns_null()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new TeacherRepository(context);

                var result = await repository.FindAsync(1);

                Assert.Null(result);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_exists_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var teacher = new Teacher
                {
                    Name = "Teacher"
                };

                context.Teachers.Add(teacher);
                await context.SaveChangesAsync();

                var repository = new TeacherRepository(context);

                var result = await repository.DeleteAsync(teacher.Id);

                Assert.True(result);

                var entity = await repository.FindAsync(teacher.Id);

                Assert.Null(entity);
            }
        }

        [Fact]
        public async Task DeleteAsync_given_id_not_exists_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var repository = new TeacherRepository(context);

                var result = await repository.DeleteAsync(1);

                Assert.False(result);
            }
        }

        [Fact]
        public async Task UpdateAsync_given_id_exists_returns_true()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var teacher = new Teacher
                {
                    Name = "Teacher"
                };

                context.Teachers.Add(teacher);
                await context.SaveChangesAsync();

                var dto = new TeacherDTO
                {
                    Id = 1,
                    Name = "Awesome teacher"
                };

                var repository = new TeacherRepository(context);

                var result = await repository.UpdateAsync(dto);

                Assert.True(result);

                var entity = await repository.FindAsync(dto.Id);

                Assert.Equal(dto.Id, entity.Id);
                Assert.Equal(dto.Name, entity.Name);
            }
        }

        [Fact]
        public async Task Update_given_id_not_exists_returns_false()
        {
            using (var connection = await CreateConnectionAsync())
            using (var context = await CreateContextAsync(connection))
            {
                var dto = new TeacherDTO
                {
                    Id = 1,
                    Name = "Awesome teacher"
                };

                var repository = new TeacherRepository(context);

                var result = await repository.UpdateAsync(dto);

                Assert.False(result);
            }
        }

        private async Task<DbConnection> CreateConnectionAsync()
        {
            var connection = new SqliteConnection("DataSource=:memory:");
            await connection.OpenAsync();

            return connection;
        }

        private async Task<IGroupItContext> CreateContextAsync(DbConnection connection)
        {
            var builder = new DbContextOptionsBuilder<GroupItContext>()
                              .UseSqlite(connection);

            var context = new GroupItContext(builder.Options);
            await context.Database.EnsureCreatedAsync();

            return context;
        }
    }
}
