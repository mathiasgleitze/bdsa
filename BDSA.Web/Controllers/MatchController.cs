﻿using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MatchController : ControllerBase
    {
        private readonly IMatchRepository _repository;

        public MatchController(IMatchRepository repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Returns a list of Matches for a specific group
        /// </summary>
        /// <param name="groupId">Id of the group</param>
        /// <returns>A list of MatchDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{groupId}")]
        public async Task<ActionResult<IEnumerable<MatchDTO>>> Get(int groupId)
        {
            return await _repository.FindAllMatches(groupId).ToListAsync();
        }



        /// <summary>
        /// Returns a Match with a specific key
        /// </summary>
        /// <param name="groupOne">Id of the one group</param>
        /// <param name="groupTwo">Id of another group</param>
        /// <returns>A MatchDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{groupOne}/{groupTwo}")]
        public async Task<ActionResult<MatchDTO>> Get(int groupOne, int groupTwo)
        {
            var matchDTO = await _repository.FindAsync(groupOne, groupTwo);

            if (matchDTO == null)
            {
                return NotFound();
            }

            return matchDTO;
        }



        /// <summary>
        /// Creates a Match.
        /// </summary>
        /// <param name="matchDTO">Body for Match</param>
        /// <returns>A newly created Match</returns>
        /// <response code="201">Returns the newly created Match</response>
        /// <response code="400">If the item is null</response>            
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult<MatchDTO>> Post([FromBody] MatchDTO matchDTO)
        {
            var created = await _repository.CreateAsync(matchDTO);
            return CreatedAtAction(nameof(Get), new {  created.GroupOne, created.GroupTwo}, created);
        }
        /// <summary>
        /// Move Matches from one id to another.
        /// </summary>
        /// <param name="oldgroup">Id of the old group</param>
        /// <param name="newgroup">Id of the new group</param>
        /// <response code="204">In case of successful update</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpPut("update/{oldgroup}/{newgroup}")]
        public async Task<ActionResult> Put(int oldgroup, int newgroup)
        {
            var updated = await _repository.UpdateAsync(oldgroup, newgroup);

            if (updated)
            {
                return NoContent();
            }
            return NotFound();
        }


        /// <summary>
        /// Deletes a Match with a specific key.
        /// </summary>
        /// <param name="groupOne">Id of the fromGroup to delete</param>
        /// <param name="groupTwo">Id of the toGroup to delete</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{groupOne}/{groupTwo}")]
        public async Task<ActionResult> Delete(int groupOne, int groupTwo)
        {
            var deleted = await _repository.DeleteAsync(groupOne, groupTwo);

            if (deleted)
            {
                return NoContent();
            }

            return NotFound();
        }
    }
}

