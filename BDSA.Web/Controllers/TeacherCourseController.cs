﻿using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TeacherCourseController : ControllerBase
    {
        private readonly ITeacherCourseRepository _repository;

        public TeacherCourseController(ITeacherCourseRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Returns a list of all teachercourses 
        /// </summary>
        /// <returns>An IEnumerable(TeacherCourseDTO)</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If nothing is found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<TeacherCourseDTO>>> Get()
        {
            return await _repository.Read().ToListAsync();
        }

        /// <summary>
        /// Returns a list of teachers with a specific id
        /// </summary>
        /// <param name="id">Id of the teachers to be found</param>
        /// <returns>A list of teachers</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("teacher/{id}")]
        public async Task<ActionResult<IEnumerable<TeacherDTO>>> GetTeachers(int id)
        {
            var teacherList = await _repository.FindTeachers(id).ToListAsync();
            
            return teacherList;
        }

        /// <summary>
        /// Returns a list of courses with a specific id
        /// </summary>
        /// <param name="id">Id of the courses to be found</param>
        /// <returns>A list of courses</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("course/{id}")]
        public async Task<ActionResult<IEnumerable<CourseDTO>>>GetCourses(int id){
            var courseList = await _repository.FindCourses(id).ToListAsync();
            return courseList;
        }

        /// <summary>
        /// Returns a TeacherCourse with a specific key
        /// </summary>
        /// <param name="teacherId">Id of the teacherId to be found</param>
        /// <param name="courseId">Id of the courseId to be found</param>
        /// <returns>A TeacherCourseDTO</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{teacherId}/{courseId}")]
        public async Task<ActionResult<TeacherCourseDTO>> Get(int teacherId, int courseId)
        {
            var requestDTO = await _repository.FindAsync(teacherId, courseId);

            if (requestDTO == null)
            {
                return NotFound();
            }

            return requestDTO;
        }

        /// <summary>
        /// Creates a teachercourse.
        /// </summary>
        /// <param name="requestDTO">Body for teacher</param>
        /// <returns>A newly created TeacherCourse</returns>
        /// <response code="201">Returns the newly created teachercourse</response>
        /// <response code="400">If the item is null</response>  
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TeacherCourseDTO requestDTO)
        {
            var created = await _repository.CreateAsync(requestDTO);

            return CreatedAtAction(nameof(Get), created);
        }

        /// <summary>
        /// Deletes a teachercourse with a specific id.
        /// </summary>
        /// <param name="teacherId">Id of the teacher to delete</param>
        /// <param name="courseId">Id of the course to delete</param>
        /// <response code="204">In case of succesful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{studentId}/{courseId}")]
        public async Task<ActionResult> Delete(int teacherId, int courseId)
        {
            var deleted = await _repository.DeleteAsync(teacherId, courseId);

            if (deleted)
            {
                return NoContent();
            }

            return NotFound();
        }
    }
}

