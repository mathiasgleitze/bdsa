﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BDSA.Models;
using BDSA.Shared;
using BDSA.Web.Hubs;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MessageController : ControllerBase
    {
        private readonly IMessageRepository _repository;
        private readonly IHubContext<MessageHub> _hubContext;

        public MessageController(IMessageRepository repository,IHubContext<MessageHub> hubContext)
        {
            _repository = repository;
            _hubContext = hubContext;
        }

        /// <summary>
        /// Returns a Message with a specific key
        /// </summary>
        /// <param name="chatId">Id of the chat</param>
        /// <param name="time">Time of the message send</param>
        /// <returns>A MessageDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{chatId}/{time}")]
        public async Task<ActionResult<MessageDTO>> Get(int chatId,DateTime time)
        {
            var group = await _repository.FindAsync(chatId,time);

            if (group == null)
            {
                return NotFound();
            }

            return group;
        }

        /// <summary>
        /// Returns all the Message in a specific chat
        /// </summary>
        /// <param name="chatId">Id of the chat</param>
        /// <returns>A List of MessageDTOs</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{chatId}")]
        public async Task<ActionResult<IEnumerable<MessageDTO>>> Get(int chatId)
        {
           return await _repository.Read(chatId).ToListAsync();
            
        }



        /// <summary>
        /// Creates a Message
        /// </summary>
        /// <param name="message">Body for Message</param>
        /// <returns>A newly created Message</returns>
        /// <response code="201">Returns the newly created GroupRequest</response>
        /// <response code="400">If the item is null</response>            
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult<MessageDTO>> Post([FromBody] MessageDTO message)
        {
            await _hubContext.Clients.Group((message.ChatId).ToString()).SendAsync("ReceiveMessage", $"GET api/message/{message.ChatId}");
            var created = await _repository.CreateAsync(message);

            return CreatedAtAction(nameof(Get),new { created.ChatId,created.Time}, created);
        }


        /// <summary>
        /// Deletes a Message with a specific key.
        /// </summary>
        /// <param name="chatId">Id of the chat</param>
        /// <param name="time">Time of the message send</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{chatId}/{time}")]
        public async Task<ActionResult> Delete(int chatId,DateTime time)
        {
            await _hubContext.Clients.Group((chatId).ToString()).SendAsync("ReceiveMessage", $"GET api/message/{chatId}");
            var deleted = await _repository.DeleteAsync(chatId,time);

            if (deleted)
            {
                return NoContent();
            }

            return NotFound();
        }
        /// <summary>
        /// Deletes all Message from a specific chat.
        /// </summary>
        /// <param name="chatId">Id of the chat</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{chatId}")]
        public async Task<ActionResult> Delete(int chatId)
        {
            await _hubContext.Clients.Group((chatId).ToString()).SendAsync("ReceiveMessage", $"GET api/message/{chatId}");
            var deleted = await _repository.DeleteMessagesAsync(chatId);

            if (deleted)
            {
                return NoContent();
            }

            return NotFound();
        }
    }
}