﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Authorization;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TeacherController : ControllerBase
    {
        private readonly ITeacherRepository _repository;

        public TeacherController(ITeacherRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Returns a teacher with a specific id
        /// </summary>
        /// <param name="id">Id of the teacher to be found</param>
        /// <returns>A Teacher</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{id}")]
        public async Task<ActionResult<TeacherDTO>> Get(int id)
        {
            var entity = await _repository.FindAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            return entity;
        }

        /// <summary>
        /// Creates a teacher.
        /// </summary>
        /// <param name="teacher">Body for teacher</param>
        /// <returns>A newly created Teacher</returns>
        /// <response code="201">Returns the newly created teacher</response>
        /// <response code="400">If the item is null</response>  
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] TeacherDTO teacher)
        {
            var dto = await _repository.CreateAsync(teacher);
            return CreatedAtAction(nameof(Get), dto);
        }

        /// <summary>
        /// Update a teacher with a specific id.
        /// </summary>
        /// <param name="id">Id of the teacher to update</param>
        /// <param name="teacher">TeacherDTO with update data</param>
        /// <response code="204">In case of succesful update</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] TeacherDTO teacher)
        {
            var updated = await _repository.UpdateAsync(teacher);

            if (updated)
            {
                return NoContent();
            }
            return NotFound();
        }

        /// <summary>
        /// Deletes a teacher with a specific id.
        /// </summary>
        /// <param name="id">Id of the chat to delete</param>
        /// <response code="204">In case of succesful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await _repository.DeleteAsync(id);

            if (deleted)
            {
                return NoContent();
            }
            return NotFound();
        }
    }
}