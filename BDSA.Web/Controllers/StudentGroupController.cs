﻿using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StudentGroupController : ControllerBase
    {
        private readonly IStudentGroupRepository _repository;

        public StudentGroupController(IStudentGroupRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Returns a list of groups with a specific id
        /// </summary>
        /// <param name="id">Id of the groups to be found</param>
        /// <returns>A list of groups</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("group/{id}")]
        public async Task<ActionResult<IEnumerable<GroupDTO>>> GetGroups(int id)
        {
            var groupList = await _repository.FindGroups(id).ToListAsync();
            return  groupList;
        }

        /// <summary>
        /// Returns a list of students with a specific id
        /// </summary>
        /// <param name="id">Id of the students to be found</param>
        /// <returns>A list of students</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("student/{id}")]
        public async Task<ActionResult<IEnumerable<StudentDTO>>> GetStudents(int id)
        {
            var studentList = await _repository.FindStudents(id).ToListAsync();
            return studentList;
        }

        /// <summary>
        /// Returns a StudentGroup with a specific key
        /// </summary>
        /// <param name="studentId">Id of the studentId to be found</param>
        /// <param name="groupId">Id of the groupId to be found</param>
        /// <returns>A StudentGroupDTO</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{studentId}/{groupId}")]
        public async Task<ActionResult<StudentGroupDTO>> Get(int studentId, int groupId)
        {
            var requestDTO = await _repository.FindAsync(studentId, groupId);

            if (requestDTO == null)
            {
                return NotFound();
            }

            return requestDTO;
        }



        /// <summary>
        /// Creates a StudentGroup.
        /// </summary>
        /// <param name="requestDTO">Body for studentgroup</param>
        /// <returns>A newly created TeacherCourse</returns>
        /// <response code="201">Returns the newly created studentgroup</response>
        /// <response code="400">If the item is null</response>  
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] StudentGroupDTO requestDTO)
        {
            var created = await _repository.CreateAsync(requestDTO);

            return CreatedAtAction(nameof(Get),new {created.StudentId,created.GroupId }, created);
        }



        /// <summary>
        /// Deletes a StudentGroup with a specific id.
        /// </summary>
        /// <param name="studentId">Id of the student to delete</param>
        /// <param name="groupId">Id of the group to delete</param>
        /// <response code="204">In case of succesful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{studentId}/{groupId}")]
        public async Task<ActionResult> Delete(int studentId, int groupId)
        {
            var deleted = await _repository.DeleteAsync(studentId, groupId);

            if (deleted)
            {
                return NoContent();
            }

            return NotFound();
        }
    }
}

