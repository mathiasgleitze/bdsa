﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StudentController : ControllerBase
    {
        private readonly IStudentRepository _repository;

        public StudentController(IStudentRepository repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Returns a Student with a specific id
        /// </summary>
        /// <param name="id">Id of the Student to be found</param>
        /// <returns>A StudentDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{id}")]
        public async Task<ActionResult<StudentDTO>> Get(int id)
        {
            var entity = await _repository.FindAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            return entity;
        }
        /// <summary>
        /// Returns a list of Students that matches the input string
        /// </summary>
        /// <param name="name">Search String for finding all the students that matches this string</param>
        /// <returns>A list of StudentDTOs</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("name/{name}")]
        public async Task<ActionResult<IEnumerable<StudentDTO>>> GetName(string name)
        {
            var dtos = _repository.FindStudentsByName(name);

            if (dtos.Count() == 0)
            {
                return NotFound();
            }

            return await dtos.ToListAsync();
        }

        /// <summary>
        /// Returns Student by mail
        /// </summary>
        /// <param name="mail">Search String for finding a student by mail address </param>
        /// <returns>A StudentDTOs</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("mail/{mail}")]
        public async Task<ActionResult<int>> GetMail(string mail)
        {
            var id = await _repository.FindIdByMailAsync(mail);

            if (id == 0)
            {
                return NotFound();
            }

            return id;
        }
        /// <summary>
        /// Creates a Student
        /// </summary>
        /// <param name="student">Body for Student</param>
        /// <returns>A newly created Student</returns>
        /// <response code="201">Returns the newly created Student</response>
        /// <response code="400">If the item is null</response>            
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] StudentDTO student)
        {
            var dto = await _repository.CreateAsync(student);
            return CreatedAtAction(nameof(Get), dto);
        }
        /// <summary>
        /// Update a student with a specific id.
        /// </summary>
        /// <param name="id">Id of the student to update</param>
        /// <param name="student">StudentDTO with update data</param>
        /// <response code="204">In case of successful update</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] StudentDTO student)
        {
            var updated = await _repository.UpdateAsync(student);
            if (updated)
            {
                return NoContent();
            }
            return NotFound();
        }

        /// <summary>
        /// Deletes a Student with a specific id.
        /// </summary>
        /// <param name="id">Id of the student to delete</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await _repository.DeleteAsync(id);
            if (deleted)
            {
                return NoContent();
            }
            return NotFound();
        }
    }
}
