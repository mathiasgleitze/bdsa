﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GroupController : ControllerBase
    {
        private readonly IGroupRepository _repository;

        public GroupController(IGroupRepository repository)
        {
            _repository = repository;
        }


        /// <summary>
        /// Returns a list of all Groups 
        /// </summary>
        /// <returns>An IEnumerable(GroupDTO)</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If nothing is found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<GroupDTO>>> Get()
        {
            return await _repository.Read().ToListAsync();
        }
        /// <summary>
        /// Returns all the groups in a project
        /// </summary>
        /// <param name="id">Id of the Project to look in</param>
        /// <returns>A list of GroupDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the items are not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("project/{id}")]
        public async Task<ActionResult<IEnumerable<GroupDTO>>> GetProject(int id)
        {
            var group =  _repository.ReadGroupsInProject(id);

            if (group == null)
            {
                return NotFound();
            }

            return await group.ToListAsync();
        }

        /// <summary>
        /// Returns a group with a specific id
        /// </summary>
        /// <param name="id">Id of the Group to be found</param>
        /// <returns>A GroupDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{id}")]
        public async Task<ActionResult<GroupDTO>> Get(int id)
        {
            var group = await _repository.FindAsync(id);

            if (group == null)
            {
                return NotFound();
            }

            return group;
        }

        /// <summary>
        /// Creates a Group.
        /// </summary>
        /// <param name="group">Body for Group</param>
        /// <returns>A newly created Group</returns>
        /// <response code="201">Returns the newly created Group</response>
        /// <response code="400">If the item is null</response>            
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] GroupDTO group)
        {
            var created = await _repository.CreateAsync(group);

            return CreatedAtAction(nameof(Get), created);
        }

        /// <summary>
        /// Update a Group with a specific id.
        /// </summary>
        /// <param name="id">Id of the course to update</param>
        /// <param name="group">GroupDTO with update data</param>
        /// <response code="204">In case of successful update</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] GroupDTO group)
        {
            var updated = await _repository.UpdateAsync(group);

            if (updated)
            {
                return NoContent();
            }

            return NotFound();
        }

        /// <summary>
        /// Deletes a group with a specific id.
        /// </summary>
        /// <param name="id">Id of the course to delete</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await _repository.DeleteAsync(id);

            if (deleted)
            {
                return NoContent();
            }

            return NotFound();
        }
    }
}
