﻿using System.Threading.Tasks;
using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class CourseController : ControllerBase
    {
        private readonly ICourseRepository _repository;

        public CourseController(ICourseRepository repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Returns a course with a specific id
        /// </summary>
        /// <param name="id">Id of the Course to be found</param>
        /// <returns>A Course</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{id}")]
        public async Task<ActionResult<CourseDTO>> Get(int id)
        {
            var entity = await _repository.FindAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            return entity;
        }
        /// <summary>
        /// Creates a Course
        /// </summary>
        /// <param name="course">Body for Course</param>
        /// <returns>A newly created Course</returns>
        /// <response code="201">Returns the newly created course</response>
        /// <response code="400">If the item is null</response>            
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] CourseDTO course)
        {
            var dto = await _repository.CreateAsync(course);
            return CreatedAtAction(nameof(Get), dto);
        }
        /// <summary>
        /// Update a course with a specific id.
        /// </summary>
        /// <param name="id">Id of the course to update</param>
        /// <param name="course">CourseDTO with update data</param>
        /// <response code="204">In case of successful update</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] CourseDTO course)
        {
            var updated = await _repository.UpdateAsync(course);
            if (updated)
            {
                return NoContent();
            }
            return NotFound();
        }
        /// <summary>
        /// Deletes a course with a specific id.
        /// </summary>
        /// <param name="id">Id of the course to delete</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await _repository.DeleteAsync(id);
            if (deleted)
            {
                return NoContent();
            }
            return NotFound();
        }
    }
}