﻿using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class StudentCourseController : ControllerBase
    {
        private readonly IStudentCourseRepository _repository;

        public StudentCourseController(IStudentCourseRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Returns a list of all StudentCourses
        /// </summary>
        /// <returns>An IEnumerable(StudentCourseDTO)</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If nothing is found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<StudentCourseDTO>>> Get()
        {
            return await _repository.Read().ToListAsync();
        }

        /// <summary>
        /// Deletes a StudentCourse with a specific id.
        /// </summary>
        /// <param name="studentId">Id of the student to delete</param>
        /// <param name="courseId">Id of the course to delete</param>
        /// <response code="204">In case of succesful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpGet("{studentId}/{courseId}")]
        public async Task<ActionResult<StudentCourseDTO>> Get(int studentId, int courseId)
        {
            var requestDTO = await _repository.FindAsync(studentId, courseId);

            if (requestDTO == null)
            {
                return NotFound();
            }

            return requestDTO;
        }

        /// <summary>
        /// Returns a list of students with a specific id
        /// </summary>
        /// <param name="id">Id of the students to be found</param>
        /// <returns>A list of students</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("student/{id}")]
        public async Task<ActionResult<IEnumerable<StudentDTO>>> GetStudents(int id)
        {
            var studentList = await _repository.FindStudents(id).ToListAsync();
            
            return studentList;
        }

        /// <summary>
        /// Returns a list of courses with a specific id
        /// </summary>
        /// <param name="id">Id of the courses to be found</param>
        /// <returns>A list of courses</returns>
        /// <response code="200">Succes</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("course/{id}")]
        public async Task<ActionResult<IEnumerable<CourseDTO>>> GetCourses(int id)
        {
            var courseList = await _repository.FindCourses(id).ToListAsync();
            return courseList;
        }



        /// <summary>
        /// Creates a StudentCourse.
        /// </summary>
        /// <param name="requestDTO">Body for studentcourse</param>
        /// <returns>A newly created StudentCourse</returns>
        /// <response code="201">Returns the newly created studentcourse</response>
        /// <response code="400">If the item is null</response>  
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] StudentCourseDTO requestDTO)
        {
            var created = await _repository.CreateAsync(requestDTO);

            return CreatedAtAction(nameof(Get), created);
        }



        /// <summary>
        /// Deletes a StudentCourse with a specific id.
        /// </summary>
        /// <param name="studentId">Id of the student to delete</param>
        /// <param name="courseId">Id of the course to delete</param>
        /// <response code="204">In case of succesful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{studentId}/{courseId}")]
        public async Task<ActionResult> Delete(int studentId, int courseId)
        {
            var deleted = await _repository.DeleteAsync(studentId, courseId);

            if (deleted)
            {
                return NoContent();
            }

            return NotFound();
        }
    }
}

