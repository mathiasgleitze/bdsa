﻿using System.Threading.Tasks;
using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ChatController : ControllerBase
    {
        private readonly IChatRepository _repository;

        public ChatController(IChatRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Returns a chat with a specific id
        /// </summary>
        /// <param name="id">Id of the chat to be found</param>
        /// <returns>A Chat</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{id}")]
        public async Task<ActionResult<ChatDTO>> Get(int id)
        {
            var chat = await _repository.FindAsync(id);

            if (chat == null)
            {
                return NotFound();
            }

            return chat;
        }
        /// <summary>
        /// Creates a Chat.
        /// </summary>
        /// <param name="chat">Body for Chat</param>
        /// <returns>A newly created Chat</returns>
        /// <response code="201">Returns the newly created chat</response>
        /// <response code="400">If the item is null</response>            
        [HttpPost]
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<ChatDTO>> Post([FromBody] ChatDTO chat)
        {
            var created = await _repository.CreateAsync(chat);

            return CreatedAtAction(nameof(Get),new { created.Id}, created);
        }


        /// <summary>
        /// Deletes a chat with a specific.
        /// </summary>
        /// <param name="id">Id of the chat to delete</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await _repository.DeleteAsync(id);

            if (deleted)
            {
                return NoContent();
            }
            return NotFound();
        }
    }
}
