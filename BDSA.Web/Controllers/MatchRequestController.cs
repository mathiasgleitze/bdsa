﻿using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MatchRequestController : ControllerBase
    {
        private readonly IMatchRequestRepository _repository;

        public MatchRequestController(IMatchRequestRepository repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Returns a list of MatchRequest from a specific group
        /// </summary>
        /// <param name="groupId">Id of the group</param>
        /// <returns>A list of MatchRequestDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("from/{groupId}")]
        public async Task<ActionResult<IEnumerable<MatchRequestDTO>>> GetFrom(int groupId)
        {
            return await _repository.ReadFrom(groupId).ToListAsync();
        }
        /// <summary>
        /// Returns a list of MatchRequest to a specific group
        /// </summary>
        /// <param name="groupId">Id of the group</param>
        /// <returns>A list of MatchRequestDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("to/{groupId}")]
        public async Task<ActionResult<IEnumerable<MatchRequestDTO>>> GetTo(int groupId)
        {
            return await _repository.ReadTo(groupId).ToListAsync();
        }



        /// <summary>
        /// Returns a MatchRequest with a specific key
        /// </summary>
        /// <param name="fromGroup">Id of the fromGroup to be found</param>
        /// <param name="toGroup">Id of the toGroup to be found</param>
        /// <returns>A MatchRequestDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{fromGroup}/{toGroup}")]
        public async Task<ActionResult<MatchRequestDTO>> Get(int fromGroup, int toGroup)
        {
            var requestDTO = await _repository.FindAsync(fromGroup, toGroup);

            if (requestDTO == null)
            {
                return NotFound();
            }

            return requestDTO;
        }



        /// <summary>
        /// Creates a MatchRequest.
        /// </summary>
        /// <param name="requestDTO">Body for GroupRequest</param>
        /// <returns>A newly created GroupRequest</returns>
        /// <response code="201">Returns the newly created GroupRequest</response>
        /// <response code="400">If the item is null</response>            
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult<MatchRequestDTO>> Post([FromBody] MatchRequestDTO requestDTO)
        {
            var created = await _repository.CreateAsync(requestDTO);

            return CreatedAtAction(nameof(Get), new { created.FromGroup, created.ToGroup }, created);
        }

        /// <summary>
        /// Deletes all MatchRequest with a specific key.
        /// </summary>
        /// <param name="groupId">Id of the fromGroup to delete</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{groupId}")]
        public async Task<ActionResult> DeleteAll(int groupId)
        {
            var delted = await _repository.DeleteAll(groupId);

            if (delted)
            {
                return NoContent();
            }
            return NotFound();
        }


        /// <summary>
        /// Deletes a MatchRequest with a specific key.
        /// </summary>
        /// <param name="fromGroup">Id of the fromGroup to delete</param>
        /// <param name="toGroup">Id of the toGroup to delete</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{fromGroup}/{toGroup}")]
        public async Task<ActionResult> Delete(int fromGroup, int toGroup)
        {
            var deleted = await _repository.DeleteAsync(fromGroup, toGroup);

            if (deleted)
            {
                return NoContent();
            }

            return NotFound();
        }
    }
}

