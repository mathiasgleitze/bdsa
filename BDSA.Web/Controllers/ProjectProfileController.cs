﻿using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProjectProfileController : ControllerBase
    {
        private readonly IProjectProfileRepository _repository;

        public ProjectProfileController(IProjectProfileRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Returns a projectprofile with a specific key
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <param name="projectId">Id of the project</param>
        /// <returns>A ProjectProfileDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{studentId}/{projectId}")]
        public async Task<ActionResult<ProjectProfileDTO>> Get(int studentId, int projectId)
        {

            var entity = await _repository.FindAsync(studentId, projectId);

            if (entity == null)
            {
                
                return NotFound();
            }
            
            return entity;
        }

        /// <summary>
        /// Creates a ProjectProfile
        /// </summary>
        /// <param name="projectProfile">Body for ProjectProfile</param>
        /// <returns>A newly created ProjectProfile</returns>
        /// <response code="201">Returns the newly created projectProfile</response>
        /// <response code="400">If the item is null</response>            
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] ProjectProfileDTO projectProfile)
        {
            var dto = await _repository.CreateAsync(projectProfile);
            return CreatedAtAction(nameof(Get), dto);
        }

        /// <summary>
        /// Update a project with a specific id.
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <param name="projectId">Id of the project</param>
        /// <param name="projectProfile">ProjectProfileDTO with update data</param>
        /// <response code="204">In case of successful update</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpPut("{studentId}/{projectId}")]
        public async Task<ActionResult> Put(int studentId, int projectId, [FromBody] ProjectProfileDTO projectProfile)
        {
            var updated = await _repository.UpdateAsync(projectProfile);

            if (updated)
            {
                return NoContent();
            }
            return NotFound();
        }

        /// <summary>
        /// Deletes a projectprofile with a specific key.
        /// </summary>
        /// <param name="studentId">Id of the student</param>
        /// <param name="projectId">Id of the project</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{studentId}/{projectId}")]
        public async Task<ActionResult> Delete(int studentId, int projectId)
        {
            var deleted = await _repository.DeleteAsync(studentId, projectId);

            if (deleted)
            {
                return NoContent();
            }
            return NotFound();
        }
    }
}
