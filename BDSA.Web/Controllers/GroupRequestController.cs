﻿using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authorization;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GroupRequestController:ControllerBase
    {
        private readonly IGroupRequestRepository _repository;

        public GroupRequestController(IGroupRequestRepository repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Returns a list of GroupRequest from a specific group
        /// </summary>
        /// <param name="groupId">Id of the group</param>
        /// <returns>A list of GroupRequestDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("from/{groupId}")]
        public async Task<ActionResult<IEnumerable<GroupRequestDTO>>> GetFrom(int groupId)
        {
            var fromList = await _repository.ReadFromGroup(groupId).ToListAsync();

            return fromList;
        }
        /// <summary>
        /// Returns a list of GroupRequest to a specific group
        /// </summary>
        /// <param name="groupId">Id of the group</param>
        /// <returns>A list of GroupRequestDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response>  
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("to/{groupId}")]
        public async Task<ActionResult<IEnumerable<GroupRequestDTO>>> GetTo(int groupId)
        {
            var toList = await _repository.ReadToGroup(groupId).ToListAsync();
            return toList;
        }



        /// <summary>
        /// Returns a GroupRequest with a specific key
        /// </summary>
        /// <param name="fromGroup">Id of the fromGroup to be found</param>
        /// <param name="toGroup">Id of the toGroup to be found</param>
        /// <returns>A GroupRequestDTO</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{fromGroup}/{toGroup}")]
        public async Task<ActionResult<GroupRequestDTO>> Get(int fromGroup, int toGroup)
        {
            var requestDTO = await _repository.FindAsync(fromGroup, toGroup);

            if (requestDTO == null)
            {
                return NotFound();
            }

            return requestDTO;
        }



        /// <summary>
        /// Creates a GroupRequest.
        /// </summary>
        /// <param name="requestDTO">Body for GroupRequest</param>
        /// <returns>A newly created GroupRequest</returns>
        /// <response code="201">Returns the newly created GroupRequest</response>
        /// <response code="400">If the item is null</response>            
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] GroupRequestDTO requestDTO)
        {
            var created = await _repository.CreateAsync(requestDTO);

            return CreatedAtAction(nameof(Get), created);
        }
        /// <summary>
        /// Move GroupRequests from one id to another.
        /// </summary>
        /// <param name="oldgroup">Id of the old group</param>
        /// <param name="newgroup">Id of the new group</param>
        /// <response code="204">In case of successful update</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpPut("update/{oldgroup}/{newgroup}")]
        public async Task<ActionResult> Put(int oldgroup, int newgroup)
        {
            var updated = await _repository.UpdateAsync(oldgroup, newgroup);

            if (updated)
            {
                return NoContent();
            }
            return NotFound();
        }



        /// <summary>
        /// Deletes a GroupRequest with a specific key.
        /// </summary>
        /// <param name="fromGroup">Id of the fromGroup to delete</param>
        /// <param name="toGroup">Id of the toGroup to delete</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{fromGroup}/{toGroup}")]
        public async Task<ActionResult> Delete(int fromGroup, int toGroup)
        {
            var deleted = await _repository.DeleteAsync(fromGroup, toGroup);

            if (deleted)
            {
                return NoContent();
            }

            return NotFound();
        }
    }
}

