﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BDSA.Models;
using BDSA.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace BDSA.Web.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ProjectController : ControllerBase
    {
        private readonly IProjectRepository _repository;

        public ProjectController(IProjectRepository repository)
        {
            _repository = repository;
        }
        /// <summary>
        /// Returns a project with a specific id
        /// </summary>
        /// <param name="id">Id of the Project to be found</param>
        /// <returns>A Project</returns>
        /// <response code="200">Success</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(200)]
        [ProducesResponseType(404)]
        [HttpGet("{id}")]
        public async Task<ActionResult<ProjectDTO>> Get(int id)
        {
            var entity = await _repository.FindAsync(id);

            if (entity == null)
            {
                return NotFound();
            }

            return entity;
        }
        /// <summary>
        /// Creates a Project
        /// </summary>
        /// <param name="project">Body for Project</param>
        /// <returns>A newly created Project</returns>
        /// <response code="201">Returns the newly created project</response>
        /// <response code="400">If the item is null</response>            
        [ProducesResponseType(201)]
        [ProducesResponseType(400)]
        [HttpPost]
        public async Task<ActionResult> Post([FromBody] ProjectDTO project)
        {
            var dto = await _repository.CreateAsync(project);
            return CreatedAtAction(nameof(Get), dto);
        }
        /// <summary>
        /// Update a project with a specific id.
        /// </summary>
        /// <param name="id">Id of the project to update</param>
        /// <param name="project">ProjectDTO with update data</param>
        /// <response code="204">In case of successful update</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpPut]
        public async Task<ActionResult> Put(int id, [FromBody] ProjectDTO project)
        {
            var updated = await _repository.UpdateAsync(project);
            if (updated)
            {
                return NoContent();
            }
            return NotFound();
        }
        /// <summary>
        /// Deletes a project with a specific id.
        /// </summary>
        /// <param name="id">Id of the project to delete</param>
        /// <response code="204">In case of successful deletion</response>
        /// <response code="404">If the item is not found</response> 
        [ProducesResponseType(204)]
        [ProducesResponseType(404)]
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var deleted = await _repository.DeleteAsync(id);
            if (deleted)
            {
                return NoContent();
            }
            return NotFound();
        }
    }
}