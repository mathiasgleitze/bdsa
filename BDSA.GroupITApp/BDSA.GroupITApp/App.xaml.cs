﻿using BDSA.GroupITApp.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;
using Microsoft.Extensions.DependencyInjection;
using System.Net.Http;
using BDSA.GroupITApp.Models;
using Microsoft.Identity.Client;
using BDSA.GroupITApp.Views.Pages;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace BDSA.GroupITApp
{
    public partial class App : Application
    {
        private static readonly Uri backendUrl = new Uri("https://localhost:44358");
        private readonly Lazy<IServiceProvider> lazyProvider = new Lazy<IServiceProvider>(() => ConfigureServices());
        public IServiceProvider Container => lazyProvider.Value;

        public App()
        {
            InitializeComponent();
            DependencyResolver.ResolveUsing(type => Container.GetService(type));

            MainPage = new LogInPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }

        private static IServiceProvider ConfigureServices()
        {
            var services = new ServiceCollection();
            var settings = new Settings();
            var publicClientApplication = new PublicClientApplication(settings.ClientId, $"https://login.microsoftonline.com/{settings.TenantId}");

            var handler = new BearerTokenHttpClientHandler(publicClientApplication, settings);

            //#if DEBUG
            //            handler.ServerCertificateCustomValidationCallback = (message, cert, chain, errors) => true;
            //#endif

            services.AddSingleton<ISettings>(settings);
            services.AddSingleton<IPublicClientApplication>(publicClientApplication);
            services.AddSingleton(_ => new HttpClient(handler) { BaseAddress = backendUrl });


            services.AddSingleton<ILogic, Logic>();
            services.AddSingleton<NavigationViewModel>();
            services.AddSingleton<ProjectsViewModel>();
            services.AddSingleton<SwipeViewModel>();
            services.AddSingleton<ChatViewModel>();
            services.AddSingleton<ActionViewModel>();
            services.AddSingleton<SearchViewModel>();
            services.AddSingleton<NotificationsViewModel>();
            services.AddSingleton<MatchesViewModel>();
            services.AddSingleton<GroupViewModel>();
            services.AddSingleton<ProjectProfileViewModel>();
            services.AddSingleton<ProfileViewModel>();
            
            return services.BuildServiceProvider();
        }
    }
}
