﻿using BDSA.GroupITApp.Views.Page;
using BDSA.GroupITApp.Views.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BDSA.GroupITApp
{
    public partial class MainPage : ContentPage
    {
        private Dictionary<string, CustomPage> pages = new Dictionary<string, CustomPage>();
        public MainPage()
        {
            InitializeComponent();
            BindingContext = this;

            var buttonRow = new Navbar(
                new List<(string, string, string, string)>()
                {
                    ("navigation","projects","Projects", null),
                    ("navigation","matches","Matches", null),
                    ("navigation","notifications","Notifications", null),
                    ("navigation","profile","Profile", null),
                }
            );

            appPageGrid.Children.Add(buttonRow, 0,1);

        }

        public void SetPage(CustomPage id)
        {
            pageContainer.Children.Clear();
            pageContainer.Children.Add(id);
        }
      
    }
}