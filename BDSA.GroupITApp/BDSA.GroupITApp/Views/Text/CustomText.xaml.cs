﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Text
{
    [XamlCompilation(XamlCompilationOptions.Compile)]

	public partial class CustomText : ContentView   
	{
        public string Text{get{ return label.Text; } set { label.Text = value; }}
        public Color TextColor{ set { label.TextColor = value; }}
        protected double FontSize{set { label.FontSize = value; }}
        protected TextAlignment HorizontalTextAlignment { set { label.HorizontalTextAlignment = value; } }
        protected FontAttributes FontAttributes { set { label.FontAttributes = value; } }
        public CustomText (string text)
		{
			InitializeComponent();
            Text = text;
        }
    }
}