﻿using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Text
{
	public class Logo : CustomText
	{
		public Logo (string text) : base(text)
		{
            FontSize = 30;
            HorizontalTextAlignment = TextAlignment.Center;
            TextColor = Color.FromHex("#ffbb00");
            FontAttributes = FontAttributes.Bold;

        }
	}
}