﻿namespace BDSA.GroupITApp.Views.Text
{
	public class Paragraph : CustomText
	{
		public Paragraph (string text) : base(text)
        {
            FontSize = 14;
        }
	}
}