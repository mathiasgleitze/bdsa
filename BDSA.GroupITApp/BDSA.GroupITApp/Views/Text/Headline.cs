﻿namespace BDSA.GroupITApp.Views.Text
{
    public class Headline : CustomText
	{
		public Headline (string size, string text) : base(text)
		{
            double fontSize;
            switch (size)
            {
                case ("x-small"):
                    fontSize = 8;
                    break;
                case ("small"):
                    fontSize = 12;
                    break;
                case ("medium"):
                    fontSize = 14;
                    break;
                case ("large"):
                    fontSize = 16;
                    break;
                default:
                    fontSize = 14;
                    break;
            }
            FontSize=fontSize;
		}
	}
}