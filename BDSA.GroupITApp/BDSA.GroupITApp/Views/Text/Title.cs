﻿namespace BDSA.GroupITApp.Views.Text
{
	public class Title : CustomText
	{
		public Title (string text) : base(text)
		{
            FontSize = 20;
		}
	}
}