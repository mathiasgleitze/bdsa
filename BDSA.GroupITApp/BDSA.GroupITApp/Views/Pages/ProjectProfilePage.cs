﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.ContentBoxes;
using BDSA.GroupITApp.Views.Page;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Pages
{
	public class ProjectProfilePage : SubPage
	{
        ProjectProfileViewModel projectProfileViewModel;

        public ProjectProfilePage () : base("Project profile")
		{
            projectProfileViewModel = DependencyService.Resolve<ProjectProfileViewModel>();
            projectProfileViewModel.PropertyChanged += Change;
		}

        private void Update()
        {
            ClearContent();
            List<ListElement> students = new List<ListElement>();
            
            foreach (StudentProjectDTO student in projectProfileViewModel.Group)
            {
                students.Add(new ListElement("button", "Profile", "(none)", null, student.Name, "groupProfile") { DataId="1+1"});
            }
            AddContentView(new InfoBox("Group", false, students, 0, "","1+1"));
        
            AddContentView(new InfoBox("Project", true, Template.ProjectProfileProperties(projectProfileViewModel.StudentProfile), 0, "projectProfile", projectProfileViewModel.StudentProfile.StudentId+""));
        }

        private void Change(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;

            if (propertyName == "Group") Update();

        }
    }
}