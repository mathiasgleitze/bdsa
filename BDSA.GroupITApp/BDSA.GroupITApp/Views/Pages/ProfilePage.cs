﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.ContentBoxes;
using BDSA.GroupITApp.Views.Page;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Pages
{
	public class ProfilePage : CustomMainPage
	{
        ProfileViewModel profileViewModel;
        public ProfilePage () : base("Profile")
        {
            profileViewModel = DependencyService.Resolve<ProfileViewModel>();
            profileViewModel.PropertyChanged += Change;
        }

        private void Update()
        {
            StudentDTO student = profileViewModel.StudentProfile;
            string placeholder = "(none)";
            var generalProperties = new List<ListElement>() {
            new ListElement("field","Name",placeholder,null,student.Name,"studentName"),
            new ListElement("field","Mail",placeholder,null,student.Mail,"studentMail"),
            };

            var personalProperties = new List<ListElement>() {
            new ListElement("field","City",placeholder,null,student.Address,"studentCity"),
            new ListElement("textarea","About me",placeholder,null,student.About,"studentAbout"),
            };

            AddContentView(new InfoBox("General", false, generalProperties, 0, "generalProfile", student.Id + ""));

            AddContentView(new InfoBox("Personal", true, personalProperties, 0, "personalProfile", student.Id + ""));
        }

        private void Change(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;

            if (propertyName == "StudentProfile") Update();
        }

    }
}