﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.ContentBoxes;
using BDSA.GroupITApp.Views.Page;
using BDSA.Shared;
using System;
using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Pages
{
	public class MatchesPage : CustomMainPage
	{
        MatchesViewModel matchesViewModel; 

        public MatchesPage () : base("Matches")
        {
            matchesViewModel = DependencyService.Resolve<MatchesViewModel>();
            matchesViewModel.PropertyChanged += Change;
        }

        private void Update()
        {
            ClearContent();
            foreach((MatchDTO, ProjectDTO) match in matchesViewModel.Matches)
            {
                AddContentView(new MatchBox(match));
            }
        }

        private void Change(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;

            if (propertyName == "Matches") Update();

        }
        protected override void OnSwiped(object sender, SwipedEventArgs e)
        {
            matchesViewModel.OnSwiped(sender, e);
        }
    }
}