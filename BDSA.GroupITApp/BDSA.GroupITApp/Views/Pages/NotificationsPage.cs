﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.ContentBoxes;
using BDSA.GroupITApp.Views.Page;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Pages
{
	public class NotificationsPage : CustomMainPage
	{
        private NotificationsViewModel notificationsViewModel;
        
        public NotificationsPage () : base("Notifications")
        {
            notificationsViewModel = DependencyService.Resolve<NotificationsViewModel>();
            notificationsViewModel.PropertyChanged += Change;
        }

        private void Update()
        {
            ClearContent();
            (List<GroupRequestDTO> group, List<MatchDTO> match) notificationTuple = notificationsViewModel.Notifications;
            foreach(GroupRequestDTO groupRequest in notificationTuple.group)
            {
                AddContentView(new RequestNotificationBox(groupRequest));
            }
            foreach(MatchDTO matchNotifcation in notificationTuple.match)
            {
                AddContentView(new MatchNotificationBox(matchNotifcation));
            }

        }
        private void Change(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;
            if (propertyName == "Notifications") Update();

        }

        protected override void OnSwiped(object sender, SwipedEventArgs e)
        {
            notificationsViewModel.OnSwiped(sender, e);
        }
    }
}