﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.ContentBoxes;
using BDSA.GroupITApp.Views.Page;
using System;
using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Pages
{
    class SwipePage : SubPage
    {
        SwipeViewModel viewModel;

        public SwipePage() : base()
        {
            BindingContext = viewModel = DependencyService.Resolve<SwipeViewModel>();
            viewModel.PropertyChanged += Change;
        }

        protected override void OnSwiped(object sender, SwipedEventArgs e)
        {
            viewModel.OnSwiped(sender, e);
        }

        private void Change(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;
            if (propertyName == "CurrentGroup") ShowGroup();
        }

        private void ShowGroup()
        {
            ClearContent();
            if (viewModel.CurrentGroupId != 0) { 
                Title = "Group " + viewModel.CurrentGroupId;
                AddContentViews(Template.StudentInfoBoxes(viewModel.CurrentGroup));
            } else
            {
                Title = "No more groups exist";
            }
            ScrollToTop(false);
        }
    }
}
