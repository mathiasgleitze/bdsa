﻿using BDSA.GroupITApp.Views.Page;
using System;
using Xamarin.Forms;
using BDSA.GroupITApp.Views.ContentBoxes;
using BDSA.GroupITApp.ViewModels;
using BDSA.Shared;

namespace BDSA.GroupITApp.Views.Pages
{
	public class ProjectsPage : CustomMainPage
	{
        private ProjectsViewModel projectsViewModel;
		public ProjectsPage () : base("Projects")
		{
            BindingContext = projectsViewModel = DependencyService.Resolve<ProjectsViewModel>();

            projectsViewModel.PropertyChanged += Change;
        }

        private void Update()
        {
            ClearContent();
            foreach (ProjectDTO project in projectsViewModel.Projects)
            {
                ProjectBox projectBox = new ProjectBox(project);
                AddContentView(projectBox);
            }
        }


        private void Change(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;

            if (propertyName == "Projects") Update();

        }

        protected override void OnSwiped(object sender, SwipedEventArgs e)
        {
            projectsViewModel.OnSwiped(sender, e);
        }
        
    }
}