﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.ContentBoxes;
using BDSA.GroupITApp.Views.Page;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Pages
{
	public class ChatPage : SubPage
	{
        private ChatBox chatBox;
        private ChatViewModel chatViewModel;
        
		public ChatPage () : base ()
		{
            chatViewModel = DependencyService.Resolve<ChatViewModel>();
            chatViewModel.PropertyChanged += Change;

            chatBox = new ChatBox(DependencyService.Resolve<NavigationViewModel>().Student, chatViewModel.Messages);

            string groupId = "groupId"; //TODO set group id
            FieldButton sendFieldButton = new FieldButton("Write a message...", "Send", "sendChatMessage", groupId);
            
            AddStaticContentView(chatBox, new GridLength(1, GridUnitType.Star));
            AddStaticContentView(sendFieldButton, new GridLength(1,GridUnitType.Auto));

            chatBox.ScrollToBottom(false);

        }

        public void Change(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;

            if (propertyName == "Messages") Update();
        }

        private void Update()
        {
            Title = "Group " + chatViewModel.ChatId;
            chatBox.Update(chatViewModel.Messages);
            chatBox.ScrollToBottom(true);
        }

        protected override void OnSwiped(object sender, SwipedEventArgs e)
        {
            chatViewModel.OnSwiped(sender, e);
        }

    }
}