﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.ContentBoxes;
using BDSA.GroupITApp.Views.Page;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Pages
{
	public class SearchPage : SubPage
	{
    SearchViewModel searchViewModel;
        
        public SearchPage () : base("Search")
		{
            searchViewModel = DependencyService.Resolve<SearchViewModel>();
            searchViewModel.PropertyChanged += Change;

            AddContentView(new FieldButton("Search for a group...", "Search", "searchGroup","this"));
            
		}
        
        private void Change(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;

            if (propertyName == "SearchResult") Update();
        }

        private void Update()
        {
            ClearContentKeepFirst(true);

            foreach ((int groupId, IEnumerable<StudentProjectDTO> students) group in searchViewModel.SearchResult)
            {
                AddContentView(new SearchGroupBox(group.groupId, group.students));
            }

        }

    }
}