﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.ContentBoxes;
using BDSA.GroupITApp.Views.Page;
using System;
using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Pages
{
    public class GroupPage : SubPage
    {
        private GroupViewModel groupViewModel;
        public GroupPage() : base()
        {
            BindingContext = groupViewModel = DependencyService.Resolve<GroupViewModel>();
            groupViewModel.PropertyChanged += Change;
        }
        

        private void Update()
        {
            ClearContent();
            Title = "Group " + groupViewModel.ViewGroupId;
            AddContentViews(Template.StudentInfoBoxes(groupViewModel.ViewGroup));
            ScrollToTop(false);
        }

        private void Change(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;

            if (propertyName == "ViewGroup") Update();
        }

    }
}