﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.ContentBoxes;
using BDSA.GroupITApp.Views.Text;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Pages
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class LogInPage : ContentPage
	{
        private NavigationViewModel navigationViewModel;
        private ActionViewModel actionViewModel;
        public LogInPage ()
		{
			InitializeComponent ();

            navigationViewModel = DependencyService.Resolve<NavigationViewModel>();
            actionViewModel = DependencyService.Resolve<ActionViewModel>();
            actionViewModel.PropertyChanged += IsPropertyChanged;
            logInPageContainer.Children.Add(new Logo("GroupIT"));

            ButtonRow buttonRow = new ButtonRow(
                new List<(string, string, string, string)>() { ("action", "logIn", "Log in", "") }, "1"
             );
            logInPageContainer.Children.Add(buttonRow);



        }

        private void IsPropertyChanged(object sender, EventArgs e) {
            System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs = (System.ComponentModel.PropertyChangedEventArgs)e;
            string actionId = propertyChangedEventArgs.PropertyName;

            if (actionId == "logIn" && actionViewModel.LoggedIn)
            {
                navigationViewModel.CreatePages();
                Navigation.PushModalAsync(navigationViewModel.GetMainPage());
            }
        }
    }
}