﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Page
{
	public class SubPage : CustomPage
	{
		public SubPage () : base()
		{
            HasBackButton = true;
        }

        public SubPage(string title) : base(title)
        {
            HasBackButton = true;
        }
    }
}