﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using BDSA.GroupITApp.Views.Text;

namespace BDSA.GroupITApp.Views.Page
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ContentBox : ContentView
	{
        protected string Title { set { title.Text = value.ToUpper(); } }
        protected int Spacing { set { contentBoxContainer.Spacing = value; } }

        private Headline3 title; 
        public ContentBox (string title)
		{
			InitializeComponent ();
            this.title = new Headline3(title.ToUpper());
            titleContainer.Children.Add(this.title);
        }

        public ContentBox()
        {
            InitializeComponent();
            title = new Headline3("");
            titleContainer.Children.Add(title);
        }

        public void ScrollToBottom(bool hasAnimation)
        {
            scrollView.ScrollToAsync(contentBoxContainer, ScrollToPosition.End, hasAnimation);
        }

        protected void AddContent(ContentView content)
        {
            content.Margin = new Thickness(0, 0, 0, 3);
            contentBoxContainer.Children.Add(content);
        }

        protected void InsertContent(ContentView content, int index)
        {
            content.Margin = new Thickness(0, 0, 0, 3);
            contentBoxContainer.Children.Insert(index, content);
        }

        protected void RemoveContent(int index)
        {
            contentBoxContainer.Children.RemoveAt(index);
        }

        protected void AddContentWithoutMargin(ContentView content)
        {
            content.Margin = new Thickness(0, 0, 0, 0);
            contentBoxContainer.Children.Add(content);
        }

        protected void ClearContent()
        {
            contentBoxContainer.Children.Clear();
        }
    }


}