﻿using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.Text;
using System;
using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Page
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CustomPage : ContentView
	{
        protected bool HasBackButton{
            set {
                backButtonContainer.Children.Clear();
                if (value) backButtonContainer.Children.Add(new NavigationButton("back", "back", ""));
            }
        }
        public String Title {
            set {
                titleContainer.Children.Clear();
                titleContainer.Children.Add(new Title(value));
            }
        }
		public CustomPage ()
		{
			InitializeComponent ();  
		}

        public CustomPage(string title)
        {
            InitializeComponent();
            Title = title;
        }

        protected virtual void OnSwiped(object sender, SwipedEventArgs e) { }

        public void ScrollToTop(bool hasAnimation)
        {
            scrollView.ScrollToAsync(contentareaContainer, ScrollToPosition.Start, hasAnimation);
        }
        protected void AddContentView(ContentView contentView)
        {
            contentareaContainer.Children.Add(contentView);
        }
        protected void AddContentViews(List<ContentView> contentViews)
        {
            foreach(ContentView contentView in contentViews)
            {
            contentareaContainer.Children.Add(contentView);
            }
        }


        protected void AddStaticContentView(ContentView contentView, GridLength gridLength)
        {
            int rowNumber = addRowDefinition(gridLength);
            contentareaGrid.Children.Add(contentView, 0, rowNumber-1);
        }

        private int addRowDefinition(GridLength gridLength)
        {
            RowDefinition rowDefinition = new RowDefinition() { Height = gridLength };
            contentareaGrid.RowDefinitions.Add(rowDefinition);
            return contentareaGrid.RowDefinitions.Count();
        }

        protected void ClearContent()
        {
            contentareaContainer.Children.Clear();
        }

        protected void ClearContentKeepFirst(bool keepFirst)
        {
            if (!keepFirst) ClearContent();
            else {
                if (contentareaContainer.Children.Count == 0) return;
                while (contentareaContainer.Children.Count > 1)
                {
                    contentareaContainer.Children.RemoveAt(1);
                }
            }
        }

    }
}