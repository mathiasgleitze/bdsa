﻿using BDSA.GroupITApp.Views.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Page
{
	public class Navbar : ButtonRow
	{
		public Navbar (List<(string type, string pageId, string text, string clickedText)> buttons) : base(buttons,"")
		{
            BackgroundColor = Color.FromHex("#555555");
		}
	}
}