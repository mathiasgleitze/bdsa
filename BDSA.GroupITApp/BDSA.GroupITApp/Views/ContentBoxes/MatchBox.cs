﻿using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.Page;
using BDSA.GroupITApp.Views.Text;
using BDSA.Shared;
using System.Collections.Generic;

namespace BDSA.GroupITApp.Views.ContentBoxes
{
    public class MatchBox : ContentBox
	{
		public MatchBox ((MatchDTO match, ProjectDTO project) match): base()
		{
            Title = match.project.Name;
            AddContent(new Headline2("Group " + match.match.GroupOne));

            var buttonRow = new ButtonRow(
                
                new List<(string, string, string, string)>()
                {
                    ("navigation", "groupProfile", "Profile", null),
                    ("navigation", "chat", "Chat", null),
                    ("action", "sendGroupRequestMatch", "Request", null),
                },
                match.match.GroupOne + "+" + match.match.ChatId + "+" + match.match.GroupTwo
            );
            AddContent(buttonRow);
        }
	}
}