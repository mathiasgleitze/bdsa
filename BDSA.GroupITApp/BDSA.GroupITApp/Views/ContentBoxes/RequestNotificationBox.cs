﻿using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.Page;
using BDSA.GroupITApp.Views.Text;
using BDSA.Shared;
using System.Collections.Generic;

namespace BDSA.GroupITApp.Views.ContentBoxes
{
	public class RequestNotificationBox : ContentBox
	{
		public RequestNotificationBox (GroupRequestDTO group) : base("Request")
        {
            string text = "Group " + group.FromGroup + " has sent you a group request!";
            AddContent(new Headline2(text));

            var buttonRow = new ButtonRow(
                
                new List<(string, string, string, string)>()
                {
                    ("action", "acceptGroupRequest", "Accept", null),
                    ("action", "declineGroupRequest", "Decline", null),
                }, group.FromGroup + "+" + group.ToGroup 
            );
            AddContent(buttonRow);
        }
	}
}