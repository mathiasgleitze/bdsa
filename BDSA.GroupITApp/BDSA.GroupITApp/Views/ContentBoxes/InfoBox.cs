﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.Page;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.ContentBoxes
{
	public class InfoBox : ContentBox
	{

        private ActionViewModel actionViewModel;
        private string id;
        private string dataId;
        private List<ListElement> listElements;
        private List<ListElement> firstList;
        private List<ListElement> secondList;
        private bool hasShowMore;
        private bool isShowingMore;
        private bool isEditable;
        public InfoBox (string title, bool isEditable, List<ListElement> listElements, int splitIndex, string id, string dataId) : base(title)
        {
            this.id = id;
            this.isEditable = isEditable;
            this.dataId = dataId;
            this.listElements = listElements;
            BindingContext = actionViewModel = DependencyService.Resolve<ActionViewModel>();
            actionViewModel.PropertyChanged += IsPropertyChanged;
            actionViewModel.SetIsEnabled(dataId, false);

            hasShowMore = splitIndex > 0;

            if (!hasShowMore)
            {
                AddContent(new CustomList(listElements));
                if (isEditable)
                {
                    var buttonRow = new ButtonRow(new List<(string, string, string, string)>(){("action", "edit", "Edit", "Save")}, dataId);
                    AddContent(buttonRow);
                }
            }

            else {
                firstList = listElements.GetRange(0, splitIndex);
                AddContent(new CustomList(firstList));
                var buttonRow = new ButtonRow(new List<(string, string, string, string)>(){ ("action", "showMore", "Show more", "Show less")}, dataId);
                AddContent(buttonRow);
                secondList = listElements.GetRange(splitIndex, listElements.Count - splitIndex);
            }
        }

        private void IsPropertyChanged(object sender, EventArgs e)
        {
            System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs = (System.ComponentModel.PropertyChangedEventArgs)e;
            string actionId = propertyChangedEventArgs.PropertyName;
            string currentDataId = actionViewModel.CurrentDataId;
            if (dataId == currentDataId )
            {
                switch (actionId)
                {
                    case "showMore":
                        if (hasShowMore)
                        {
                            if (isShowingMore){
                                RemoveContent(1);
                            }
                            else{
                                InsertContent(new CustomList(secondList), 1);
                            }
                            isShowingMore = !isShowingMore;
                        }
                        break;
                    case "edit":
                        if (isEditable)
                        {
                            bool isEnabled = actionViewModel.IsEnabled(dataId);
                            var newValues = new Dictionary<string, string>();
                            foreach (ListElement listElement in listElements)
                            {
                                if (!isEnabled) newValues[listElement.PropertyId] = listElement.Value;
                                listElement.NewIsEnabled = isEnabled;
                            }
                            if (!isEnabled) actionViewModel.SetValues(id, dataId, newValues);
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}