﻿using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.Page;
using System.Collections.Generic;

namespace BDSA.GroupITApp.Views.ContentBoxes
{
	public class LogInBox : ContentBox
	{
		public LogInBox () : base()
		{
            Title = "Log in";
            //Field mailField = new Field("Enter ITU mail...", "mail", "1") { NewIsEnabled=true};
            //Field passwordField = new Field("Enter password...", "password", "1") { NewIsEnabled = true, IsPassword = true};
            ButtonRow buttonRow = new ButtonRow(
                new List<(string, string, string, string)>() { ("action", "logIn", "Log in", "") }, "1"
             );
            //AddContent(mailField);
            //AddContent(passwordField);
            AddContent(buttonRow);

        }
	}
}