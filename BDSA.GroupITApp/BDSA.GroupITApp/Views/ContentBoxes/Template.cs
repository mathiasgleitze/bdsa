﻿using BDSA.GroupITApp.Views.Content;
using BDSA.Shared;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.ContentBoxes
{
    public class Template
    {
        private static string placeholder = "(none)";
        public static InfoBox StudentInfoBox(StudentProjectDTO student, int index)
        {
            var info = new List<ListElement>();

            info.Add(new ListElement("field", "Name", placeholder, null, student.Name,"studentName"));

            info.AddRange(ProjectProfilePropertiesReadOnly(student));
            info.AddRange(ProfileProperties(student));

            InfoBox infoBox = new InfoBox("Group member " + index, false, info, 6, "profile", student.Id + "");
            return infoBox;
        }

        public static List<ContentView> StudentInfoBoxes(ObservableCollection<StudentProjectDTO> students)
        {
            List<ContentView> infoBoxes = new List<ContentView>();

            int index = 0;
            foreach (StudentProjectDTO student in students)
            {
                index++;
                infoBoxes.Add(StudentInfoBox(student, index));
            }

            return infoBoxes;
        }

        public static List<ListElement> ProjectProfileProperties(ProjectProfileDTO student) {
            var itemsSource = new List<string>();
            for (int i = 1; i < 11; i++) itemsSource.Add(i+"");
            var projectProfileProperties = new List<ListElement>() {
                new ListElement("dropdown","Ambitions", placeholder,itemsSource, student.Ambitions+"","studentAmbitions"),
                new ListElement("dropdown","Skill level",placeholder,itemsSource,student.SkillLevel+"","studentSkillLevel"),
                new ListElement("dropdown","Flexibility", placeholder,itemsSource,student.Flexibility+"","studentFlexibility" ),
                new ListElement("dropdown","Meeting disciplin", placeholder,itemsSource,student.MeetingDisciplin+"","studentMeetingDisciplin"),
                new ListElement("textarea","About project",placeholder,itemsSource,student.About,"studentAboutProject"),
            };

            return projectProfileProperties;
        }

        public static List<ListElement> ProjectProfilePropertiesReadOnly(StudentProjectDTO student)
        {
            var projectProfileProperties = new List<ListElement>() {
                new ListElement("field","Ambitions", placeholder,null, student.Ambitions + "/10", "studentAmbitions"),
                new ListElement("field","Skill level",placeholder,null, student.SkillLevel + "/10","studentSkillLevel"),
                new ListElement("field","Flexibility", placeholder,null, student.Flexibility + "/10","studentFlexibility" ),
                new ListElement("field","Meeting disciplin", placeholder,null, student.MeetingDisciplin + "/10","studentMeetingDisciplin"),
                new ListElement("textarea","About project",placeholder,null, student.AboutProject,"studentAboutProject"),
            };

            return projectProfileProperties;
        }

        public static List<ListElement> ProfileProperties(StudentProjectDTO student)
        {
            var profileProperties = new List<ListElement>() {
                new ListElement("field", "Mail", placeholder, null, student.Mail, "studentMail"),
                new ListElement("field", "City", placeholder, null, student.Address, "studentCity"),
                new ListElement("textarea", "About", placeholder, null,student.AboutStudent, "studentAbout")
            };

            return profileProperties;
        }
    }
}