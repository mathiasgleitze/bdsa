﻿using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.Page;
using BDSA.GroupITApp.Views.Text;
using BDSA.Shared;
using System.Collections.Generic;

namespace BDSA.GroupITApp.Views.ContentBoxes
{
	public class ProjectBox : ContentBox
	{
		public ProjectBox (ProjectDTO project) : base()
        {
            BindingContext = project;
            Title = project.CourseName;
            AddContent(new Headline2(project.Name));
            
            var buttonRow = new ButtonRow(
           
                new List<(string, string, string, string)>{
                    ("navigation","chat","Chat", null),
                    ("navigation","projectProfile","Profile", null),
                    ("navigation","search","Search", null),
                    ("navigation","swipe","Swipe", null)
                },
                project.Id + ""
            );
            AddContent(buttonRow);
        }
    }
}