﻿using BDSA.GroupITApp.Views.Page;
using BDSA.GroupITApp.Views.Text;
using BDSA.Shared;

namespace BDSA.GroupITApp.Views.ContentBoxes
{
	public class MatchNotificationBox : ContentBox
	{
		public MatchNotificationBox (MatchDTO match) : base("Match")
		{
            string text = "You have matched with Group " + match.GroupOne + "!";
            AddContent(new Headline2(text));
		}
	}
}