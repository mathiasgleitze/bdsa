﻿using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.Page;
using BDSA.GroupITApp.Views.Text;
using BDSA.Shared;
using System.Collections.Generic;

namespace BDSA.GroupITApp.Views.ContentBoxes
{
	public class SearchGroupBox : ContentBox
	{
		public SearchGroupBox (int group, IEnumerable<StudentProjectDTO> students) : base()
		{
            Title = "Group " + group;
			foreach(StudentProjectDTO student in students)
            {
                AddContent(new Paragraph(student.Name));
            }

            var buttonRow = new ButtonRow(
                
                new List<(string, string, string, string)>()
                {
                    ("action", "sendGroupRequestSearch", "Request", null),
                }, group + ""
            );
            AddContent(buttonRow);
        }
	}
}