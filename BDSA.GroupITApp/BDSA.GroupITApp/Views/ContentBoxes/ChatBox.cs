﻿using BDSA.GroupITApp.Views.Content;
using BDSA.GroupITApp.Views.Page;
using BDSA.Shared;
using System.Collections.ObjectModel;

namespace BDSA.GroupITApp.Views.ContentBoxes
{
	public class ChatBox : ContentBox
	{
        private int studentId;
		public ChatBox (int studentId, ObservableCollection<MessageDTO> messages) : base ()
		{
            this.studentId = studentId;
            Spacing = 0;
            Update(messages);   
        }



        public void Update(ObservableCollection<MessageDTO> messages)
        {
            ClearContent();
            Title = messages.Count + " message" + (messages.Count != 1 ? "s": "");
            foreach (MessageDTO message in messages)
            {
                bool hasSent = message.StudentId == studentId ? true : false;
                ChatMessage chatMessage = new ChatMessage(message.StudentId + "", message.Text, message.Time, hasSent);
                AddContentWithoutMargin(chatMessage);
            }
        }
	}
}