﻿using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Content
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Textarea : ContentView
	{
        public string Text { get { return textarea.Text; } set { textarea.Text = value; } }
        public bool NewIsEnabled { set { textarea.IsEnabled = value; } }
        public Textarea(bool isEnabled, string placeholder)
        {
            InitializeComponent();

            textarea.Placeholder = placeholder;
            textarea.IsEnabled = isEnabled;
        }
    }
}