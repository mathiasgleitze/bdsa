﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ButtonRow : ContentView
	{
        public Color BackgrundColor { set { buttonRowOuterContainer.BackgroundColor = value; } }
        
		public ButtonRow (List<(string type, string id, string text, string clickedText)> buttons, string dataId)
		{
			InitializeComponent ();
            foreach ((string type, string id, string text, string clickedText) button in buttons)
            {
                CustomButton customButton = null;
                switch (button.type)
                {
                    case ("navigation"):
                        customButton = new NavigationButton(button.text, button.id, dataId);
                        break;
                    case ("action"):
                        if (button.clickedText == null) customButton = new ActionButton(button.text, button.id, dataId);
                        else customButton = new ActionButton(button.text, button.clickedText, button.id, dataId);
                        break;
                    default:
                        break;

                }
                buttonRowContainer.Children.Add(customButton);
            }
		}
	}
}