﻿using BDSA.GroupITApp.ViewModels;
using System;
using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Content
{
	public class ActionButton : CustomButton
	{
        private string actionId;
        private string dataId;
        private string text;
        private ActionViewModel actionViewModel;

        //TODO change actionId to actionType
        //TODO should dataId be a int?
        public ActionButton (string text, string actionId, string dataId) : base(text)
        {
            AddClicked(Clicked);
            Initialize(text, actionId, dataId);
            
        }
        public ActionButton(string text, string clickedText, string actionId, string dataId) : base(text, clickedText)
        {
            AddClicked(Clicked);
            Initialize(text, actionId, dataId);
        }

        private void Initialize(string text, string actionId, string dataId) {
            BindingContext = actionViewModel = DependencyService.Resolve<ActionViewModel>();
            actionViewModel.PropertyChanged += IsPropertyChanged;

            this.actionId = actionId;
            this.dataId = dataId;
            this.text = text.ToUpper();

        }

        public async void Clicked(object sender, EventArgs e)
        {
            await actionViewModel.DispatchAction(actionId, dataId);
        }

        private void IsPropertyChanged(object sender, EventArgs e)
        {
            System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs = (System.ComponentModel.PropertyChangedEventArgs)e;
            string actionId = propertyChangedEventArgs.PropertyName;
            string currentDataId = actionViewModel.CurrentDataId;
            if (dataId == currentDataId)
            {
                switch (actionId)
                {
                    case "edit":
                        bool isEnabled = actionViewModel.IsEnabled(dataId); 
                        ChangeText(isEnabled);
                        break;
                    case "showMore":
                        ChangeText();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}