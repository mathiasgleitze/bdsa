﻿using BDSA.GroupITApp.ViewModels;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CustomButton : ContentView
    {
        private ActionViewModel actionViewModel;
        protected string Text { get { return button.Text; } set { button.Text = value; } }
        private string text;
        private string clickedText;
        public CustomButton(string text, string clickedText)
        {
            Initialize(text);
            this.clickedText = clickedText.ToUpper();
        }

        public CustomButton(string text)
        {
            Initialize(text);
        }

        private void Initialize(string text)
        {
            InitializeComponent();
            this.text = text.ToUpper();
            Text = this.text;
        }

        public void ChangeText(bool isEnabled)
        {
            Text = isEnabled ? clickedText : text;
        }

        public void ChangeText()
        {
            Text = Text==text ? clickedText : text;
        }

        protected void AddClicked(EventHandler clicked)
        {
            button.Clicked += clicked;
        }
    }


}