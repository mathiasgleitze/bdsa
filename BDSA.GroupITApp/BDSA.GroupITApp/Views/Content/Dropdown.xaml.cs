﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Dropdown : ContentView
	{
        public string SelectedItem { get { return (string) dropdown.SelectedItem; } set { dropdown.SelectedItem = value; } }
        public bool NewIsEnabled { set { dropdown.IsEnabled = value; } }

        public Dropdown(bool isEnabled, string placeholder, List<string> itemsSource)
        {
            InitializeComponent();
            dropdown.IsEnabled = isEnabled;
            dropdown.ItemsSource = itemsSource;
        }
    }
}