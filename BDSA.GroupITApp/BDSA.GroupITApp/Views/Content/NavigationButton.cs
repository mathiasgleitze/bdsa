﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.Page;
using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace BDSA.GroupITApp.Views.Content
{
	public class NavigationButton : CustomButton
	{
        private Dictionary<string, CustomPage> pages = new Dictionary<string, CustomPage>();
        private string pageId;
        private string dataId;
        public NavigationButton (string text, string pageId, string dataId) : base(text)
		{
            //TODO change pageId to pageType
            this.pageId = pageId;
            this.dataId = dataId;
            AddClicked(Clicked);
		}

        public void Clicked(object sender, EventArgs e)
        {
            DependencyService.Resolve<NavigationViewModel>().ChangePage(pageId, dataId);
        }
    }
}