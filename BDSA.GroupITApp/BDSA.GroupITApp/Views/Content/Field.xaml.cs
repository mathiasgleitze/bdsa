﻿using BDSA.GroupITApp.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Content
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class Field : ContentView
	{
        private ActionViewModel actionViewModel;
        private string propertyId;
        private string dataId;
        private TextAlignment horizontalTextAlignment;
        public bool IsPassword { set { field.IsPassword = value; } }
        public TextAlignment HorizontalTextAlignment { get { return field.HorizontalTextAlignment; } set { field.HorizontalTextAlignment = value; } }
        public string Text { get {return field.Text; } set { field.Text = value; } }
        public bool NewIsEnabled { set {
                if (value)
                {
                    horizontalTextAlignment = HorizontalTextAlignment;
                    HorizontalTextAlignment = TextAlignment.Start;
                }
                else HorizontalTextAlignment = horizontalTextAlignment;
                field.IsEnabled = value; }
        }
        public Field(string placeholder, string propertyId, string dataId)
        {
            //TODO change propertyId to propertyType
            this.propertyId = propertyId;
            this.dataId = dataId;

            InitializeComponent();

            BindingContext = actionViewModel = DependencyService.Resolve<ActionViewModel>();

            field.Placeholder = placeholder;
            field.Completed += Completed;
            NewIsEnabled = false;
        }

        private async void Completed(object sender, EventArgs e) {
            await actionViewModel.DispatchAction(propertyId, dataId);
        }
    }
}