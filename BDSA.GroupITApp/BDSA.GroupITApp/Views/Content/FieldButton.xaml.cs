﻿using BDSA.GroupITApp.ViewModels;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FieldButton : ContentView
    {
        private ActionViewModel actionViewModel;
        private string propertyId;
        private string dataId;
        private Field field;
        public FieldButton(string placeholder, string buttonText, string propertyId, string dataId)
        {
            this.propertyId = propertyId;
            this.dataId = dataId;

            InitializeComponent();

            field = new Field(placeholder, propertyId, dataId) { NewIsEnabled = true};

            BindingContext = actionViewModel = DependencyService.Resolve<ActionViewModel>();
            actionViewModel.PropertyChanged += IsPropertyChanged;


            fieldContainer.Children.Add(field);
            ActionButton actionButton = new ActionButton(buttonText, propertyId, dataId);
            buttonContainer.Children.Add(actionButton);
        }

        private void IsPropertyChanged(object sender, EventArgs e)
        {
            System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs = (System.ComponentModel.PropertyChangedEventArgs)e;
            string actionId = propertyChangedEventArgs.PropertyName;
            string currentDataId = actionViewModel.CurrentDataId;
            if (dataId == currentDataId)
            {
                switch (actionId)
                {
                    case "sendChatMessage":
                        if (field.Text.Trim() != "")
                        {
                            actionViewModel.SendChatMessage(dataId, field.Text);
                            field.Text = "";
                        }
                        break;
                    case "searchGroup":
                        if (field.Text.Trim() != "")
                        {
                            actionViewModel.SearchGroup(dataId, field.Text);
                            field.Text = "";
                        }
                        break;
                    default:
                        break;
                }
            }
        }
    }
}