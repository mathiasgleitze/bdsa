﻿using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CustomList : ContentView
	{
		public CustomList ( List<ListElement> listElements)
		{
			InitializeComponent ();
            foreach(ListElement listElement in listElements)
            {
                listContainer.Children.Add(listElement);
            }
		}
	}
}