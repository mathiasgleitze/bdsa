﻿using BDSA.GroupITApp.ViewModels;
using BDSA.GroupITApp.Views.Text;
using System.Collections.Generic;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListElement : ContentView
    {
        private ActionViewModel actionViewModel;
        public string Type { get; set; }
        public bool NewIsEnabled
        {
            get { return isEnabled; }
            set { SetIsEnabled(value); }
        }
        public bool IsEditable { get; set; } = true;
        public Paragraph leftParagraph;
        public string Value { get {return GetValue() ; } set { SetValue(value); } }
        public string PropertyId { get; set; }
        public string DataId { get; set; }

        private bool isEnabled;
        public ContentView InputContentView { get; set; }
        public ListElement(string type, string name, string placeholder, List<string> itemsSource, string value, string propertyId)
        {
            Type = type;
            PropertyId = propertyId;

            InitializeComponent();
            if (type != "button") leftParagraph = new Paragraph(name);
            else leftParagraph = new Paragraph(placeholder);

            nameContainer.Children.Add(leftParagraph);
            switch (type)
            {
                case ("field"):
                    Field field = new Field(placeholder, propertyId, DataId) { HorizontalTextAlignment = TextAlignment.End };
                    rightValueContainer.Children.Add(field);
                    InputContentView = field;
                    break;
                case ("dropdown"):
                    Dropdown dropdown = new Dropdown(NewIsEnabled, placeholder, itemsSource);
                    rightValueContainer.Children.Add(dropdown);
                    InputContentView = dropdown;
                    break;
                case ("textarea"):
                    Textarea textarea = new Textarea(NewIsEnabled, placeholder);
                    bottomValueContainer.HeightRequest = 100;
                    bottomValueContainer.Children.Add(textarea);
                    InputContentView = textarea;
                    break;
                case ("button"):
                    NavigationButton button = new NavigationButton(name, propertyId, DataId);
                    rightValueContainer.Children.Add(button);
                    rightValueContainer.HorizontalOptions = LayoutOptions.End;
                    break;
                default:
                    break;
            }

            SetValue(value);
        }

        private void SetIsEnabled(bool isEnabled) {
            switch (Type)
            {
                case "field":
                    ((Field)InputContentView).NewIsEnabled = isEnabled;
                    break;
                case "textarea":
                    ((Textarea)InputContentView).NewIsEnabled = isEnabled;
                    break;
                case "dropdown":
                    ((Dropdown)InputContentView).NewIsEnabled = isEnabled;
                    break;
                default:
                    break;

            };
            this.isEnabled = isEnabled;
        }

        private string GetValue() {
            string value = "";
            switch (Type)
            {
                case "field":
                    value = ((Field)InputContentView).Text;
                    break;
                case "textarea":
                    value = ((Textarea)InputContentView).Text;
                    break;
                case "dropdown":
                    value = ((Dropdown)InputContentView).SelectedItem;
                    break;
                case "button":
                    value = leftParagraph.Text;
                    break;
                default:
                    break;

            };
            return value;
        }

        private string SetValue(string value)
        {
            switch (Type)
            {
                case "field":
                    ((Field)InputContentView).Text = value;
                    break;
                case "textarea":
                    ((Textarea)InputContentView).Text = value;
                    break;
                case "dropdown":
                    ((Dropdown)InputContentView).SelectedItem = value;
                    break;
                case "button":
                    leftParagraph.Text = value;
                    break;
                default:
                    break;

            };
            return value;
        }
    }
}