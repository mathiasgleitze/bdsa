﻿using BDSA.GroupITApp.Views.Text;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace BDSA.GroupITApp.Views.Content
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class ChatMessage : ContentView
	{
		public ChatMessage (string sender, string messageText, DateTime dateTime, bool hasSent)
		{
			InitializeComponent ();
            int leftColumnWidth = hasSent ? 2 : 8;
            int rightColumnWidth = 10 - leftColumnWidth;

            leftColumnDefinition.Width = new GridLength(leftColumnWidth, GridUnitType.Star);
            rightColumnDefinition.Width = new GridLength(rightColumnWidth, GridUnitType.Star);

            Color textColor = Color.White;
            Headline3 studentName = new Headline3(sender);
            Paragraph paragraph = new Paragraph(messageText) { TextColor = textColor };
            string timeString = dateTime.Day + "/" + dateTime.Month + "/" + dateTime.Year + " " + dateTime.Hour + ":" + dateTime.Minute;
            Headline4 sentTime = new Headline4("(Sent " + timeString + ")") { TextColor = textColor };

            StackLayout chatMessageContainer = hasSent ? rightChatMessageContainer : leftChatMessageContainer;
            StackLayout hiddenChatMessageContainer = hasSent ? leftChatMessageContainer : rightChatMessageContainer;
            hiddenChatMessageContainer.Opacity = 0;

            StackLayout chatMessageTextContainer = hasSent ? rightChatMessageTextContainer : leftChatMessageTextContainer;

            if(!hasSent) chatMessageContainer.Children.Insert(0,studentName);
            chatMessageTextContainer.Children.Add(paragraph);
            chatMessageTextContainer.Children.Add(sentTime);
            chatMessageContainer.Padding = new Thickness(10, 10, 10, 10);
        }
	}
}