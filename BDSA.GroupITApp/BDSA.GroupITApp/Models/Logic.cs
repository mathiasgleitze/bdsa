using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BDSA.GroupITApp.Models.Repositories;
using BDSA.GroupITApp.Models.CompareAlgorithm;
namespace BDSA.GroupITApp.Models
{
    public class Logic : ILogic
    {
        private HttpClient _client;
        private IChatRepository _chatRepository;
        private IMatchRepository _matchRepository;
        private IMatchRequestRepository _matchRequestRepository;
        private IGroupRepository _groupRepository;
        private IGroupRequestRepository _groupRequestRepository;
        private IStudentCourseRepository _studentCourseRepository;
        private IStudentGroupRepository _studentGroupRepository;
        private IProjectRepository _projectRepository;
        private IProjectProfileRepository _projectProfileRepository;
        private IMessageRepository _messageRepository;
        private IStudentRepository _studentRepository;
        private ICourseRepository _courseRepository;
        private ICompareAlgorithm _compareAlgorithm;

        public Logic(HttpClient client)
        {
            _client = client;
            _chatRepository = new ChatRepository(_client);
            _messageRepository = new MessageRepository(_client);
            _matchRepository = new MatchRepository(_client);
            _matchRequestRepository = new MatchRequestRepository(_client);
            _groupRepository = new GroupRepository(_client);
            _groupRequestRepository = new GroupRequestRepository(_client);
            _studentCourseRepository = new StudentCourseRepository(_client);
            _studentGroupRepository = new StudentGroupRepository(_client);
            _projectRepository = new ProjectRepository(_client);
            _projectProfileRepository = new ProjectProfileRepository(_client);
            _studentRepository = new StudentRepository(_client);
            _courseRepository = new CourseRepository(_client);
            _compareAlgorithm = new SimpleCompareAlgoritm(_client);
        }
        public async Task<int> GetGroup(int studentId,int projectId)
        {
            var groups = await _studentGroupRepository.ReadGroupsAsync(studentId);
            var entity = from g in groups
                         where g.ProjectId == projectId
                         select g;
            return entity.FirstOrDefault().Id;
        }
        public async Task<int> GetGroupChat(int groupId)
        {
            var group = await _groupRepository.FindAsync(groupId);
            return group.ChatId;
        }
        public async Task<ProjectDTO>AddCourseName(int projectId)
        {
            var project = await _projectRepository.FindAsync(projectId);
            var course = await _courseRepository.FindAsync(project.CourseId);
            project.CourseName = course.Name;
            return project;
        }
        public async Task<(bool,string)>SendGroupRequest(int fromGroupId,int toGroupId)
        {
            var request = await _groupRequestRepository.FindAsync(fromGroupId, toGroupId);
            if (request != null)
            {
                return (false, "A GroupRequest already exists");
            }
            var fromGroup = await _groupRepository.FindAsync(fromGroupId);
            var fromGroupStudent = await _studentGroupRepository.ReadStudentsAsync(fromGroupId);

            var toGroup = await _groupRepository.FindAsync(toGroupId);
            var toGroupStudent = await _studentGroupRepository.ReadStudentsAsync(toGroupId);

            var project = await _projectRepository.FindAsync(toGroup.ProjectId);

            var studentNumber = fromGroupStudent.Count() + toGroupStudent.Count();
            if (studentNumber > project.MaxSize)
            {
                return (false, "The new group will be bigger than the allowed");
            }
            var result = await _groupRequestRepository.CreateAsync(
                new GroupRequestDTO { FromGroup = fromGroupId, ToGroup = toGroupId }
                );
            if (result == null)
            {
                return (false, "Error not created");
            }
            return (true, "");

           
        }
        public async Task<(bool,string)>DeclineGroupRequest(int fromGroup,int toGroup)
        {
            var result = await _groupRequestRepository.DeleteAsync(fromGroup, toGroup);
            if (result)
            {
                return (true, "GroupRequest has been deleted");
            }
            return (false, "GroupRequest not found");
        }

        public async Task <IEnumerable<StudentProjectDTO>>ViewGroup(int groupId)
        {
            var group = await _groupRepository.FindAsync(groupId);
            var students = await _studentGroupRepository.ReadStudentsAsync(groupId);
            var studentProjectDtos = new List<StudentProjectDTO>();
            
            foreach(StudentDTO s in students)
            {
                var pProfile = await _projectProfileRepository.FindAsync(s.Id, group.ProjectId);
                var sProfile = await GetStudentProject(group.ProjectId, s.Id);
                studentProjectDtos.Add(sProfile);
            }
            return studentProjectDtos;
        }
        public async Task<List<(int groupId,IEnumerable<StudentProjectDTO> students)>>FindGroupForSwiping(int groupId,int projectId)
        { 
            var bestGroups = new List<(int, int, IEnumerable<StudentProjectDTO>)>();

            
            var groupInproject = await _groupRepository.ReadProjectAsync(projectId);

       
            foreach (GroupDTO i in groupInproject)
            {
                if (i.Id != groupId)
                {
                    var match = await _matchRepository.FindAsync(groupId, i.Id);
                    if (match == null)
                    {
                        var matchRequest = await _matchRequestRepository.FindAsync(groupId, i.Id);
                        if (matchRequest == null)
                        {
                            var viewGroup = await ViewGroup(i.Id);
                            var score = await _compareAlgorithm.Compare(groupId, i.Id);

                            if (score != -1)
                            {
                                bestGroups.Add((score, i.Id, viewGroup));
                            }
                        }
                    }
                }
            }
            var OrderedBestGroup = from b in bestGroups
                                 orderby b.Item1
                                 select (b.Item2, b.Item3);

            
            return OrderedBestGroup.ToList();
        }
        public async Task<(bool,string)> MergeGroup(int GroupOneId,int GroupTwoId)
        {
            await _groupRequestRepository.DeleteAsync(GroupOneId, GroupTwoId);
            

            var groupOneStudent = await _studentGroupRepository.ReadStudentsAsync(GroupOneId);

            var groupTwo = await _groupRepository.FindAsync(GroupTwoId);
            var groupTwoStudent = await _studentGroupRepository.ReadStudentsAsync(GroupTwoId);

            var project = await _projectRepository.FindAsync(groupTwo.ProjectId);

            var studentNumber = groupOneStudent.Count() + groupTwoStudent.Count();
            if (studentNumber > project.MaxSize)
            {
                return (false, "The new group will be bigger than the allowed");
            }


            await _matchRequestRepository.DeleteAllAsync(GroupTwoId);

            var match = await _matchRepository.FindAsync(GroupOneId, GroupTwoId);
            if (match == null)
            {
                return (false, "Match not found");
            }



            await _messageRepository.DeleteMessagesAsync(match.ChatId);
            

            await _chatRepository.DeleteAsync(match.ChatId);
            
            await _matchRepository.UpdateAsync(GroupTwoId, GroupOneId);
            await _groupRequestRepository.UpdateAsync(GroupTwoId, GroupOneId);

            var studentsDTO = await _studentGroupRepository.ReadStudentsAsync(GroupTwoId);
            foreach(StudentDTO s in studentsDTO)
            {
                var delete= await _studentGroupRepository.DeleteAsync(s.Id, GroupTwoId);
                if (!delete)
                {
                    return (false, "Student not found");
                }
                await _studentGroupRepository.CreateAsync(new StudentGroupDTO { StudentId = s.Id, GroupId = GroupOneId });
            }

            var group = await _groupRepository.FindAsync(GroupTwoId);

            var deleteMessagesGroup = await _messageRepository.DeleteMessagesAsync(group.ChatId);
            if (!deleteMessagesGroup)
            {
                //return (false, "Messages not found");
            }
            var deleteGroup = await _groupRepository.DeleteAsync(GroupTwoId);
            if (!deleteGroup)
            {
               // return (false, "Group not found");
            }
            var deleteChatGroup = await _chatRepository.DeleteAsync(group.ChatId);
            if (!deleteChatGroup)
            {
               //// return (false, "Chat not found");
            }
            return (true, "");

        }
        public async Task<int>FindByEmail(string mail)
        {
            var email= await _studentRepository.FindIdByMailAsync(mail);
            return email;
        }

        public async Task<ProjectDTO>GetProject(int groupId)
        {
            var group = await _groupRepository.FindAsync(groupId);
            return await _projectRepository.FindAsync(group.ProjectId);
        }
        public async Task<(bool,string)>CreateMatchRequest(int fromGroup,int toGroup)
        {
            var exist = await _matchRequestRepository.FindAsync(toGroup, fromGroup);
            
            if (exist != null)
            {
               await CreateMatch(fromGroup, toGroup);
                return (true, "");
            }
            else
            {
                var matchRequest = new MatchRequestDTO { FromGroup = fromGroup, ToGroup = toGroup };
                await _matchRequestRepository.CreateAsync(matchRequest);
                return (true, "");
            }
        }
        public async Task<(bool, string)> CreateMatch(int fromGroup, int toGroup)
        {
            var deletedMatchRequest = await _matchRequestRepository.DeleteAsync(toGroup, fromGroup);
           
            var chatDTO = new ChatDTO();
            var chat = await _chatRepository.CreateAsync(chatDTO);
            if (chat == null)
            {
                return (false, "Chat not created");
            }

            var matchDTO = new MatchDTO
            {
                ChatId = chat.Id,
                GroupOne = fromGroup,
                GroupTwo = toGroup
            };
            var match = await _matchRepository.CreateAsync(matchDTO);
            if (match == null)
            {

                return (false, "Match not created");
            }

            return (true, "");
        }
        public async Task<(bool, string)> RemoveGroupMember(int studentId, int groupId)
        {
            var studentGroupRepository = new StudentGroupRepository(_client);
            var groupRepository = new GroupRepository(_client);
            var chatRepository = new ChatRepository(_client);
            
            var deleted = await studentGroupRepository.DeleteAsync(studentId, groupId);
            if (!deleted)
            {
                return (false, "Old student group connection not deleted");
            }

            var chatDTO = new ChatDTO();
            var chat = await chatRepository.CreateAsync(chatDTO);
            if (chat == null)
            {
                return (false, "Chat not created");
            }

            var oldGroup = await groupRepository.FindAsync(groupId);
            if (oldGroup == null)
            {
                return (false, $"Old group with ID {groupId} not found");
            }

            var newGroupDTO = new GroupDTO
            {
                ChatId = chat.Id,
                ProjectId = oldGroup.ProjectId
            };

            var newGroup = await groupRepository.CreateAsync(newGroupDTO);
            if (newGroup == null)
            {
                return (false, "New group not created");
            }

            var studentGroupDTO = new StudentGroupDTO
            {
                GroupId = newGroup.Id,
                StudentId = studentId

            };

            var studentGroup = await studentGroupRepository.CreateAsync(studentGroupDTO);
            if (studentGroup == null)
            {
                return (false, "New student group connection not created");
            }

            return (true, "");
        }
        public async Task<List<(int groupId, IEnumerable<StudentProjectDTO> students)>> Search(int projectId, int studentId, string input)
        {
            var results = new List<(int, IEnumerable<StudentProjectDTO>)>();

            var project = await _projectRepository.FindAsync(projectId);
            if (project == null)
            {
                return results;
            }

            
            var studentsInCourse = await _studentCourseRepository.ReadStudentsAsync(project.CourseId);
            if (!studentsInCourse.Any())
            {
                return results;
            }
            var matchingStudents = await _studentRepository.FindStudentsByNameAsync(input);
            if (matchingStudents==null)
            {
                return results;
            }
            
            foreach (var student in matchingStudents)
            {
                if (student.Id != studentId) //Not the student searching
                {
                    var check = await _studentCourseRepository.FindAsync(student.Id, project.CourseId);
                    if (check!=null)
                    {
                        var groups = await _studentGroupRepository.ReadGroupsAsync(student.Id);
                        foreach (var group in groups)
                        {
                            if (group.ProjectId == projectId)
                            {
                                results.Add((group.Id, await ViewGroup(group.Id)));
                            }
                        }
                    }
                }
            }
            return results;
        }
        public async Task<(bool,string)> SendMessage(int studentId,int chatId,string text)
        {
            var message = new MessageDTO
            {
                StudentId = studentId,
                ChatId = chatId,
                Text = text,
                Time = DateTime.Now
                
            };
            var result = await _messageRepository.CreateAsync(message);
            if (result == null)
            {
                return (false, "message not created");
            }

            return (true, "");
        }

        public async Task<List<ProjectDTO>> GetProjects(int studentId)
        {
            var groups = await _studentGroupRepository.ReadGroupsAsync(studentId);

            var projectIds = new List<int>();
            foreach (var group in groups)
            {
                if (!projectIds.Contains(group.ProjectId))
                {
                    projectIds.Add(group.ProjectId);
                }
            }

            var projectDTOs = new List<ProjectDTO>();
            foreach (var id in projectIds)
            {
                projectDTOs.Add(await AddCourseName(id));
            }

            return projectDTOs;
        }

        public async Task<StudentDTO> GetProfile(int studentId)
        {
            return await _studentRepository.FindAsync(studentId);
        }

        public async Task<ProjectProfileDTO> GetProjectProfile(int studentId, int projectId)
        {
            var profile = await _projectProfileRepository.FindAsync(studentId, projectId);
            return profile;
        }

        public async Task<bool> UpdateProfile(StudentDTO student)
        {
            return await _studentRepository.UpdateAsync(student);
        }

        public async Task<bool> UpdateProjectProfile(ProjectProfileDTO projectProfile)
        {
            return await _projectProfileRepository.UpdateAsync(projectProfile);
        }

        public async Task<List<MatchDTO>> GetMatches(int studentId)
        {
            var groups = await _studentGroupRepository.ReadGroupsAsync(studentId);

            var matches = new List<MatchDTO>();
            foreach (var group in groups)
            {
                matches.AddRange(await _matchRepository.FindAllMatchesAsync(group.Id));
            }

            return matches;
        }

        public async Task<List<MessageDTO>> GetMessages(int groupId)
        {
            var group = await _groupRepository.FindAsync(groupId);
            if (group == null)
            {
                return new List<MessageDTO>();
            }
            var messages = await _messageRepository.ReadAsync(group.ChatId);
            if (messages == null)
            {
                return new List<MessageDTO>();
            }
            return messages.ToList();
        }

        public async Task<(List<GroupRequestDTO>, List<MatchDTO>)> GetNotifications(int studentId)
        {
            var groups = await _studentGroupRepository.ReadGroupsAsync(studentId);
            var finalMatches = new List<MatchDTO>();
            var finalrequests = new List<GroupRequestDTO>();
            foreach (var group in groups)
            {
                var requests = await _groupRequestRepository.ReadToAsync(group.Id);
                foreach(var request in requests)
                {
                    finalrequests.Add(request);
                }
                var matches = await _matchRepository.FindAllMatchesAsync(group.Id);
                foreach (var match in matches)
                {
                    var messages = await _messageRepository.ReadAsync(match.ChatId);
                    if (!messages.Any())
                    {
                        finalMatches.Add(match);
                    }
                }
            }
            
            return (finalrequests, finalMatches);
        }
        public async Task<(bool,string)>IsProjectProfileValid(int projectId,int studentId)
        {
            var pProfile = await _projectProfileRepository.FindAsync(studentId, projectId);
            if (pProfile.Ambitions == 0
                || pProfile.Flexibility == 0
                || pProfile.MeetingDisciplin == 0
                || pProfile.SkillLevel == 0)
            {
                return (false, "Profile not valid");
            }
            return (true, "");
        }

        public async Task<StudentProjectDTO> GetStudentProject(int projectId, int studentId)
        {
            var student = await _studentRepository.FindAsync(studentId);
            var pProfile = await _projectProfileRepository.FindAsync(studentId, projectId);
            var sProfile = new StudentProjectDTO()
            {
                Id = student.Id,
                Name = student.Name,
                Mail = student.Mail,
                Address = student.Address,
                AboutStudent = student.About,
                AboutProject = pProfile.About,
                Ambitions = pProfile.Ambitions,
                MeetingDisciplin = pProfile.MeetingDisciplin,
                Flexibility = pProfile.Flexibility,
                SkillLevel = pProfile.SkillLevel
            };
            return sProfile;
        }
    }
}

