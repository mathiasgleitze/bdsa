﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.GroupITApp.Models
{
    public interface ISettings
    {
        string ClientId { get; }
        IReadOnlyCollection<string> Scopes { get; }
        string TenantId { get; }
    }
}
