﻿using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface ITeacherRepository
    {
        Task<TeacherDTO> CreateAsync(TeacherDTO teacher);
        Task<bool> DeleteAsync(int id);
        Task<TeacherDTO> FindAsync(int id);
        Task<bool> UpdateAsync(TeacherDTO teacher);
    }
}