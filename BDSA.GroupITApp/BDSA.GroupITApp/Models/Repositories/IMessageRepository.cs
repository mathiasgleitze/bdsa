﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface IMessageRepository
    {
        Task<MessageDTO> CreateAsync(MessageDTO message);
        Task<bool> UpdateAsync(MessageDTO message);
        Task<MessageDTO> FindAsync(int chatId, DateTime time);
        Task<bool> DeleteAsync(int chatId, DateTime time);
        Task<bool> DeleteMessagesAsync(int chatId);
        Task<IEnumerable<MessageDTO>> ReadAsync(int chatId);
    }
}
