﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface IStudentCourseRepository
    {
        Task<StudentCourseDTO> CreateAsync(StudentCourseDTO studentCourse);
        Task<bool> DeleteAsync(int studentId, int courseId);
        Task<IEnumerable<StudentDTO>> ReadStudentsAsync(int id);
        Task<IEnumerable<CourseDTO>> ReadCoursesAsync(int id);
        Task<StudentCourseDTO> FindAsync(int studentId, int courseId);
    }
}