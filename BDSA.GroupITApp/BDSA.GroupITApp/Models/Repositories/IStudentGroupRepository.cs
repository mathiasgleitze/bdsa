﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface IStudentGroupRepository
    {
        Task<StudentGroupDTO> CreateAsync(StudentGroupDTO studentGroup);
        Task<bool> DeleteAsync(int studentId, int groupId);
        Task<IEnumerable<StudentDTO>> ReadStudentsAsync(int id);        
        Task<IEnumerable<GroupDTO>> ReadGroupsAsync(int id);        
        Task<StudentGroupDTO> FindAsync(int studentId, int groupId);
    }
}