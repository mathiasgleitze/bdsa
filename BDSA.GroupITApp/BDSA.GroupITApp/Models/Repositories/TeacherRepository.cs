﻿using System.Net.Http;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class TeacherRepository : ITeacherRepository
    {
        private readonly HttpClient _client;
        private readonly string api = "api/teacher";

        public TeacherRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<TeacherDTO> CreateAsync(TeacherDTO teacher)
        {
            var response = await _client.PostAsJsonAsync(api, teacher);

            return await response.Content.ReadAsAsync<TeacherDTO>();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var response = await _client.DeleteAsync($"{api}/{id}");

            return response.IsSuccessStatusCode;
        }

        public async Task<TeacherDTO> FindAsync(int id)
        {
            var response = await _client.GetAsync($"{api}/{id}");

            return await response.Content.ReadAsAsync<TeacherDTO>();
        }

        public async Task<bool> UpdateAsync(TeacherDTO teacher)
        {
            var response = await _client.PutAsJsonAsync($"{api}/{teacher.Id}", teacher);

            return response.IsSuccessStatusCode;
        }
    }
}
