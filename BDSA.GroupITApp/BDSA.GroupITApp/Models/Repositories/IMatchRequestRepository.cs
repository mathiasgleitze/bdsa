﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface IMatchRequestRepository
    {
        Task<MatchRequestDTO> CreateAsync(MatchRequestDTO match);
        Task<bool> DeleteAsync(int fromGroup, int toGroup);
        Task<bool> DeleteAllAsync(int group);
        Task<IEnumerable<MatchRequestDTO>> GetFromAsync(int id);
        Task<IEnumerable<MatchRequestDTO>> GetToAsync(int id);
        Task<MatchRequestDTO> FindAsync(int fromGroup, int toGroup);
    }
}