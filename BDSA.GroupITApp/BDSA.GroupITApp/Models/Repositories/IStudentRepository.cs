﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface IStudentRepository
    {
        Task<StudentDTO> CreateAsync(StudentDTO student);
        Task<bool> DeleteAsync(int id);
        Task<StudentDTO> FindAsync(int id);
        Task<IEnumerable<StudentDTO>> FindStudentsByNameAsync(string name);
        Task<int> FindIdByMailAsync(string mail);
        Task<bool> UpdateAsync(StudentDTO student);
    }
}