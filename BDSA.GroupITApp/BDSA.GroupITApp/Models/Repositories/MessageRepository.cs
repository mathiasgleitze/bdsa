﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class MessageRepository : IMessageRepository
    {
        private readonly HttpClient _client;
        private readonly string api = "api/message";

        public MessageRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<MessageDTO> CreateAsync(MessageDTO message)
        {
            var response = await _client.PostAsJsonAsync(api, message);

            return await response.Content.ReadAsAsync<MessageDTO>();
        }

        public async Task<bool> UpdateAsync(MessageDTO message)
        {
            var response = await _client.PutAsJsonAsync($"{api}/{message.ChatId}", message);

            return response.IsSuccessStatusCode;
        }

        public async Task<MessageDTO> FindAsync(int chatId, DateTime time)
        {
            var response = await _client.GetAsync($"{api}/{chatId}/{time}");

            return await response.Content.ReadAsAsync<MessageDTO>();
        }

        public async Task<bool> DeleteAsync(int chatId, DateTime time)
        {
            var response = await _client.DeleteAsync($"{api}/{chatId}/{time}");

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteMessagesAsync(int chatId)
        {
            var response = await _client.DeleteAsync($"{api}/{chatId}");

            return response.IsSuccessStatusCode;
        }

        public async Task<IEnumerable<MessageDTO>> ReadAsync(int chatId)
        {
            var response = await _client.GetAsync($"{api}/{chatId}");

            return await response.Content.ReadAsAsync<IEnumerable<MessageDTO>>();
        }
    }
}
