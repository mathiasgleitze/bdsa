﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface ITeacherCourseRepository
    {
        Task<TeacherCourseDTO> CreateAsync(TeacherCourseDTO teacherCourse);
        Task<bool> DeleteAsync(int teacherId, int courseId);
        Task<IEnumerable<TeacherCourseDTO>> FindAllTeacherCoursesAsync(int id);
        Task<TeacherCourseDTO> FindAsync(int teacherId, int courseId);
    }
}