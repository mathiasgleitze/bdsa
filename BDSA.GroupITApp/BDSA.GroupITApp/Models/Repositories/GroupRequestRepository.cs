﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class GroupRequestRepository : IGroupRequestRepository
    {
        private readonly HttpClient _client;

        public GroupRequestRepository(HttpClient client)
        {
        _client = client;
        }
    
        public async Task<GroupRequestDTO> CreateAsync(GroupRequestDTO request)
        {
            var response = await _client.PostAsJsonAsync("api/grouprequest", request);

            return await response.Content.ReadAsAsync<GroupRequestDTO>();
        }

        public async Task<bool> DeleteAsync(int fromGroup, int toGroup)
        {
            var response = await _client.DeleteAsync($"api/grouprequest/{fromGroup}/{toGroup}");

            return response.IsSuccessStatusCode;
        }


        public async Task<GroupRequestDTO> FindAsync(int fromGroup, int toGroup)
        {
            var response = await _client.GetAsync($"api/grouprequest/{fromGroup}/{toGroup}");

            return await response.Content.ReadAsAsync<GroupRequestDTO>();
        }

        public async Task<IEnumerable<GroupRequestDTO>> ReadFromAsync(int group)
        {
            var response = await _client.GetAsync($"api/grouprequest/from/{group}");

            return await response.Content.ReadAsAsync<IEnumerable<GroupRequestDTO>>();
        }

        public async Task<IEnumerable<GroupRequestDTO>> ReadToAsync(int group)
        {
            var response = await _client.GetAsync($"api/grouprequest/to/{group}");

            return await response.Content.ReadAsAsync<IEnumerable<GroupRequestDTO>>();
        }
        
        public async Task<bool> UpdateAsync(int oldgroup, int newgroup)
        {
            var response = await _client.PutAsync($"api/grouprequest/update/{oldgroup}/{newgroup}",new StringContent(string.Empty));

            return response.IsSuccessStatusCode;
        }
    }
}
