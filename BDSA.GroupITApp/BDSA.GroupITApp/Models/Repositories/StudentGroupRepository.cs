using BDSA.Shared;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class StudentGroupRepository : IStudentGroupRepository
    {
        private readonly HttpClient _client;

        public StudentGroupRepository(HttpClient client)
        {
            _client = client;
        }
        public async Task<StudentGroupDTO> CreateAsync(StudentGroupDTO studentGroup)
        {
            var response = await _client.PostAsJsonAsync("api/studentgroup", studentGroup);

            return await response.Content.ReadAsAsync<StudentGroupDTO>();
        }

        public async Task<bool> DeleteAsync(int studentId, int groupId)
        {
            var response = await _client.DeleteAsync($"api/studentgroup/{studentId}/{groupId}");

            return response.IsSuccessStatusCode;
        }

        public async Task<IEnumerable<StudentDTO>> ReadStudentsAsync(int id)
        {
            var response = await _client.GetAsync($"api/studentgroup/student/{id}");

            return await response.Content.ReadAsAsync<IEnumerable<StudentDTO>>();
        }

        public async Task<IEnumerable<GroupDTO>> ReadGroupsAsync(int id)
        {
            var response = await _client.GetAsync($"api/studentgroup/group/{id}");

            return await response.Content.ReadAsAsync<IEnumerable<GroupDTO>>();
        }

        public async Task<StudentGroupDTO> FindAsync(int studentId, int groupId)
        {
            var response = await _client.GetAsync($"api/studentgroup/{studentId}/{groupId}");

            return await response.Content.ReadAsAsync<StudentGroupDTO>();
        }
    }
}

