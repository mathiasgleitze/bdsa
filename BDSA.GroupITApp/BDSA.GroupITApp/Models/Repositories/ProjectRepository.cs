﻿using System.Net.Http;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly HttpClient _client;
        private readonly string api = "api/project";

        public ProjectRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<ProjectDTO> CreateAsync(ProjectDTO project)
        {
            var response = await _client.PostAsJsonAsync(api, project);

            return await response.Content.ReadAsAsync<ProjectDTO>();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var response = await _client.DeleteAsync($"{api}/{id}");

            return response.IsSuccessStatusCode;
        }

        public async Task<ProjectDTO> FindAsync(int id)
        {
            var response = await _client.GetAsync($"{api}/{id}");

            return await response.Content.ReadAsAsync<ProjectDTO>();
        }

        public async Task<bool> UpdateAsync(ProjectDTO project)
        {
            var response = await _client.PutAsJsonAsync($"{api}/{project.Id}", project);

            return response.IsSuccessStatusCode;
        }
    }
}
