﻿using System.Net.Http;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class CourseRepository : ICourseRepository
    {
        private readonly HttpClient _client;

        public CourseRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<CourseDTO> CreateAsync(CourseDTO course)
        {
            var response = await _client.PostAsJsonAsync("api/course", course);

            return await response.Content.ReadAsAsync<CourseDTO>();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var response = await _client.DeleteAsync($"api/course/{id}");

            return response.IsSuccessStatusCode;
        }

        public async Task<CourseDTO> FindAsync(int id)
        {
            var response = await _client.GetAsync($"api/course/{id}");

            return await response.Content.ReadAsAsync<CourseDTO>();
        }

        public async Task<bool> UpdateAsync(CourseDTO course)
        {
            var response = await _client.PutAsJsonAsync($"api/course/{course.Id}",course);

            return response.IsSuccessStatusCode;
        }
    }
}
