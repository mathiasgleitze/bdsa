﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface IGroupRepository
    {
        Task<GroupDTO> CreateAsync(GroupDTO group);
        Task<bool> DeleteAsync(int groupId);
        Task<GroupDTO> FindAsync(int groupId);
        Task<bool> UpdateAsync(GroupDTO group);
        Task<IEnumerable<GroupDTO>> ReadAsync();
        Task<IEnumerable<GroupDTO>> ReadProjectAsync(int projectId);
    }
}