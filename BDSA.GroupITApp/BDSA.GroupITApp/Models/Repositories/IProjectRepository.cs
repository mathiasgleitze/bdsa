﻿using BDSA.Shared;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface IProjectRepository
    {
        Task<ProjectDTO> CreateAsync(ProjectDTO project);
        Task<bool> DeleteAsync(int id);
        Task<ProjectDTO> FindAsync(int id);
        Task<bool> UpdateAsync(ProjectDTO project);
    }
}
