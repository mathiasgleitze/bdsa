﻿using BDSA.Shared;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class MatchRequestRepository : IMatchRequestRepository
    {
        private readonly HttpClient _client;

        public MatchRequestRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<MatchRequestDTO> CreateAsync(MatchRequestDTO matchRequest)
        {
            var response = await _client.PostAsJsonAsync("api/matchRequest", matchRequest);

            return await response.Content.ReadAsAsync<MatchRequestDTO>();
        }

        public async Task<bool> DeleteAsync(int fromGroup, int toGroup)
        {
            var response = await _client.DeleteAsync($"api/matchRequest/{fromGroup}/{toGroup}");

            return response.IsSuccessStatusCode;
        }

        public async Task<bool> DeleteAllAsync(int groupId)
        {
            var response = await _client.DeleteAsync($"api/matchrequest/{groupId}");

            return response.IsSuccessStatusCode;
        }

        public async Task<IEnumerable<MatchRequestDTO>> GetFromAsync(int id)
        {
            var response = await _client.GetAsync($"api/matchRequest/from/{id}");

            return await response.Content.ReadAsAsync<IEnumerable<MatchRequestDTO>>();
        }

        public async Task<IEnumerable<MatchRequestDTO>> GetToAsync(int id)
        {
            var response = await _client.GetAsync($"api/matchRequest/to/{id}");

            return await response.Content.ReadAsAsync<IEnumerable<MatchRequestDTO>>();
        }

        public async Task<MatchRequestDTO> FindAsync(int fromGroup, int toGroup)
        {
            var response = await _client.GetAsync($"api/matchRequest/{fromGroup}/{toGroup}");

            return await response.Content.ReadAsAsync<MatchRequestDTO>();
        }
    }
}
