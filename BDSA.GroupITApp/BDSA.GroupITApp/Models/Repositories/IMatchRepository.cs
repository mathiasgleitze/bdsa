﻿using BDSA.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface IMatchRepository
    {
        Task<MatchDTO> CreateAsync(MatchDTO match);
        Task<bool> DeleteAsync(int groupOne, int groupTwo);
        Task<MatchDTO> FindAsync(int groupOne, int groupTwo);
        Task<IEnumerable<MatchDTO>> FindAllMatchesAsync(int id);
        Task<bool> UpdateAsync(int oldgroup, int newgroup);
    }
}
