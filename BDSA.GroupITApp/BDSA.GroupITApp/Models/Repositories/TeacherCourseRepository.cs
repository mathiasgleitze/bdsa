﻿using BDSA.Shared;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class TeacherCourseRepository : ITeacherCourseRepository
    {
        private readonly HttpClient _client;

        public TeacherCourseRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<TeacherCourseDTO> CreateAsync(TeacherCourseDTO teacherCourse)
        {
            var response = await _client.PostAsJsonAsync("api/teacherCourse", teacherCourse);

            return await response.Content.ReadAsAsync<TeacherCourseDTO>();
        }

        public async Task<bool> DeleteAsync(int teacherId, int courseId)
        {
            var response = await _client.DeleteAsync($"api/teacherCourse/{teacherId}/{courseId}");

            return response.IsSuccessStatusCode;
        }

        public async Task<IEnumerable<TeacherCourseDTO>> FindAllTeacherCoursesAsync(int id)
        {
            var response = await _client.GetAsync($"api/teacherCourse/{id}");

            return await response.Content.ReadAsAsync<IEnumerable<TeacherCourseDTO>>();
        }

        public async Task<TeacherCourseDTO> FindAsync(int teacherId, int courseId)
        {
            var response = await _client.GetAsync($"api/teacherCourse/{teacherId}/{courseId}");

            return await response.Content.ReadAsAsync<TeacherCourseDTO>();
        }
    }
}
