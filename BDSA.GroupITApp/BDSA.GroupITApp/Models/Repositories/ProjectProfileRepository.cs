﻿using BDSA.Shared;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class ProjectProfileRepository : IProjectProfileRepository
    {
        private readonly HttpClient _client;

        public ProjectProfileRepository(HttpClient client)
        {
            _client = client;
        }
        public async Task<ProjectProfileDTO> CreateAsync(ProjectProfileDTO projectProfile)
        {
            var response = await _client.PostAsJsonAsync("api/projectProfile", projectProfile);

            return await response.Content.ReadAsAsync<ProjectProfileDTO>();
        }

        public async Task<bool> DeleteAsync(int studentId, int projectId)
        {
            var response = await _client.DeleteAsync($"api/projectProfile/{studentId}/{projectId}");

            return response.IsSuccessStatusCode;
        }

        public async Task<IEnumerable<ProjectProfileDTO>> FindAllProjectProfilesAsync(int id)
        {
            var response = await _client.GetAsync($"api/projectProfile/{id}");

            return await response.Content.ReadAsAsync<IEnumerable<ProjectProfileDTO>>();
        }

        public async Task<ProjectProfileDTO> FindAsync(int studentId, int projectId)
        {
            var response = await _client.GetAsync($"api/projectProfile/{studentId}/{projectId}");
            
            return await response.Content.ReadAsAsync<ProjectProfileDTO>();
        }

        public async Task<bool> UpdateAsync(ProjectProfileDTO projectProfile)
        {
            var response = await _client.PutAsJsonAsync($"api/projectprofile/{projectProfile.StudentId}/{projectProfile.ProjectId}", projectProfile);

            return response.IsSuccessStatusCode;
        }
    }
}
