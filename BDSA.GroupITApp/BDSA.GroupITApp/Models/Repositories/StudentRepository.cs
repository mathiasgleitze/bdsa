﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class StudentRepository : IStudentRepository
    {
        private readonly HttpClient _client;
        private readonly string api = "api/student";

        public StudentRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<StudentDTO> CreateAsync(StudentDTO student)
        {
            var response = await _client.PostAsJsonAsync(api, student);

            return await response.Content.ReadAsAsync<StudentDTO>();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var response = await _client.DeleteAsync($"{api}/{id}");

            return response.IsSuccessStatusCode;
        }

        public async Task<StudentDTO> FindAsync(int id)
        {
            var response = await _client.GetAsync($"{api}/{id}");

            return await response.Content.ReadAsAsync<StudentDTO>();
        }

        public async Task<IEnumerable<StudentDTO>> FindStudentsByNameAsync(string name)
        {
            var response = await _client.GetAsync($"{api}/name/{name}");

            return await response.Content.ReadAsAsync<IEnumerable<StudentDTO>>();
        }

        public async Task<int> FindIdByMailAsync(string mail)
        {
            var response = await _client.GetAsync($"{api}/mail/{mail}");

            return await response.Content.ReadAsAsync<int>();
        }
        
        public async Task<bool> UpdateAsync(StudentDTO student)
        {
            var response = await _client.PutAsJsonAsync($"{api}/{student.Id}", student);

            return response.IsSuccessStatusCode;
        }
    }
}