﻿using BDSA.Shared;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface ICourseRepository
    {
        Task<CourseDTO> CreateAsync(CourseDTO course);
        Task<bool> DeleteAsync(int id);
        Task<CourseDTO> FindAsync(int id);
        Task<bool> UpdateAsync(CourseDTO course);
    }
}
