﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class GroupRepository : IGroupRepository
    {
        private readonly HttpClient _client;

        public GroupRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<GroupDTO> CreateAsync(GroupDTO group)
        {
            var response = await _client.PostAsJsonAsync("api/group", group);

            return await response.Content.ReadAsAsync<GroupDTO>();
        }

        public async Task<IEnumerable<GroupDTO>> ReadAsync()
        {
            var response = await _client.GetAsync("api/group");
            return await response.Content.ReadAsAsync<IEnumerable<GroupDTO>>();
        }
        public async Task<IEnumerable<GroupDTO>>ReadProjectAsync(int projectId)
        {
            var response = await _client.GetAsync($"api/group/project/{projectId}");
            return await response.Content.ReadAsAsync<IEnumerable<GroupDTO>>();
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var response = await _client.DeleteAsync($"api/group/{id}");

            return response.IsSuccessStatusCode;
        }

        public async Task<GroupDTO> FindAsync(int id)
        {
            var response = await _client.GetAsync($"api/group/{id}");

            return await response.Content.ReadAsAsync<GroupDTO>();
        }

        public async Task<bool> UpdateAsync(GroupDTO group)
        {
            var response = await _client.PutAsJsonAsync($"api/group/{group.Id}",group);

            return response.IsSuccessStatusCode;
        }
    }
}
