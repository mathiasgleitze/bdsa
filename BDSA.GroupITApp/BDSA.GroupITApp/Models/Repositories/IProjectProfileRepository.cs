﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface IProjectProfileRepository
    {
        Task<ProjectProfileDTO> CreateAsync(ProjectProfileDTO projectProfile);
        Task<bool> DeleteAsync(int studentId, int projectId);
        Task<IEnumerable<ProjectProfileDTO>> FindAllProjectProfilesAsync(int id);
        Task<ProjectProfileDTO> FindAsync(int studentId, int projectId);
        Task<bool> UpdateAsync(ProjectProfileDTO projectProfile);
    }
}