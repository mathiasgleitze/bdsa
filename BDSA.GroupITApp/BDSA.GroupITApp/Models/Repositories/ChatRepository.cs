﻿using System.Net.Http;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class ChatRepository : IChatRepository
    {
        private readonly HttpClient _client;

        public ChatRepository(HttpClient client)
        {
            _client = client;
        }
        public async Task<ChatDTO> CreateAsync(ChatDTO chat)
        {
            var response = await _client.PostAsJsonAsync("api/chat", chat);

            return await response.Content.ReadAsAsync<ChatDTO>();
        }

        public async Task<bool> DeleteAsync(int chatId)
        {
            var response = await _client.DeleteAsync($"api/chat/{chatId}");

            return response.IsSuccessStatusCode;
        }

        public async Task<ChatDTO> FindAsync(int chatId)
        {
            var response = await _client.GetAsync($"api/chat/{chatId}");

            return await response.Content.ReadAsAsync<ChatDTO>();
        }
    }
}
