﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class MatchRepository : IMatchRepository
    {
        private readonly HttpClient _client;

        public MatchRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<MatchDTO> CreateAsync(MatchDTO match)
        {
            var response = await _client.PostAsJsonAsync("api/match", match);

            return await response.Content.ReadAsAsync<MatchDTO>();
        }

        public async Task<bool> DeleteAsync(int groupOne, int groupTwo)
        {
            var response = await _client.DeleteAsync($"api/match/{groupOne}/{groupTwo}");

            return response.IsSuccessStatusCode;
        }
        
        public async Task<MatchDTO> FindAsync(int groupOne, int groupTwo)
        {
            var response = await _client.GetAsync($"api/match/{groupOne}/{groupTwo}");

            return await response.Content.ReadAsAsync<MatchDTO>();
        }

        public async Task<IEnumerable<MatchDTO>> FindAllMatchesAsync(int id)
        {
            var response = await _client.GetAsync($"api/match/{id}");

            return await response.Content.ReadAsAsync<IEnumerable<MatchDTO>>();
        }

        public async Task<bool> UpdateAsync(int oldgroup, int newgroup)
        {
            var response = await _client.PutAsync($"api/match/update/{oldgroup}/{newgroup}", new StringContent(string.Empty));

            return response.IsSuccessStatusCode;
        }
    }
}
