using BDSA.Shared;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.Repositories
{
    public class StudentCourseRepository : IStudentCourseRepository
    {
        private readonly HttpClient _client;

        public StudentCourseRepository(HttpClient client)
        {
            _client = client;
        }

        public async Task<StudentCourseDTO> CreateAsync(StudentCourseDTO studentCourse)
        {
            var response = await _client.PostAsJsonAsync("api/studentCourse", studentCourse);

            return await response.Content.ReadAsAsync<StudentCourseDTO>();
        }

        public async Task<bool> DeleteAsync(int studentId, int courseId)
        {
            var response = await _client.DeleteAsync($"api/studentCourse/{studentId}/{courseId}");

            return response.IsSuccessStatusCode;
        }

        public async Task<IEnumerable<StudentDTO>> ReadStudentsAsync(int id)
        {

            var response = await _client.GetAsync($"api/studentcourse/student/{id}");

            return await response.Content.ReadAsAsync<IEnumerable<StudentDTO>>();
        }

        public async Task<IEnumerable<CourseDTO>> ReadCoursesAsync(int id)
        {
            var response = await _client.GetAsync($"api/studentcourse/course/{id}");

            return await response.Content.ReadAsAsync<IEnumerable<CourseDTO>>();

        }

        public async Task<StudentCourseDTO> FindAsync(int studentId, int courseId)
        {
            var response = await _client.GetAsync($"api/studentCourse/{studentId}/{courseId}");

            return await response.Content.ReadAsAsync<StudentCourseDTO>();
        }


    }
}
