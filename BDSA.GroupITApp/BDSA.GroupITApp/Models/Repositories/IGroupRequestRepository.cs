﻿using BDSA.Shared;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.Repositories
{
    public interface IGroupRequestRepository
    {
        Task<GroupRequestDTO> CreateAsync(GroupRequestDTO request);
        Task<bool> DeleteAsync(int fromGroup,int toGroup);
        Task<GroupRequestDTO> FindAsync(int fromGroup, int toGroup);
        Task<IEnumerable<GroupRequestDTO>> ReadFromAsync(int group);
        Task<IEnumerable<GroupRequestDTO>> ReadToAsync(int group);
        Task<bool> UpdateAsync(int oldgroup, int newgroup);
    }
}
