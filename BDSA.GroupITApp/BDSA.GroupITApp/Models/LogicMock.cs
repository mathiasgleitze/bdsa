﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models
{
    class LogicMock : ILogic
    {
        private List<MessageDTO> messages = new List<MessageDTO>() {
            new MessageDTO(){ StudentId=0, Text="Dette er en besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=1, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=1, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=0, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=2, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=0, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=2, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=3, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=2, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=1, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=0, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=4, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=3, Text="Dette er en anden besked", Time=new DateTime()},
            new MessageDTO(){ StudentId=3, Text="Dette er en anden besked", Time=new DateTime()}
            };


        public Task<(bool, string)> CreateMatch(int fromGroup, int toGroup)
        {
            return null;
        }

        public async Task<List<(int groupId, IEnumerable<StudentProjectDTO> students)>> FindGroupForSwiping(int groupId)
        {
            List <(int, IEnumerable<StudentProjectDTO>)> Groups;

            Groups = new List<(int groupId, IEnumerable<StudentProjectDTO> students)>() {
            (1,
                new List<StudentProjectDTO> {
                new StudentProjectDTO { Name = "Jesper", Mail = "jsam", Ambitions = 1, AboutProject = "Meh", Id = 1},
                new StudentProjectDTO { Name = "My", Mail = "mymo", Ambitions = 2, AboutProject = "Meh", Id = 2}}
            ),
            (2,
                new List<StudentProjectDTO> {
                new StudentProjectDTO { Name = "Martin", Mail = "mdrh", AboutProject = "Meh", Ambitions = 1, Id = 3},
                new StudentProjectDTO { Name = "Mai", Mail = "manb", AboutProject = "Meh", Ambitions = 1, Id = 4}}
            ),
            (3,
                new List<StudentProjectDTO> {
                new StudentProjectDTO { Name = "Mathias", Mail = "hoffnar", AboutProject = "Meh", Ambitions = 1, Id = 5},
                new StudentProjectDTO { Name = "Lucas", Mail = "lukh", AboutProject = "Meh", Ambitions = 1, Id = 6}})
            };

            return Groups;
            
        }

        public Task<List<MatchDTO>> GetMatches(int studentId)
        {
            List<MatchDTO> matches = new List<MatchDTO>
            {
                new MatchDTO { ChatId = 1, GroupOne = 1, GroupTwo = 2},
                new MatchDTO { ChatId = 3, GroupOne = 2, GroupTwo = 4},
                new MatchDTO { ChatId = 5, GroupOne = 2, GroupTwo = 7}
            };
            return Task.FromResult(matches);
        }

        public Task<List<MessageDTO>> GetMessages(int groupId)
        {
            
        return Task.FromResult(messages);
    }

        public async Task<(List<GroupRequestDTO>, List<MatchDTO>)> GetNotifications(int studentId)
        {
            List<GroupRequestDTO> groupRequests = new List<GroupRequestDTO>
            {
                new GroupRequestDTO { FromGroup = 1, ToGroup = 2},
                new GroupRequestDTO { ToGroup = 4, FromGroup = 2}
            };
            List<MatchDTO> matches = new List<MatchDTO>
            {
                new MatchDTO { GroupOne = 2, GroupTwo = 3, ChatId = 1},
                new MatchDTO { GroupOne = 2, GroupTwo = 5, ChatId = 2}
            };
            return await Task.FromResult((groupRequests, matches));


            throw new NotImplementedException();
        }

        public Task<StudentDTO> GetProfile(int studentId)
        {
            StudentDTO student = new StudentDTO { Name = "H", Address = "address", About = "About me", Mail = "kaka", Id = 1 };
            return Task.FromResult(student);
        }

        public Task<ProjectProfileDTO> GetProjectProfile(int studentId, int projectId)
        {
            ProjectProfileDTO profile = new ProjectProfileDTO { Ambitions = 1, Flexibility = 1, SkillLevel = 1, StudentId = 1, MeetingDisciplin = 1, ProjectId = 1, About = "Hej" };
            return Task.FromResult(profile);
        }

        public async Task<List<ProjectDTO>> GetProjects(int studentId)
        {
            List<ProjectDTO> projects = new List<ProjectDTO>();
            projects.Add(new ProjectDTO { CourseId = 1, Id = 1, Name = "BDSAPROJECT", CourseName = "BDSA" });
            projects.Add(new ProjectDTO { CourseId = 1, Id = 2, Name = "ModisMiniP", CourseName = "Modis" });
            return await Task.FromResult(projects);
        }

        public Task<(bool, string)> MergeGroup(int GroupOneId, int GroupTwoId)
        {
            return null;
        }

        public Task<(bool, string)> RemoveGroupMember(int studentId, int groupId)
        {
            throw new NotImplementedException();
        }

        public async Task<List<(int groupId, IEnumerable<StudentProjectDTO> students)>> Search(int projectId, int studentId, string input)
        {
            List<(int, IEnumerable<StudentProjectDTO>)> Groups;

            Groups = new List<(int groupId, IEnumerable<StudentProjectDTO> students)>() {
            (1,
                new List<StudentProjectDTO> {
                new StudentProjectDTO { Name = "Jesper", Mail = "jsam", Ambitions = 1, AboutProject = "Meh", Id = 1},
                new StudentProjectDTO { Name = "My", Mail = "mymo", Ambitions = 2, AboutProject = "Meh", Id = 2}}
            ),
            (2,
                new List<StudentProjectDTO> {
                new StudentProjectDTO { Name = "Martin", Mail = "mdrh", AboutProject = "Meh", Ambitions = 1, Id = 3},
                new StudentProjectDTO { Name = "Mai", Mail = "manb", AboutProject = "Meh", Ambitions = 1, Id = 4}}
            ),
            (3,
                new List<StudentProjectDTO> {
                new StudentProjectDTO { Name = "Mathias", Mail = "hoffnar", AboutProject = "Meh", Ambitions = 1, Id = 5},
                new StudentProjectDTO { Name = "Lucas", Mail = "lukh", AboutProject = "Meh", Ambitions = 1, Id = 6}})
            };

            return Groups;
        }

        public async Task<StudentProjectDTO> GetStudentProject(int projectId, int studentId)
        {
            StudentProjectDTO student = new StudentProjectDTO { Name = "Jesper", Mail = "jsam", Ambitions = 1, AboutProject = "Meh", Id = 1 };
            return student;
        }

        public async Task<(bool, string)> SendMessage(int studentId, int chatId, string text)
        {
            MessageDTO newMessage = new MessageDTO { ChatId = chatId, StudentId = studentId, Text = text, Time = new DateTime() };
            messages.Add(newMessage);
            (bool, string) returnvalue = (true, null);
            return await Task.FromResult(returnvalue);
        }

        public Task<bool> UpdateProfile(StudentDTO student)
        {
            return Task.FromResult(true);
            throw new NotImplementedException();
        }

        public Task<bool> UpdateProjectProfile(ProjectProfileDTO projectProfile)
        {
            return Task.FromResult(true);
            throw new NotImplementedException();
        }

        public Task<IEnumerable<StudentProjectDTO>> ViewGroup(int groupId)
        {
            IEnumerable<StudentProjectDTO> otherGroup = new List<StudentProjectDTO> {
                new StudentProjectDTO { Name = "Dem De andre", Mail = "DDA", Ambitions = 9, AboutProject = "Such Excitement", Id = 1},
                new StudentProjectDTO { Name = "Også En Anden", Mail = "ONA", Ambitions = 8, AboutProject = "Much happiness", Id = 2},
                new StudentProjectDTO { Name = "Break scale?", Mail = "BRES", Ambitions = 11, AboutProject = "Much happiness", Id = 2}};
            return  Task.FromResult(otherGroup);

        }
       
        public async Task<int> GetGroup(int studentId, int projectId)
        {
            return await Task.FromResult(2);
        }

        public async Task<int> GetGroupChat(int groupId)
        {
            return await Task.FromResult(1);
        }

        public Task<ProjectDTO> AddCourseName(int projectId)
        {
            throw new NotImplementedException();
        }

        public Task<(bool, string)> DeclineGroupRequest(int fromGroup, int toGroup)
        {
            return null;
        }

        public async Task<ProjectDTO> GetProject(int groupId)
        {
            return await Task.FromResult(new ProjectDTO { Id = 1, Name = "BDSA Project" });
        }

        public Task<(bool, string)> SendGroupRequest(int fromGroupId, int toGroupId)
        {
            return null;
        }

        public Task<(bool, string)> IsProjectProfileValid(int projectId, int studentId)
        {
            (bool, string) returnValue = (true, "");
            return Task.FromResult(returnValue);
        }
        public async Task<(bool, string)> CreateMatchRequest(int fromGroup, int toGroup)
        {
            return (true, "");
        }
        public async Task<int> FindByEmail(string mail)
        {
            throw new NotImplementedException();
        }

        public Task<List<(int groupId, IEnumerable<StudentProjectDTO> students)>> FindGroupForSwiping(int groupId, int projectId)
        {
            throw new NotImplementedException();
        }

        public Task<IEnumerable<StudentProjectDTO>> ViewGroup(int groupId, int projectId)
        {
            throw new NotImplementedException();
        }
    }
}
