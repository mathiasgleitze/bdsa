﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.GroupITApp.Models
{
    public class Settings : ISettings
    {
        public string ClientId => "865070f5-c3c3-4fa2-a7a4-b2afc3e1ec2f";

        public IReadOnlyCollection<string> Scopes => new[] { "865070f5-c3c3-4fa2-a7a4-b2afc3e1ec2f/user_impersonation" };

        public string TenantId => "bea229b6-7a08-4086-b44c-71f57f716bdb";
    }
}
