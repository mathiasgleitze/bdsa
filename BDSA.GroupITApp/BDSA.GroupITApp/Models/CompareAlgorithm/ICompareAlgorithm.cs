﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.CompareAlgorithm
{
    interface ICompareAlgorithm
    {
        Task<int> Compare(int groupOne, int groupTwo);
    }
}
