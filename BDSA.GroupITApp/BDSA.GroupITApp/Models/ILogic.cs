﻿using System.Collections.Generic;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.GroupITApp.Models
{
    public interface ILogic
    {
        Task<ProjectDTO> AddCourseName(int projectId);
        Task<(bool, string)> CreateMatch(int fromGroup, int toGroup);
        Task<(bool, string)> DeclineGroupRequest(int fromGroup, int toGroup);
        Task<List<(int groupId, IEnumerable<StudentProjectDTO> students)>> FindGroupForSwiping(int groupId,int projectId);
        Task<int> GetGroup(int studentId, int projectId);
        Task<int> GetGroupChat(int groupId);
        Task<List<MatchDTO>> GetMatches(int studentId);
        Task<List<MessageDTO>> GetMessages(int groupId);
        Task<(List<GroupRequestDTO>, List<MatchDTO>)> GetNotifications(int studentId);
        Task<StudentDTO> GetProfile(int studentId);
        Task<ProjectDTO> GetProject(int groupId);
        Task<ProjectProfileDTO> GetProjectProfile(int studentId, int projectId);
        Task<List<ProjectDTO>> GetProjects(int studentId);
        Task<StudentProjectDTO> GetStudentProject(int projectId, int studentId);
        Task<(bool, string)> MergeGroup(int GroupOneId, int GroupTwoId);
        Task<(bool, string)> RemoveGroupMember(int studentId, int groupId);
        Task<List<(int groupId, IEnumerable<StudentProjectDTO> students)>> Search(int projectId, int studentId, string input);
        Task<(bool, string)> SendGroupRequest(int fromGroupId, int toGroupId);
        Task<(bool, string)> SendMessage(int studentId, int chatId, string text);
        Task<bool> UpdateProfile(StudentDTO student);
        Task<bool> UpdateProjectProfile(ProjectProfileDTO projectProfile);
        Task<IEnumerable<StudentProjectDTO>> ViewGroup(int groupId);
        Task<(bool, string)> IsProjectProfileValid(int projectId, int studentId);
        Task<(bool, string)> CreateMatchRequest(int fromGroup, int toGroup);
        Task<int> FindByEmail(string mail);
    }
}