﻿using BDSA.GroupITApp.Models;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace BDSA.GroupITApp.ViewModels
{
    class SearchViewModel : BaseViewModel
    {
        private ILogic logic;
        private NavigationViewModel navigationViewModel;

        private ObservableCollection<(int groupId, IEnumerable<StudentProjectDTO> students)> searchResult { get; set; }
        public ObservableCollection<(int groupId, IEnumerable<StudentProjectDTO> students)> SearchResult { get { return searchResult; }
            set
            {
                searchResult = value;
                OnPropertyChanged();
            }
        }

        public SearchViewModel(ILogic logic, NavigationViewModel navigationViewModel)
        {
            this.logic = logic;
            this.navigationViewModel = navigationViewModel;
            navigationViewModel.PropertyChanged += NavigationChange;
        }

        private void NavigationChange(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;
            if (propertyName == "Group")
            {
                SearchResult = new ObservableCollection<(int groupId, IEnumerable<StudentProjectDTO> students)>();
            }
        }

    }
}
