﻿using BDSA.GroupITApp.Models;
using BDSA.GroupITApp.Models.Repositories;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;
using Xamarin.Forms;

namespace BDSA.GroupITApp.ViewModels
{
    class ProjectsViewModel : BaseViewModel
    {
        private ILogic logic;
        private NavigationViewModel navigationViewModel;
        private ObservableCollection<ProjectDTO> projects { get; set; }
        public ObservableCollection<ProjectDTO> Projects { get { return projects; }
            set {
                if (projects != value) { 
                    projects = value;
                    OnPropertyChanged();
                }
            }
        }
        
        public ProjectsViewModel(ILogic logic, NavigationViewModel navigationViewModel)
        {
            this.logic = logic;
            this.navigationViewModel = navigationViewModel;
            GetProjects();
        }

        public async void GetProjects()
        {
            Projects = new ObservableCollection<ProjectDTO>(await logic.GetProjects(navigationViewModel.Student));
        }
        
        public void OnSwiped(object sender, SwipedEventArgs e)
        {
            if (e.Direction == SwipeDirection.Down) GetProjects();
        }

    }
}
