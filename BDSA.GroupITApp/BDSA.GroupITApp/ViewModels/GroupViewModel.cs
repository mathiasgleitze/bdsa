﻿using BDSA.GroupITApp.Models;
using BDSA.Shared;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace BDSA.GroupITApp.ViewModels
{
    class GroupViewModel : BaseViewModel
    {
        private ILogic logic;
        private NavigationViewModel navigationViewModel;
        private ObservableCollection<StudentProjectDTO> viewGroup { get; set; }
        public int ViewGroupId { get; private set; }
        public ObservableCollection<StudentProjectDTO> ViewGroup { get { return viewGroup; }
            private set
            {
                viewGroup = new ObservableCollection<StudentProjectDTO>(value.Where(g => g.Id != navigationViewModel.Student));
                OnPropertyChanged();
            }
        }

        public GroupViewModel(ILogic logic, NavigationViewModel navigationViewModel)
        {
            this.logic = logic;
            this.navigationViewModel = navigationViewModel;
            navigationViewModel.PropertyChanged += NavigationChange;
        }

        private async void GetViewGroup()
        {
            ViewGroupId = navigationViewModel.ViewGroup;
            ViewGroup = new ObservableCollection<StudentProjectDTO>(await logic.ViewGroup(ViewGroupId));
        }

        public void NavigationChange(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;
            if (propertyName == "ViewGroup")
            {
                GetViewGroup();
            }
        }
    }
}
