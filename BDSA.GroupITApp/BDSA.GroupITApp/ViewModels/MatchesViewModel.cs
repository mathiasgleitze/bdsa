﻿using BDSA.GroupITApp.Models;
using BDSA.Shared;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace BDSA.GroupITApp.ViewModels
{
    class MatchesViewModel : BaseViewModel
    {
        private ILogic logic;
        private NavigationViewModel navigationViewModel;

        private ObservableCollection<(MatchDTO, ProjectDTO)> matches { get; set; }
        public ObservableCollection<(MatchDTO, ProjectDTO)> Matches { get { return matches; }
            set
            {
                matches = value;
                OnPropertyChanged();
            }
        }

        public MatchesViewModel(ILogic logic, NavigationViewModel navigationViewModel)
        {
            this.logic = logic;
            this.navigationViewModel = navigationViewModel;
            GetMatches();
        }

        public async void GetMatches()
        {
            List<MatchDTO> rawMatches = await logic.GetMatches(navigationViewModel.Student);
            List<(MatchDTO, ProjectDTO)> matches = new List<(MatchDTO, ProjectDTO)>();


            foreach(MatchDTO match in rawMatches)
            {
                ProjectDTO inProject = await logic.GetProject(match.GroupOne);
                int ownGroup = await logic.GetGroup(navigationViewModel.Student, inProject.Id);
                if (match.GroupOne == ownGroup)
                {
                    match.GroupOne = match.GroupTwo;
                    match.GroupTwo = ownGroup;
                }
                matches.Add((match, inProject));
            }

            Matches = new ObservableCollection<(MatchDTO, ProjectDTO)>(matches);
        }

        public void OnSwiped(object sender, SwipedEventArgs e)
        {
            if (e.Direction == SwipeDirection.Down) GetMatches();
        }
    }
}
