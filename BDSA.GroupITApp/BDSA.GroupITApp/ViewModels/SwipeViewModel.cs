﻿using BDSA.GroupITApp.Models;
using BDSA.Shared;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BDSA.GroupITApp.ViewModels
{
    class SwipeViewModel : BaseViewModel
    {
        private ILogic logic;
        private NavigationViewModel navigationViewModel;

        private ObservableCollection<StudentProjectDTO> currentGroup;
        private IEnumerator<(int groupId, IEnumerable<StudentProjectDTO> students)> it;
        private ObservableCollection<(int, IEnumerable<StudentProjectDTO>)> groups;
        private bool anyGroups = true;
        public int CurrentGroupId { get; private set; }
        private ObservableCollection<(int, IEnumerable<StudentProjectDTO>)> Groups
        {
            get { return groups; }
            set {
                groups = value;
                it = Groups.GetEnumerator();
                NextGroup();
            }
        }

        
        private bool canSwipe = true;
        public void OnSwiped(object sender, SwipedEventArgs e)
        {
            if (!anyGroups) return;
            if (canSwipe)
            {
                canSwipe = false;
                //Swipe gesture read 1 swipe as a number between 1-3, this makes it so that OnSwipe only does something every 0.5 sec.
                Task.Run(async delegate
                {
                    await Task.Delay(500);
                    canSwipe = true;
                });
                switch (e.Direction)
                {
                    case SwipeDirection.Left:
                        //Ikke gør noget
                        break;
                    case SwipeDirection.Right:
                        logic.CreateMatchRequest(navigationViewModel.Group, it.Current.groupId);
                        break;
                }
                NextGroup();     
            }
        }

        private void NextGroup()
        {
            it.MoveNext();
            if (it.Current.students == null) GetGroups();
            CurrentGroupId = it.Current.groupId;
            if (it.Current.students != null) CurrentGroup = new ObservableCollection<StudentProjectDTO>(it.Current.students);
        }

        public ObservableCollection<StudentProjectDTO> CurrentGroup
        {
            get { return currentGroup; }
            set
            {
                currentGroup = value;
                OnPropertyChanged();
            }
        }
        
        public SwipeViewModel(ILogic logic, NavigationViewModel navigationViewModel)
        {
            this.logic = logic;
            this.navigationViewModel = navigationViewModel;
            navigationViewModel.PropertyChanged += NavigationChange;
        }

        private async void GetGroups()
        {
            var groups = new ObservableCollection<(int, IEnumerable<StudentProjectDTO>)>(await logic.FindGroupForSwiping(navigationViewModel.Group,navigationViewModel.Project));
            if (!groups.Any())
            {
                Groups = new ObservableCollection<(int, IEnumerable<StudentProjectDTO>)>() { (0, new List<StudentProjectDTO>() { new StudentProjectDTO { AboutProject = "No More Groups" } }) };
                anyGroups = false;
            }
            else Groups = groups;
        }
      
        private void NavigationChange(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;
            if (propertyName == "Group")
            {
                anyGroups = true;
                GetGroups();
            }
        }
    }
}
