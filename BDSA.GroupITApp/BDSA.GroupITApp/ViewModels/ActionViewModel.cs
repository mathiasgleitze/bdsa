﻿using BDSA.GroupITApp.Models;
using BDSA.Shared;
using Microsoft.Identity.Client;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace BDSA.GroupITApp.ViewModels
{
	class ActionViewModel : BaseViewModel
	{
        private Dictionary<string, bool> isEnabledDictionary;
        private Dictionary<string, StudentProjectDTO> studentProjectDictionary;
        private List<string> profileProperties, projectProfileProperties;
        private ILogic logic;
        private NavigationViewModel navigationViewModel;
        private readonly IPublicClientApplication publicClientApplication;
        private readonly ISettings settings;
        public string Username { get; set; }
        public bool LoggedIn { get; set; }

        public string CurrentDataId { get; set; }

        public ActionViewModel(ILogic logic, NavigationViewModel navigationViewModel, IPublicClientApplication publicClientApplication, ISettings settings)
        {
            this.logic = logic;
            this.navigationViewModel = navigationViewModel;
            this.publicClientApplication = publicClientApplication;
            this.settings = settings;

            navigationViewModel.PropertyChanged += AutoSetValues;
            isEnabledDictionary = new Dictionary<string, bool>();

            studentProjectDictionary = new Dictionary<string, StudentProjectDTO>();

            profileProperties = new List<string>() { "studentName", "studentMail", "studentCity","studentAbout"};
            projectProfileProperties = new List<string>() { "studentAmbitions", "studentSkillLevel", "studentFlexibility", "studentMeetingDisciplin", "studentAboutProject" };

            CurrentDataId = "";
        }

        public async Task DispatchAction(string actionId, string dataId) {
            switch (actionId) {
                case "edit":
                    isEnabledDictionary[dataId] = !isEnabledDictionary[dataId];
                    break;
                case "sendChatMessage":
                    break;
                case "acceptGroupRequest":
                    var groupsAccept = dataId.Split('+');
                    await logic.MergeGroup(Int32.Parse(groupsAccept[0]), Int32.Parse(groupsAccept[1]));
                    DependencyService.Resolve<NotificationsViewModel>().GetNotifications();
                    break;
                case "declineGroupRequest":
                    var groupsDecline = dataId.Split('+');
                    await logic.DeclineGroupRequest(Int32.Parse(groupsDecline[0]), Int32.Parse(groupsDecline[1]));
                    DependencyService.Resolve<NotificationsViewModel>().GetNotifications();
                    break;
                case "sendGroupRequestSearch":
                     await logic.SendGroupRequest(navigationViewModel.Group, Int32.Parse(dataId));
                     break;
                case "sendGroupRequestMatch":
                    string[] result = dataId.Split('+');
                    await logic.SendGroupRequest(Int32.Parse(result[2]), Int32.Parse(result[0]));
                    break;
                case "searchGroup":
                    break;
                case "logIn":
                    await LogInAsync();
                    break;
                default:
                    break;
            }
            CurrentDataId = dataId;
            OnPropertyChanged(actionId);
        }



        public void SetIsEnabled(string dataId, bool isEnabled)
        {
            if (!isEnabledDictionary.ContainsKey(dataId)) isEnabledDictionary.Add(dataId, isEnabled);
        }

        public bool IsEnabled(string dataId) {
            return isEnabledDictionary[dataId];
        }

        public void AutoSetValues(object sender, EventArgs e) {
            System.ComponentModel.PropertyChangedEventArgs propertyChangedEventArgs = (System.ComponentModel.PropertyChangedEventArgs)e;
            string actionId = propertyChangedEventArgs.PropertyName;

            if (actionId == "SetPage") {
            foreach (string dataId in isEnabledDictionary.Keys.ToList())
            {
                isEnabledDictionary[dataId] = false;
                    CurrentDataId = dataId;
                 OnPropertyChanged("edit");

                }
            }
        }

        public void SetValues(string id, string dataId, Dictionary<string,string> newValues) {
            switch (id) {
                case "projectProfile":
                    ProjectProfileDTO projectProfile = new ProjectProfileDTO()
                    {
                        StudentId = Int32.Parse(dataId),
                        ProjectId = navigationViewModel.Project,
                        About = newValues["studentAboutProject"],
                        Ambitions = Int32.Parse(newValues["studentAmbitions"] == "" ? "0" : newValues["studentAmbitions"]),
                        MeetingDisciplin = Int32.Parse(newValues["studentMeetingDisciplin"] == "" ? "0" : newValues["studentMeetingDisciplin"]),
                        SkillLevel = Int32.Parse(newValues["studentSkillLevel"] == "" ? "0" : newValues["studentSkillLevel"]),
                        Flexibility = Int32.Parse(newValues["studentFlexibility"] == "" ? "0" : newValues["studentFlexibility"])
                    };
                    logic.UpdateProjectProfile(projectProfile);
                    break;
                case "personalProfile":
                    StudentDTO profile = new StudentDTO()
                    {
                        Id = Int32.Parse(dataId),
                        Address = newValues["studentCity"],
                        About = newValues["studentAbout"],
                    };
                    logic.UpdateProfile(profile);
                    break;
                default:
                    break;
            }
        }
        

        public void SendChatMessage(string dataId, string text)
        {
            logic.SendMessage(navigationViewModel.Student, navigationViewModel.Chat, text);
            DependencyService.Resolve<ChatViewModel>().GetChatMessages();
        }

        public async void SearchGroup(string dataId, string text)
        {
            List<(int groupId, IEnumerable<StudentProjectDTO> students)> result = null;
            if(await logic.Search(navigationViewModel.Project, navigationViewModel.Student, text) == null)
            {
                result = new List<(int groupId, IEnumerable<StudentProjectDTO> students)>();
            }
            else result = await logic.Search(navigationViewModel.Project, navigationViewModel.Student, text);
            var asObservable = new ObservableCollection<(int groupId, IEnumerable<Shared.StudentProjectDTO> students)>(result);
            DependencyService.Resolve<SearchViewModel>().SearchResult = asObservable;
        }


        private async Task LogInAsync()
        {
            AuthenticationResult authenticationResult;
            try
            {
                authenticationResult = await publicClientApplication.AcquireTokenAsync(settings.Scopes);
            }
            catch (MsalClientException e)
            {
                LoggedIn = false;
                return;
            }

            if (authenticationResult != null)
            {
                Username = authenticationResult.Account.Username;
                //So that everyone can log in, and try the system
                int id = await logic.FindByEmail(Username);
                if (id == 0) id = 1;
                navigationViewModel.Student = id;
            }
            else
            {
                LoggedIn = false;
            }

            LoggedIn = true;
        }
    }
}