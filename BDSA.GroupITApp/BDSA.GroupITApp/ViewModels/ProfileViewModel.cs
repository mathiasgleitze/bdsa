﻿using BDSA.GroupITApp.Models;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Text;

namespace BDSA.GroupITApp.ViewModels
{
    class ProfileViewModel : BaseViewModel
    {

        ILogic logic;
        NavigationViewModel navigationViewModel;
        
        private StudentDTO studentProfile { get; set; }
        public StudentDTO StudentProfile
        {
            get { return studentProfile; }
            set
            {
                studentProfile = value;
                OnPropertyChanged();
            }
        }
        public ProfileViewModel(ILogic logic, NavigationViewModel navigationViewModel)
        {
            this.logic = logic;
            this.navigationViewModel = navigationViewModel;
            navigationViewModel.PropertyChanged += NavigationChange;
            GetProfile();
        }

        private async void GetProfile()
        {
            StudentProfile = await logic.GetProfile(navigationViewModel.Student);
        }

        public void NavigationChange(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;
            if (propertyName == "Student")
            {
                GetProfile();
            }
        }
    }
}
