﻿using BDSA.GroupITApp.Models;
using BDSA.Shared;
using System;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace BDSA.GroupITApp.ViewModels
{
    class ChatViewModel : BaseViewModel
    {
        private ILogic logic;
        private NavigationViewModel navigationViewModel;
        private ObservableCollection<MessageDTO> messages { get; set; }
        public int ChatId { get; private set; }
        public ObservableCollection<MessageDTO> Messages { get { return messages; }
            set
            {
                messages = value;
                OnPropertyChanged();
            }
        }

        public ChatViewModel(ILogic logic, NavigationViewModel navigationViewModel)
        {
            this.logic = logic;
            this.navigationViewModel = navigationViewModel;
            navigationViewModel.PropertyChanged += NavigationChange;
            Messages = new ObservableCollection<MessageDTO>();
            GetChatMessages();
        }

        public async void GetChatMessages()
        {
            ChatId = navigationViewModel.Chat;
            
            var messageDTOs = await logic.GetMessages(ChatId);

            Messages = new ObservableCollection<MessageDTO>(messageDTOs);
            
        }

        public void NavigationChange(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;
            if (propertyName == "Chat")
            {
                GetChatMessages();
            }
        }
        public void OnSwiped(object sender, SwipedEventArgs e)
        {
            if (e.Direction == SwipeDirection.Up) GetChatMessages();
        }
    }
}
