﻿using BDSA.GroupITApp.Models;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Linq;
using System.Diagnostics;

namespace BDSA.GroupITApp.ViewModels
{
    class ProjectProfileViewModel : BaseViewModel
    {
        ILogic logic;
        NavigationViewModel navigationViewModel;

        private ObservableCollection<StudentProjectDTO> group { get; set; }
        public ObservableCollection<StudentProjectDTO> Group { get { return group; }
            set
            {
                group = new ObservableCollection<StudentProjectDTO>(value.Where(s => s.Id != studentProfile.StudentId));
                OnPropertyChanged();
            }
        }

        private ProjectProfileDTO studentProfile { get; set; }
        public ProjectProfileDTO StudentProfile
        {
            get { return studentProfile; }
            set
            {
                studentProfile = value;
                OnPropertyChanged();
            }
        }
        public ProjectProfileViewModel(ILogic logic, NavigationViewModel navigationViewModel)
        {
            this.logic = logic;
            this.navigationViewModel = navigationViewModel;
            navigationViewModel.PropertyChanged += NavigationChange;
        }

        private async void GetData()
        {
            StudentProfile = await logic.GetProjectProfile(navigationViewModel.Student, navigationViewModel.Project);
            Group = new ObservableCollection<StudentProjectDTO>(await logic.ViewGroup(navigationViewModel.Group));
        }

        private void NavigationChange(object sender, EventArgs e)
        {
            string propertyName = ((System.ComponentModel.PropertyChangedEventArgs)e).PropertyName;
            if (propertyName == "Group")
            {
                GetData();
            }
        }
    }
}
