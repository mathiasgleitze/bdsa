﻿using BDSA.GroupITApp.Models;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace BDSA.GroupITApp.ViewModels
{
    class NotificationsViewModel : BaseViewModel
    {
        private ILogic logic;
        private NavigationViewModel navigationViewModel;

        private (List<GroupRequestDTO>, List<MatchDTO>) notifications { get; set; }
        public (List<GroupRequestDTO>, List<MatchDTO>) Notifications { get { return notifications; }
            set
            {
                notifications = value;
                OnPropertyChanged();
            }
        }

        public NotificationsViewModel(ILogic logic, NavigationViewModel navigationViewModel)
        {
            this.logic = logic;
            this.navigationViewModel = navigationViewModel;
            GetNotifications();
        }

        public async void GetNotifications()
        {
            var rawNotifications = await logic.GetNotifications(navigationViewModel.Student);

            foreach (MatchDTO match in rawNotifications.Item2)
            {
                ProjectDTO inProject = await logic.GetProject(match.GroupOne);
                int ownGroup = await logic.GetGroup(navigationViewModel.Student, inProject.Id);
                if (match.GroupOne == ownGroup)
                {
                    match.GroupOne = match.GroupTwo;
                    match.GroupTwo = ownGroup;
                }
            }
            Notifications = rawNotifications;
        }

        public void OnSwiped(object sender, SwipedEventArgs e)
        {
            if (e.Direction == SwipeDirection.Down) GetNotifications();
        }
    }
}
