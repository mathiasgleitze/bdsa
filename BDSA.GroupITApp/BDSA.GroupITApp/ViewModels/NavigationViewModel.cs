﻿using BDSA.GroupITApp.Views.Page;
using BDSA.GroupITApp.Views.Pages;
using System.Collections.Generic;
using System.Linq;
using BDSA.GroupITApp.Models;
using System;

namespace BDSA.GroupITApp.ViewModels
{
    class NavigationViewModel : BaseViewModel
    {
        private ILogic logic;


        public void CreatePages()
        {
            //Projects Pages
            pages.Add("projects", new ProjectsPage());
            pages.Add("search", new SearchPage());
            pages.Add("swipe", new SwipePage());
            pages.Add("projectProfile", new ProjectProfilePage());

            //Matches Pages
            pages.Add("matches", new MatchesPage());

            //Matches and Projects pages
            pages.Add("chat", new ChatPage());
            pages.Add("groupProfile", new GroupPage());

            //Notification Page
            pages.Add("notifications", new NotificationsPage());

            //Profile Page
            pages.Add("profile", new ProfilePage());
        }


        public int Student { get; set; }
        private int project = 0;
        private int group = 0;
        private int chat = 0;
        private int viewGroup = 0;

        private async void UpdateGroupAndChat()
        {
            int newGroup = await logic.GetGroup(Student, Project);
            Group = newGroup;
            int newProjectChat = await logic.GetGroupChat(Group);
            ProjectChat = newProjectChat;
        }
        
        public int Project
        {
            get { return project; }
            set
            {
                if (project != value) { 
                    project = value;
                    UpdateGroupAndChat();
                    OnPropertyChanged();
                }
            }
        }
        public int Group
        {
            get { return group; }
            set
            {
                group = value;
                ViewGroup = group;
                OnPropertyChanged();
            }
        }
        public int Chat
        {
            get { return chat; }
            set
            {
                chat = value;
                OnPropertyChanged();
            }
        }

        private int projectChat = 0;
        private int ProjectChat { get { return projectChat; }
            set
            {
                projectChat = value;
                Chat = projectChat;
            }
        }
        private int matchChat = 0;
        private int MatchChat { get { return matchChat; }
            set
            {
                matchChat = value;
                Chat = matchChat;
            }
        }
        private int otherGroup = 0;
        private int OtherGroup { get { return otherGroup; }
            set
            {
                otherGroup = value;
                ViewGroup = otherGroup;
            }
        }

        public int ViewGroup { get { return viewGroup; }
            set
            {
                viewGroup = value;
                OnPropertyChanged();
            }
        }


        private MainPage navigationPage;

        private Stack<string> projectsStack = new Stack<string>();
        private Stack<string> matchesStack = new Stack<string>();
        
        
        private Dictionary<string, CustomPage> pages = new Dictionary<string, CustomPage>();
        private string currentBranch = "projects"; //TODO der hvor man starter

        public async void ChangePage(string pageId, string dataId)
        {

            OnPropertyChanged("SetPage");
            switch (pageId)
            {
                case "back":
                    if (currentBranch == "projects")
                    {
                        projectsStack.Pop();
                        SetPage(projectsStack.Peek());
                    } else
                    {
                        matchesStack.Pop();
                        SetPage(matchesStack.Peek());
                    }
                    break;
                case "projects":
                    if (!projectsStack.Any()) SetPage(pageId);
                    else SetPage(projectsStack.Peek());
                    currentBranch = "projects";
                    if (ProjectChat != 0) Chat = ProjectChat;
                    if (group != 0) ViewGroup = group;
                    break;
                case "matches":
                    if (!matchesStack.Any()) SetPage(pageId);
                    else SetPage(matchesStack.Peek());
                    currentBranch = "matches";
                    if (MatchChat != 0) Chat = MatchChat;
                    if (OtherGroup != 0) ViewGroup = otherGroup;
                    break;
                case "notifications":
                case "profile":
                    SetPage(pageId);
                    break;
                default:
                    if (currentBranch == "projects")
                    {
                        if (dataId != null && dataId.Split('+').Count() == 1) Project = Int32.Parse(dataId);
                        (bool, string) isValid = await logic.IsProjectProfileValid(Project, Student);
                        if (!isValid.Item1) pageId = "projectProfile";
                        projectsStack.Push(pageId);
                    }
                    else if (currentBranch == "matches")
                    {
                        matchesStack.Push(pageId);
                        string[] ids = dataId.Split('+');
                        MatchChat = Int32.Parse(ids[1]);
                        OtherGroup = Int32.Parse(ids[0]);
                    }

                    SetPage(pageId);
                    break;
            }

        }

        public NavigationViewModel(ILogic logic)
        {
            this.logic = logic;
            navigationPage = new MainPage();
            
            projectsStack.Push("projects");
            matchesStack.Push("matches");
        }


        private void SetPage(string id)
        {
            CustomPage page;
            pages.TryGetValue(id, out page);
            navigationPage.SetPage(page);
        }

        public MainPage GetMainPage()
        {
            navigationPage = new MainPage();
            SetPage("projects");
            return navigationPage;
        }
    }
}
