﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using BDSA.Shared;
using BDSA.GroupITApp.Models;

namespace BDSA.Logic
{
    public class SimpleCompareAlgoritm : ICompareAlgorithm
    {

        private HttpClient _client;
        private IGroupRepository _groupRepository;
        private IProjectRepository _projectRepository;
        private IStudentGroupRepository _studentGroupRepository;
        private IProjectProfileRepository _projectProfileRepository;

        public SimpleCompareAlgoritm(HttpClient client)
        {
            _client = client;
            _groupRepository = new GroupRepository(_client);
            _projectRepository = new ProjectRepository(_client);
            _studentGroupRepository = new StudentGroupRepository(_client);
            _projectProfileRepository = new ProjectProfileRepository(_client);
        }
        public async Task<int> Compare(int groupOneId, int groupTwoId)
        {
            var studentsOne = await _studentGroupRepository.ReadStudents(groupOneId);
            var studentsTwo = await _studentGroupRepository.ReadStudents(groupTwoId);
            var group = await _groupRepository.FindAsync(groupOneId);
            var project = await _projectRepository.FindAsync(group.ProjectId);
            var nrOfGroupMembersOne = studentsOne.Count();
            var nrOfGroupMembersTwo = studentsTwo.Count();
            if (nrOfGroupMembersOne + nrOfGroupMembersTwo > project.MaxSize)
            {
                return -1;
            }
            var scoreOne = new int[4];
            foreach (StudentDTO s in studentsOne)
            {
                var profile = await _projectProfileRepository.FindAsync(s.Id, project.Id);
                scoreOne[0] += profile.Ambitions;
                scoreOne[1] += profile.Flexibility;
                scoreOne[2] += profile.MeetingDisciplin;
                scoreOne[3] += profile.SkillLevel;
            }
            var finalScoreOne = 0;
            for (int j = 0; j < scoreOne.Count(); j++)
            {
                finalScoreOne += (scoreOne[j] / nrOfGroupMembersOne);
            }


            var scoreTwo = new int[4];
            foreach (StudentDTO s in studentsTwo)
            {
                var profile = await _projectProfileRepository.FindAsync(s.Id, project.Id);
                scoreTwo[0] += profile.Ambitions;
                scoreTwo[1] += profile.Flexibility;
                scoreTwo[2] += profile.MeetingDisciplin;
                scoreTwo[3] += profile.SkillLevel;
            }
            var finalScoreTwo = 0;
            for (int j = 0; j < scoreTwo.Count(); j++)
            {
                finalScoreTwo += (scoreTwo[j] / nrOfGroupMembersTwo);
            }
            return Math.Abs(finalScoreTwo - finalScoreOne);

        }
    }
}
