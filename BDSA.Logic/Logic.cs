﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using BDSA.GroupITApp.Models;

namespace BDSA.Logic
{
    public class Logic
    {
        private HttpClient _client;
        private IChatRepository _chatRepository;
        private IMatchRepository _matchRepository;
        private IMatchRequestRepository _matchRequestRepository;
        private IGroupRepository _groupRepository;
        private IGroupRequestRepository _groupRequestRepository;
        private IStudentCourseRepository _studentCourseRepository;
        private IStudentGroupRepository _studentGroupRepository;
        private IProjectRepository _projectRepository;
        private IProjectProfileRepository _projectProfileRepository;
        private IMessageRepository _messageRepository;
        private IStudentRepository _studentRepository;
        private ICompareAlgorithm _compareAlgorithm;

        public Logic(HttpClient client)
        {
            _client = client;
            _chatRepository = new ChatRepository(_client);
            _messageRepository = new MessageRepository(_client);
            _matchRepository = new MatchRepository(_client);
            _matchRequestRepository = new MatchRequestRepository(_client);
            _groupRepository = new GroupRepository(_client);
            _groupRequestRepository = new GroupRequestRepository(_client);
            _studentCourseRepository = new StudentCourseRepository(_client);
            _studentGroupRepository = new StudentGroupRepository(_client);
            _projectRepository = new ProjectRepository(_client);
            _projectProfileRepository = new ProjectProfileRepository(_client);
            _studentRepository = new StudentRepository(_client);
            _compareAlgorithm = new SimpleCompareAlgoritm(_client);
        }

        public async Task <IEnumerable<StudentProjectDTO>>ViewGroup(int groupId)
        {
            var students = await _studentGroupRepository.ReadStudents(groupId);
            var studentProjectDtos = new List<StudentProjectDTO>();
            var group = await _groupRepository.FindAsync(groupId);
            foreach(StudentDTO s in students)
            {
                var pProfile = await _projectProfileRepository.FindAsync(s.Id, group.ProjectId);
                var sProfile = new StudentProjectDTO()
                {
                    Id = s.Id,
                    Name = s.Name,
                    Mail = s.Mail,
                    Address = s.Address,
                    AboutStudent = s.About,
                    AboutProject = pProfile.About,
                    Ambitions = pProfile.Ambitions,
                    MeetingDisciplin = pProfile.MeetingDisciplin,
                    Flexibility = pProfile.Flexibility,
                    SkillLevel = pProfile.SkillLevel
                };
                studentProjectDtos.Add(sProfile);
            }
            return studentProjectDtos;
        }
        public async Task<List<(int groupId,IEnumerable<StudentProjectDTO> students)>>FindGroupForSwiping(int groupId)
        { 
            var bestGroups = new List<(int, int, IEnumerable<StudentProjectDTO>)>();
            var group = await _groupRepository.FindAsync(groupId);
            var project = await _projectRepository.FindAsync(group.ProjectId);


            var allgroups = await _groupRepository.ReadAsync();
            var groupInproject= from g in allgroups
                                where g.ProjectId==project.Id&&g.Id!=groupId
                                select  g.Id;
            
            foreach (int i in groupInproject)
            {
                var viewGroup = await ViewGroup(i);
                var score = await  _compareAlgorithm.Compare(groupId, i);

                if (score != -1)
                {
                    bestGroups.Add((score, i, viewGroup));
                }
                
            }
            var OrderedBestGroup = from b in bestGroups
                                 orderby b.Item1
                                 select (b.Item2, b.Item3);

            
            return OrderedBestGroup.ToList();
        }
        public async Task<(bool,string)> MergeGroup(int GroupOneId,int GroupTwoId)
        {


            await _matchRequestRepository.DeleteAllAsync(GroupTwoId);

            var match = await _matchRepository.FindAsync(GroupOneId, GroupTwoId);
            if (match == null)
            {
                return (false, "Match not found");
            }

            var deleteGroupRequest = await _groupRequestRepository.DeleteAsync(GroupOneId, GroupTwoId);
            if (!deleteGroupRequest)
            {
                return (false, "GroupReqeuest not found");
            }

            var deleteMessagesMatch = await _messageRepository.DeleteMessagesAsync(match.ChatId);
            if (!deleteMessagesMatch)
            {
                return (false, "Messages not found");
            }

            var deleteMatchChat = await _chatRepository.DeleteAsync(match.ChatId);
            if (!deleteMatchChat)
            {
                return (false, "Chat not found");
            }
            await _matchRepository.UpdateAsync(GroupTwoId, GroupOneId);
            await _groupRequestRepository.UpdateAsync(GroupTwoId, GroupOneId);

            var studentsDTO = await _studentGroupRepository.ReadStudents(GroupTwoId);
            foreach(StudentDTO s in studentsDTO)
            {
                var delete= await _studentGroupRepository.DeleteAsync(s.Id, GroupTwoId);
                if (!delete)
                {
                    return (false, "Student not found");
                }
                await _studentGroupRepository.CreateAsync(new StudentGroupDTO { StudentId = s.Id, GroupId = GroupOneId });
            }

            var group = await _groupRepository.FindAsync(GroupTwoId);

            var deleteMessagesGroup = await _messageRepository.DeleteMessagesAsync(group.ChatId);
            if (!deleteMessagesGroup)
            {
                return (false, "Messages not found");
            }
            var deleteGroup = await _groupRepository.DeleteAsync(GroupTwoId);
            if (!deleteGroup)
            {
                return (false, "Group not found");
            }
            var deleteChatGroup = await _chatRepository.DeleteAsync(group.ChatId);
            if (!deleteChatGroup)
            {
                return (false, "Chat not found");
            }
            return (true, "");

        }
        public async Task<(bool, string)> CreateMatch(int fromGroup, int toGroup)
        {
            var deletedMatchRequest = await _matchRequestRepository.DeleteAsync(fromGroup, toGroup);
            if (!deletedMatchRequest)
            {
                return (false, "Match request not deleted");
            }

            var chatDTO = new ChatDTO();
            var chat = await _chatRepository.CreateAsync(chatDTO);
            if (chat == null)
            {
                return (false, "Chat not created");
            }

            var matchDTO = new MatchDTO
            {
                ChatId = chatDTO.Id,
                GroupOne = fromGroup,
                GroupTwo = toGroup
            };
            var match = await _matchRepository.CreateAsync(matchDTO);
            if (match == null)
            {
                return (false, "Match not created");
            }

            return (true, "");
        }
        public async Task<(bool, string)> RemoveGroupMember(int studentId, int groupId)
        {
            var studentGroupRepository = new StudentGroupRepository(_client);
            var groupRepository = new GroupRepository(_client);
            var chatRepository = new ChatRepository(_client);
            
            var deleted = await studentGroupRepository.DeleteAsync(studentId, groupId);
            if (!deleted)
            {
                return (false, "Old student group connection not deleted");
            }

            var chatDTO = new ChatDTO();
            var chat = await chatRepository.CreateAsync(chatDTO);
            if (chat == null)
            {
                return (false, "Chat not created");
            }

            var oldGroup = await groupRepository.FindAsync(groupId);
            if (oldGroup == null)
            {
                return (false, $"Old group with ID {groupId} not found");
            }

            var newGroupDTO = new GroupDTO
            {
                ChatId = chat.Id,
                ProjectId = oldGroup.ProjectId
            };

            var newGroup = await groupRepository.CreateAsync(newGroupDTO);
            if (newGroup == null)
            {
                return (false, "New group not created");
            }

            var studentGroupDTO = new StudentGroupDTO
            {
                GroupId = newGroup.Id,
                StudentId = studentId
            };

            var studentGroup = await studentGroupRepository.CreateAsync(studentGroupDTO);
            if (studentGroup == null)
            {
                return (false, "New student group connection not created");
            }

            return (true, "");
        }
        public async Task<List<(int groupId, IEnumerable<StudentProjectDTO> students)>> Search(int projectId, int studentId, string input)
        {
            var results = new List<(int, IEnumerable<StudentProjectDTO>)>();


            var project = await _projectRepository.FindAsync(projectId);
            if (project == null)
            {
                return results;
            }
            
            var studentsInCourse = (await _studentCourseRepository.Read(project.CourseId)).students;
            if (!studentsInCourse.Any())
            {
                return results;
            }

            var matchingStudents = await _studentRepository.FindStudentsByNameAsync(input);
            if (!matchingStudents.Any())
            {
                return results;
            }
            
            foreach (var student in matchingStudents)
            {
                if (student.Id != studentId) //Not the student searching
                {
                    if (studentsInCourse.Contains(student))
                    {
                        var groups = await _studentGroupRepository.ReadGroups(student.Id);

                        foreach (var group in groups)
                        {
                            if (group.ProjectId == projectId)
                            {
                                results.Add((group.Id, await ViewGroup(group.Id)));
                            }
                        }
                    }
                }
            }
            return results;
        }
        public async Task<(bool,string)> SendMessage(int studentId,int chatId,string text)
        {
            var message = new MessageDTO
            {
                StudentId = studentId,
                ChatId = chatId,
                Text = text,
                Time = new DateTime()
                
            };
            var result = await _messageRepository.CreateAsync(message);
            if (result == null)
            {
                return (false, "message not created");
            }

            return (true, "");
        }

        public async Task<List<ProjectDTO>> GetProjects(int studentId)
        {
            var groups = await _studentGroupRepository.ReadGroups(studentId);

            var projectIds = new List<int>();
            foreach (var group in groups)
            {
                if (!projectIds.Contains(group.ProjectId))
                {
                    projectIds.Add(group.ProjectId);
                }
            }

            var projectDTOs = new List<ProjectDTO>();
            foreach (var id in projectIds)
            {
                projectDTOs.Add(await _projectRepository.FindAsync(id));
            }

            return projectDTOs;
        }

        public async Task<StudentDTO> GetProfile(int studentId)
        {
            return await _studentRepository.FindAsync(studentId);
        }

        public async Task<ProjectProfileDTO> GetProjectProfile(int studentId, int projectId)
        {
            return await _projectProfileRepository.FindAsync(studentId, projectId);
        }

        public async Task<bool> UpdateProfile(StudentDTO student)
        {
            return await _studentRepository.UpdateAsync(student);
        }

        public async Task<bool> UpdateProjectProfile(ProjectProfileDTO projectProfile)
        {
            return await _projectProfileRepository.UpdateAsync(projectProfile);
        }

        public async Task<List<MatchDTO>> GetMatches(int studentId)
        {
            var groups = await _studentGroupRepository.ReadGroups(studentId);

            var matches = new List<MatchDTO>();
            foreach (var group in groups)
            {
                matches.AddRange(await _matchRepository.FindAllMatches(group.Id));
            }

            return matches;
        }

        public async Task<List<MessageDTO>> GetMessages(int groupId)
        {
            var group = await _groupRepository.FindAsync(groupId);
            return (await _messageRepository.ReadAsync(group.ChatId)).ToList();
        }

        public async Task<(List<GroupRequestDTO>, List<MatchDTO>)> GetNotifications(int studentId)
        {
            var groups = await _studentGroupRepository.ReadGroups(studentId);

            var finalMatches = new List<MatchDTO>();
            var requests = new List<GroupRequestDTO>();
            foreach (var group in groups)
            {
                requests = (await _groupRequestRepository.ReadTo(group.Id)).ToList();

                var matches = await _matchRepository.FindAllMatches(group.Id);
                foreach (var match in matches)
                {
                    var messages = await _messageRepository.ReadAsync(match.ChatId);
                    if (!messages.Any())
                    {
                        finalMatches.Add(match);
                    }
                }
            }

            return (requests, finalMatches);
        }
    }
}
