﻿using System;
using System.Linq;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.Models
{
    public interface IMessageRepository
    {
        Task<MessageDTO> CreateAsync(MessageDTO message);
        Task<MessageDTO> FindAsync(int chatId, DateTime time);
        Task<bool> DeleteAsync(int chatId, DateTime time);
        Task<bool> DeleteMessagesAsync(int chatId);
        IQueryable<MessageDTO> Read(int chatId);
    }
}