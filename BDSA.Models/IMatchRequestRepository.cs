﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDSA.Models
{
    public interface IMatchRequestRepository
    {
        Task<MatchRequestDTO> CreateAsync(MatchRequestDTO request);
        Task<bool> DeleteAsync(int fromGroup, int toGroup);
        Task<bool> DeleteAll(int group);
        Task<MatchRequestDTO> FindAsync(int fromGroup, int toGroup);
        IQueryable<MatchRequestDTO> ReadTo(int group);
        IQueryable<MatchRequestDTO> ReadFrom(int group);
    }
}
