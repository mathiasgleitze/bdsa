﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDSA.Entities;
using BDSA.Shared;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Models
{
    public class StudentCourseRepository : IStudentCourseRepository
    {
        private readonly IGroupItContext _context;

        public StudentCourseRepository(IGroupItContext context)
        {
            _context = context;
        }

        public async Task<StudentCourseDTO> CreateAsync(StudentCourseDTO enrolled)
        {
            var entity = new StudentCourse
            {
                CourseId = enrolled.CourseId,
                StudentId = enrolled.StudentId
            };

            _context.StudentCourses.Add(entity);
            await _context.SaveChangesAsync();

            var studentCourse = await _context.StudentCourses.FindAsync(entity.StudentId, entity.CourseId);
            if (studentCourse == null)
            {
                return null;
            }

            return new StudentCourseDTO
            {
                CourseId = studentCourse.CourseId,
                StudentId = studentCourse.StudentId
            };
        }

        public async Task<bool> DeleteAsync(int studentId, int courseId)
        {
            var entity = await _context.StudentCourses.FindAsync(studentId, courseId);

            if (entity == null)
            {
                return false;
            }

            _context.StudentCourses.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }
        public async Task<StudentCourseDTO>FindAsync(int studentId,int courseId)
        {
            var studentcourses = from c in _context.StudentCourses
                        where c.StudentId == studentId&& c.CourseId==courseId
                        select new StudentCourseDTO
                        {
                            StudentId = c.StudentId,
                            CourseId = c.CourseId
                        };

            return await studentcourses.FirstOrDefaultAsync();
        }
        public IQueryable<StudentCourseDTO> Read()
        {
            return from sc in _context.StudentCourses
                   select new StudentCourseDTO
                   {
                       StudentId = sc.StudentId,
                       CourseId = sc.CourseId
                   };
        }

        public IQueryable<CourseDTO> FindCourses(int studentId)
        {
            return from enrolled in _context.StudentCourses
                   where enrolled.StudentId == studentId
                   select new CourseDTO
                   {
                       Id = enrolled.CourseId,
                       Name = _context.Courses
                              .Where(course => course.Id == enrolled.CourseId)
                              .Select(course => course.Name)
                              .FirstOrDefault()
                   };
        }

        public IQueryable<StudentDTO> FindStudents(int courseId)
        {
            var studentIds = from enrolled in _context.StudentCourses
                             where enrolled.CourseId == courseId
                             select enrolled.StudentId;

            return from student in _context.Students
                   where studentIds.Contains(student.Id)
                   select new StudentDTO
                   {
                       Id = student.Id,
                       Name = student.Name,
                       Mail = student.Mail,
                       Address = student.Address,
                       About = student.About
                   };
        }
    }
}