﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BDSA.Models
{
    public interface IProjectRepository
    {
        Task<ProjectDTO> CreateAsync(ProjectDTO project);
        Task<bool> DeleteAsync(int id);
        Task<ProjectDTO> FindAsync(int id);
        Task<bool> UpdateAsync(ProjectDTO project);
    }
}
