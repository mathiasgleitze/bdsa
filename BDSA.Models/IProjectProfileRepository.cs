﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDSA.Models
{
    public interface IProjectProfileRepository
    {
        Task<ProjectProfileDTO> CreateAsync(ProjectProfileDTO projectProfile);
        Task<bool> DeleteAsync(int studentId , int projectId);
        Task<ProjectProfileDTO> FindAsync(int studentId , int projectId);
        Task<bool> UpdateAsync(ProjectProfileDTO course);
        IQueryable<ProjectProfileDTO> FindProjectProfilesAsync(int studentId);
    }
}
