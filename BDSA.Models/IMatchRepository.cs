﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDSA.Models
{
    public interface IMatchRepository
    {
        Task<MatchDTO> CreateAsync(MatchDTO match);
        Task<bool> DeleteAsync(int groupOne, int groupTwo);
        Task<bool> UpdateAsync(int oldgroup, int newgroup);
        Task<MatchDTO> FindAsync(int groupOne, int groupTwo);
        IQueryable<MatchDTO> FindAllMatches(int id);
    }
}
