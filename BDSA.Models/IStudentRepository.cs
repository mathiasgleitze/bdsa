﻿using System.Linq;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.Models
{
    public interface IStudentRepository
    {
        Task<StudentDTO> CreateAsync(StudentDTO student);
        Task<bool> DeleteAsync(int id);
        Task<StudentDTO> FindAsync(int id);
        IQueryable<StudentDTO> FindStudentsByName(string name);
        Task<int> FindIdByMailAsync(string mail);
        Task<bool> UpdateAsync(StudentDTO student);
    }
}