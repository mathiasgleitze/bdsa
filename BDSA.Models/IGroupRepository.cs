﻿using System.Linq;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.Models
{
    public interface IGroupRepository
    {
        Task<bool> CreateAsync(GroupDTO group);
        Task<bool> DeleteAsync(int groupId);
        Task<GroupDTO> FindAsync(int groupId);
        Task<bool> UpdateAsync(GroupDTO group);
        IQueryable<GroupDTO> Read();
        IQueryable<GroupDTO> ReadGroupsInProject(int projectId);
    }
}