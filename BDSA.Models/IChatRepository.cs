﻿using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.Models
{
    public interface IChatRepository
    {
        Task<ChatDTO> CreateAsync(ChatDTO chat);
        Task<bool> DeleteAsync(int chatId);
        Task<ChatDTO> FindAsync(int chatId);
    }
}