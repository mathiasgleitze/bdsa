﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BDSA.Entities;
using BDSA.Shared;

namespace BDSA.Models
{
    public class ProjectRepository : IProjectRepository
    {
        private readonly IGroupItContext _context;

        public ProjectRepository(IGroupItContext context)
        {
            _context = context;
        }

        public async Task<ProjectDTO> CreateAsync(ProjectDTO project)
        {
            var entity = new Project
            {
                CourseId = project.CourseId,
                MaxSize = project.MaxSize 
            };

            _context.Projects.Add(entity);
            await _context.SaveChangesAsync();

            return await FindAsync(entity.Id);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await _context.Projects.FindAsync(id);
            if (entity == null)
            {
                return false;
            }

            _context.Projects.Remove(entity);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<ProjectDTO> FindAsync(int id)
        {
            var entity = await _context.Projects.FindAsync(id);
            if (entity == null)
            {
                return null;
            }

            var dto = new ProjectDTO
            {
                Id = entity.Id,
                CourseId = entity.CourseId,
                MaxSize = entity.MaxSize,
                Name = entity.Name
            };

            return dto;
        }

        public async Task<bool> UpdateAsync(ProjectDTO project)
        {
            var entity = await _context.Projects.FindAsync(project.Id);
            if (entity == null)
            {
                return false;
            }

            entity.CourseId = project.CourseId;

            return true;
        }
    }
}
