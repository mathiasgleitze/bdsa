﻿using BDSA.Entities;
using BDSA.Shared;
using System;
using System.Threading.Tasks;


using System.Collections.Generic;
using System.Text;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Models
{
    public class MessageRepository : IMessageRepository
    {
        private readonly IGroupItContext _context;
        public MessageRepository(IGroupItContext context)
        {
            _context = context;
        }

        // Creates a new message instance
        public async Task<MessageDTO> CreateAsync(MessageDTO message)
        {
            var entity = new Message
            {
                Time = message.Time,
                Text = message.Text,
                ChatId = message.ChatId,
                StudentId = message.StudentId,
            };

            _context.Messages.Add(entity);

            await _context.SaveChangesAsync();


            return await FindAsync(entity.ChatId,entity.Time);
        }

        // Finds message instance from an integer represntation of a message id
        public async Task<MessageDTO> FindAsync(int chatId, DateTime time)
        {
            var message = from m in _context.Messages
                          where (m.ChatId == chatId && m.Time == time)
                          select new MessageDTO
                          {
                              ChatId = m.ChatId,
                              Time = m.Time,
                              StudentId = m.StudentId,
                              Text = m.Text
                          };


            return await message.FirstOrDefaultAsync();
        }

        // Updates one message instance
        public async Task<bool> UpdateAsync(MessageDTO message)
        {
            var entity = await _context.Messages.FindAsync(message.ChatId, message.Time);

            if (entity == null)
            {
                return false;
            }
            entity.ChatId = message.ChatId;
            entity.StudentId = message.StudentId;
            entity.Text = message.Text;
            entity.Time = message.Time;


            await _context.SaveChangesAsync();

            return true;
        }

        // Deletes one message instance
        public async Task<bool> DeleteAsync(int chatId, DateTime time)
        {
            var message = await _context.Messages.FindAsync(chatId, time);

            if (message == null)
            {
                return false;
            }

            _context.Messages.Remove(message);

            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteMessagesAsync(int chatId)
        {
            var messages = _context.Messages.Where(message => message.ChatId == chatId).Select(message => message);
            if (messages.Count() == 0)
            {
                return false;
            }
            _context.Messages.RemoveRange(messages);

            await _context.SaveChangesAsync();
            return true;
        }

        public IQueryable<MessageDTO> Read(int chatId)
        {
            var entities = from c in _context.Messages
                           where c.ChatId == chatId
                           select new MessageDTO
                           {
                               ChatId = c.ChatId,
                               Time = c.Time,
                               StudentId = c.StudentId,
                               Text = c.Text
                           };

            return entities;
        }
    }
}