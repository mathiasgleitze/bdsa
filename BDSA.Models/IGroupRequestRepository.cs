﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDSA.Models
{
    public interface IGroupRequestRepository
    {
        Task<GroupRequestDTO> CreateAsync(GroupRequestDTO request);
        Task<bool> DeleteAsync(int fromGroup,int toGroup);
        Task<bool> UpdateAsync(int oldgroup,int newgroup);
        Task<GroupRequestDTO> FindAsync(int fromGroup, int toGroup);
        IQueryable<GroupRequestDTO> ReadFromGroup(int group);
        IQueryable<GroupRequestDTO> ReadToGroup(int group);
    }
}
