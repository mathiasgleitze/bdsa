﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDSA.Entities;
using BDSA.Shared;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Models
{
    public class TeacherCourseRepository : ITeacherCourseRepository
    {
        private readonly IGroupItContext _context;

        public TeacherCourseRepository(IGroupItContext context)
        {
            _context = context;
        }

        public async Task<TeacherCourseDTO> CreateAsync(TeacherCourseDTO teacherCourse)
        {
            var entity = new TeacherCourse
            {
                TeacherId = teacherCourse.TeacherId,
                CourseId = teacherCourse.CourseId
            };

            _context.TeacherCourses.Add(entity);
            await _context.SaveChangesAsync();

            var teacherCourses = await _context.TeacherCourses.FindAsync(entity.TeacherId, entity.CourseId);
            if (teacherCourses == null)
            {
                return null;
            }

            return new TeacherCourseDTO
            {
                TeacherId = teacherCourses.TeacherId,
                CourseId = teacherCourses.CourseId
            };
        }

        public async Task<bool> DeleteAsync(int teacherId, int courseId)
        {
            var entity = await _context.TeacherCourses.FindAsync(teacherId, courseId);

            if (entity == null)
            {
                return false;
            }

            _context.TeacherCourses.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }

        public IQueryable<CourseDTO> FindCourses(int teacherId)
        {
            return from teacherCourse in _context.TeacherCourses
                   where teacherCourse.TeacherId == teacherId
                   select new CourseDTO
                   {
                       Id = teacherCourse.CourseId,
                       Name = _context.Courses
                              .Where(course => course.Id == teacherCourse.CourseId)
                              .Select(course => course.Name)
                              .FirstOrDefault()
                   };
        }

        public IQueryable<TeacherDTO> FindTeachers(int courseId)
        {
            var teacherIds = from teacherCourse in _context.TeacherCourses
                             where teacherCourse.CourseId == courseId
                             select teacherCourse.TeacherId;

            return from teacher in _context.Teachers
                   where teacherIds.Contains(teacher.Id)
                   select new TeacherDTO
                   {
                       Id = teacher.Id,
                       Name = teacher.Name
                   };
        }
        public async Task<TeacherCourseDTO> FindAsync(int teacherId, int courseId)
        {
            var teacherCourses = from c in _context.TeacherCourses
                                 where c.TeacherId == teacherId && c.CourseId == courseId
                                 select new TeacherCourseDTO
                                 {
                                     TeacherId = c.TeacherId,
                                     CourseId = c.CourseId
                                 };

            return await teacherCourses.FirstOrDefaultAsync();
        }
        public IQueryable<TeacherCourseDTO> Read()
        {
            return from tc in _context.TeacherCourses
                   select new TeacherCourseDTO
                   {
                       TeacherId = tc.TeacherId,
                       CourseId = tc.CourseId
                   };
        }
    }
}
