﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BDSA.Models
{
    public interface ITeacherRepository
    {
        Task<TeacherDTO> CreateAsync(TeacherDTO teacher);
        Task<bool> DeleteAsync(int id);
        Task<TeacherDTO> FindAsync(int id);
        Task<bool> UpdateAsync(TeacherDTO teacher);
    }
}
