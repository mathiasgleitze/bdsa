﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDSA.Models
{
    public interface ITeacherCourseRepository
    {
        Task<TeacherCourseDTO> CreateAsync(TeacherCourseDTO manages);
        Task<bool> DeleteAsync(int teacherId, int courseId);
        IQueryable<TeacherDTO> FindTeachers(int courseId);
        IQueryable<CourseDTO> FindCourses(int teacherId);
        IQueryable<TeacherCourseDTO> Read();
        Task<TeacherCourseDTO> FindAsync(int teacherId, int courseId);
    }
}
