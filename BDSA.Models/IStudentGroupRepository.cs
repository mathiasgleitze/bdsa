﻿using System.Linq;
using System.Threading.Tasks;
using BDSA.Shared;

namespace BDSA.Models
{
    public interface IStudentGroupRepository
    {
        Task<StudentGroupDTO> CreateAsync(StudentGroupDTO studentGroup);
        Task<bool> DeleteAsync(int studentId, int groupId);
        Task<StudentGroupDTO> FindAsync(int studentId, int groupId);
        IQueryable<GroupDTO> FindGroups(int studentId);
        IQueryable<StudentDTO> FindStudents(int groupId);
        IQueryable<StudentGroupDTO> Read();
    }
}