﻿using BDSA.Entities;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Models
{
    public class GroupRepository : IGroupRepository
    {
        private readonly IGroupItContext _context;
        public GroupRepository(IGroupItContext context)
        {
            _context = context;
        }

        // Creates a new group instance
        public async Task<bool> CreateAsync(GroupDTO group)
        {
            var entity = new Group
            {
                Id = group.Id,
                ChatId = group.ChatId,
                ProjectId = group.ProjectId
            };

            _context.Groups.Add(entity);

            await _context.SaveChangesAsync();

            return true;
        }

        // Finds group instance from an integer represntation of a group id
        public async Task<GroupDTO> FindAsync(int groupId)
        {
            var entity = await _context.Groups.FindAsync(groupId);
            if (entity == null)
            {
                return null;
            }

            var dto = new GroupDTO
            {
                Id = entity.Id,
                ProjectId=entity.ProjectId,
                ChatId=entity.ChatId
                
            };

            return dto;
        }

        // Updates one group instance
        public async Task<bool> UpdateAsync(GroupDTO group)
        {
            var entity = await _context.Groups.FindAsync(group.Id);

            if (entity == null)
            {
                return false;
            }

            entity.Id = group.Id;
            entity.ChatId = group.ChatId;
            entity.ProjectId = group.ProjectId;

            await _context.SaveChangesAsync();

            return true;
        }

        // Deletes one group instance
        public async Task<bool> DeleteAsync(int groupID)
        {
            var group = await _context.Groups.FindAsync(groupID);

            if (group == null)
            {
                return false;
            }

            _context.Groups.Remove(group);

            await _context.SaveChangesAsync();

            return true;
        }
        public IQueryable<GroupDTO> ReadGroupsInProject(int projectId)
        {
            var groups = from g in _context.Groups
                         where g.ProjectId == projectId
                         select new GroupDTO
                         {
                             Id = g.Id,
                             ProjectId = g.ProjectId,
                             ChatId = g.ChatId
                         };
            return groups;
        }
        public IQueryable<GroupDTO> Read()
        {
            return from sc in _context.Groups
                   select new GroupDTO
                   {
                       Id=sc.Id,
                       ProjectId=sc.ProjectId,
                       ChatId=sc.ChatId,
                       
                   };
        }
    }
}
