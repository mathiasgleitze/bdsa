﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BDSA.Models
{
    public interface IStudentCourseRepository
    {
            Task<StudentCourseDTO> CreateAsync(StudentCourseDTO enrolled);
            Task<StudentCourseDTO> FindAsync(int studentId, int courseId);
            Task<bool> DeleteAsync(int studentId,int courseId);
            IQueryable<StudentDTO> FindStudents(int courseId);
            IQueryable<CourseDTO> FindCourses(int studentId);
            IQueryable<StudentCourseDTO> Read();

    }
}
