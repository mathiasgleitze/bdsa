﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDSA.Entities;
using BDSA.Shared;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Models
{
    public class MatchRepository:IMatchRepository { 
        private readonly IGroupItContext _context;
        public MatchRepository(IGroupItContext context)
        {
            _context = context;
        }
        public async Task<MatchDTO> CreateAsync(MatchDTO match)
        {
            var entity = new Match
            {
                GroupOneId = match.GroupOne,
                GroupTwoId = match.GroupTwo,
                ChatId = match.ChatId
            };
            var exist = await _context.Matches.FindAsync(entity.GroupOne, entity.GroupTwo);
            if (exist == null)
            {
                _context.Matches.Add(entity);

                try
                {
                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateConcurrencyException)
                {

                    throw;
                }               
             
                return await FindAsync(entity.GroupOneId,entity.GroupTwoId);
            }
            return null;
        }

        public async Task<bool> DeleteAsync(int groupOne, int groupTwo)
        {
            var match = await _context.Matches.FindAsync(groupOne, groupTwo);

            if (match == null)
            {
                match = await _context.Matches.FindAsync(groupTwo, groupOne);
                if (match == null)
                {
                    return false;
                }

            }
            
          
            _context.Matches.Remove(match);


           
            
                try
                {
                    await _context.SaveChangesAsync();
                  
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw;
                }

            

            return true;
        }

        public async Task<MatchDTO> FindAsync(int groupOne, int groupTwo)
        {
            var request = from r in _context.Matches
                          where ((r.GroupOneId == groupOne && r.GroupTwoId == groupTwo) || (r.GroupOneId == groupTwo && r.GroupTwoId == groupOne))
                          select new MatchDTO
                          {
                              GroupOne = r.GroupOneId,
                              GroupTwo = r.GroupTwoId
                          };


            return await request.FirstOrDefaultAsync();
        }

        public IQueryable<MatchDTO> FindAllMatches(int id)
        {
            var entities = from r in _context.Matches
                           where (r.GroupOneId == id || r.GroupTwoId == id)
                           select new MatchDTO
                           {
                               GroupOne = r.GroupOneId,
                               GroupTwo = r.GroupTwoId
                           };

            return entities;
        }
        public async Task<bool> UpdateAsync(int oldGroup, int newGroup)
        {
            if (!_context.Matches.Any())
            {
                return false;
            }

            foreach (Match m in _context.Matches)
            {
                Match match;
                if (m.GroupOneId == oldGroup)
                {
                    match = new Match { GroupOneId = newGroup, GroupTwoId = m.GroupTwoId };
                }
                else if (m.GroupTwoId == oldGroup)
                {
                    match = new Match { GroupOneId = m.GroupOneId, GroupTwoId = newGroup };
                }
                else
                {
                    continue;
                }

                _context.Matches.Remove(m);
                _context.Matches.Add(match);
                await _context.SaveChangesAsync();

            }

            return true;
        }


    }
    
}
