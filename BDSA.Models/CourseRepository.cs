﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BDSA.Entities;
using BDSA.Shared;

namespace BDSA.Models
{
    public class CourseRepository : ICourseRepository
    {
        private readonly IGroupItContext _context;

        public CourseRepository(IGroupItContext context)
        {
            _context = context;
        }

        public async Task<CourseDTO> CreateAsync(CourseDTO course)
        {
            var entity = new Course
            {
                Name = course.Name
            };

            _context.Courses.Add(entity);
            await _context.SaveChangesAsync();
            
            return await FindAsync(entity.Id);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await _context.Courses.FindAsync(id);
            if (entity == null)
            {
                return false;
            }

            _context.Courses.Remove(entity);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<CourseDTO> FindAsync(int id)
        {
            var entity = await _context.Courses.FindAsync(id);
            if (entity == null)
            {
                return null;
            }

            var dto = new CourseDTO
            {
                Id = entity.Id,
                Name = entity.Name
            };

            return dto;
        }

        public async Task<bool> UpdateAsync(CourseDTO course)
        {
            var entity = await _context.Courses.FindAsync(course.Id);
            if (entity == null)
            {
                return false;
            }

            entity.Name = course.Name;

            return true;
        }
    }
}
