﻿using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BDSA.Models
{
    public interface ICourseRepository
    {
        Task<CourseDTO> CreateAsync(CourseDTO course);
        Task<bool> DeleteAsync(int id);
        Task<CourseDTO> FindAsync(int id);
        Task<bool> UpdateAsync(CourseDTO course);
    }
}
