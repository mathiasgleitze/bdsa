﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDSA.Entities;
using BDSA.Shared;

namespace BDSA.Models
{
    public class ProjectProfileRepository : IProjectProfileRepository
    {
        private readonly IGroupItContext _context;

        public ProjectProfileRepository(IGroupItContext context)
        {
            _context = context;
        }

        public async Task<ProjectProfileDTO> CreateAsync(ProjectProfileDTO projectProfile)
        {
            var entity = new ProjectProfile
            {
                StudentId = projectProfile.StudentId,
                ProjectId = projectProfile.ProjectId,
                Ambitions = projectProfile.Ambitions,
                MeetingDisciplin = projectProfile.MeetingDisciplin,
                SkillLevel = projectProfile.SkillLevel,
                Flexibility = projectProfile.Flexibility,
                About = projectProfile.About
            };

            _context.ProjectProfiles.Add(entity);
            await _context.SaveChangesAsync();

            return await FindAsync(entity.StudentId, entity.ProjectId);
        }

        public async Task<bool> DeleteAsync(int studentId, int projectId)
        {
            var entity = await _context.ProjectProfiles.FindAsync(studentId, projectId);
            if (entity == null)
            {
                return false;
            }

            _context.ProjectProfiles.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<ProjectProfileDTO> FindAsync(int studentId, int projectId)
        {
            var entity = await _context.ProjectProfiles.FindAsync(studentId,projectId);
            if (entity == null)
            {
                return null;
            }
            var dto = new ProjectProfileDTO
            {
                StudentId = entity.StudentId,
                ProjectId = entity.ProjectId,
                Ambitions = entity.Ambitions,
                MeetingDisciplin = entity.MeetingDisciplin,
                SkillLevel = entity.SkillLevel,
                Flexibility = entity.Flexibility,
                About = entity.About
            };

            return dto;
        }

        public IQueryable<ProjectProfileDTO> FindProjectProfilesAsync(int studentId)
        {
            return from projectProfile in _context.ProjectProfiles
                   where projectProfile.StudentId == studentId
                   select new ProjectProfileDTO
                   {
                       StudentId = projectProfile.StudentId,
                       ProjectId = projectProfile.ProjectId,
                       Ambitions = projectProfile.Ambitions,
                       MeetingDisciplin = projectProfile.MeetingDisciplin,
                       SkillLevel = projectProfile.SkillLevel,
                       Flexibility = projectProfile.Flexibility,
                       About = projectProfile.About
                   };
        }

        public async Task<bool> UpdateAsync(ProjectProfileDTO projectProfile)
        {
            var entity = await _context.ProjectProfiles.FindAsync(projectProfile.StudentId, projectProfile.ProjectId);
            if (entity == null)
            {
                return false;
            }

            entity.StudentId = projectProfile.StudentId;
            entity.ProjectId = projectProfile.ProjectId;
            entity.Ambitions = projectProfile.Ambitions;
            entity.MeetingDisciplin = projectProfile.MeetingDisciplin;
            entity.SkillLevel = projectProfile.SkillLevel;
            entity.Flexibility = projectProfile.Flexibility;
            entity.About = projectProfile.About;

            await _context.SaveChangesAsync();

            return true;
        }
    }
}
