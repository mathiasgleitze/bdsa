﻿using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BDSA.Entities;
using BDSA.Shared;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace BDSA.Models
{
    public class StudentRepository : IStudentRepository
    {
        private readonly IGroupItContext _context;

        public StudentRepository(IGroupItContext context)
        {
            _context = context;
        }

        public async Task<StudentDTO> CreateAsync(StudentDTO student)
        {
            var entity = new Student
            {
                Name = student.Name,
                Mail = student.Mail,
                Address = student.Address,
                About = student.About
            };

            _context.Students.Add(entity);
            await _context.SaveChangesAsync();

            return await FindAsync(entity.Id);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await _context.Students.FindAsync(id);
            if (entity == null)
            {
                return false;
            }

            _context.Students.Remove(entity);
            await _context.SaveChangesAsync();
            return true;
        }

        public async Task<StudentDTO> FindAsync(int id)
        {
            var entity = await _context.Students.FindAsync(id);
            if (entity == null)
            {
                return null;
            }

            var dto = new StudentDTO
            {
                Id = entity.Id,
                Name = entity.Name,
                Mail = entity.Mail,
                Address = entity.Address,
                About = entity.About
            };

            return dto;
        }

        public IQueryable<StudentDTO> FindStudentsByName(string name)
        {
            return from student in _context.Students
                   where student.Name.StartsWith(name)
                   orderby student.Name
                   select new StudentDTO
                   {
                       Id = student.Id,
                       Name = student.Name,
                       Mail = student.Mail,
                       Address = student.Address,
                       About = student.About
                   };
        }

        public async Task<int> FindIdByMailAsync(string mail)
        {
            var ids = from student in _context.Students
                      where student.Mail.Equals(mail)
                      select student.Id;

            if (ids.Count() == 0)
            {
                return 0;
            }

            return await ids.FirstOrDefaultAsync();
        }
        
        public async Task<bool> UpdateAsync(StudentDTO student)
        {
            var entity = await _context.Students.FindAsync(student.Id);
            if (entity == null)
            {
                return false;
            }
            
            entity.Address = student.Address;
            entity.About = student.About;
            
            await _context.SaveChangesAsync();
            return true;
        }
    }
}

