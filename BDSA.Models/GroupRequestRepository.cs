﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDSA.Entities;
using BDSA.Shared;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Models
{
    public class GroupRequestRepository : IGroupRequestRepository
    {
        private readonly IGroupItContext _context;

        public GroupRequestRepository(IGroupItContext context)
        {
            _context = context;
        }

        public async Task<GroupRequestDTO> CreateAsync(GroupRequestDTO request)
        {
            var entity = new GroupRequest
            {
                FromGroup = request.FromGroup,
                ToGroup = request.ToGroup
            };
            var exist = await _context.GroupRequests.FindAsync(entity.ToGroup, entity.FromGroup);
            if (exist == null)
            {
                _context.GroupRequests.Add(entity);
                try
                {
                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateConcurrencyException)
                {

                    throw;
                }
                return request;
            }
            return null;
            
            
        }

        public async Task<bool> DeleteAsync(int fromGroup, int toGroup)
        {
            var request = await _context.GroupRequests.FindAsync(fromGroup, toGroup);

            if (request == null)
            {
                request = await _context.GroupRequests.FindAsync(toGroup, fromGroup);
                if (request == null)
                {
                    return false;
                }
                
            }

            _context.GroupRequests.Remove(request);

            try
            {
                await _context.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return true;
        }

        public async Task<GroupRequestDTO> FindAsync(int fromGroup, int toGroup)
        {
            var request = from r in _context.GroupRequests
                          where ((r.FromGroup==fromGroup&&r.ToGroup==toGroup)||(r.FromGroup == toGroup && r.ToGroup == fromGroup))
                          select new GroupRequestDTO
                          {
                              FromGroup=r.FromGroup,
                              ToGroup=r.ToGroup
                          };


            return await request.FirstOrDefaultAsync();
        }

        public IQueryable<GroupRequestDTO> ReadFromGroup(int group)
        {
            var entities = from r in _context.GroupRequests
                           where r.FromGroup==@group
                           select new GroupRequestDTO
                           {
                               FromGroup=r.FromGroup,
                               ToGroup=r.ToGroup
                           };

            return entities;
        }

        public IQueryable<GroupRequestDTO> ReadToGroup(int group)
        {
            var entities = from r in _context.GroupRequests
                           where r.ToGroup == @group
                           select new GroupRequestDTO
                           {
                               FromGroup = r.FromGroup,
                               ToGroup = r.ToGroup
                           };

            return entities;
        }

        public async Task<bool> UpdateAsync(int oldGroup, int newGroup)
        {
            if (!_context.GroupRequests.Any())
            {
                return false;
            }
           
            foreach (GroupRequest r in _context.GroupRequests)
            {
                GroupRequest request;
                if (r.FromGroup == oldGroup)
                {
                    request = new GroupRequest { FromGroup = newGroup, ToGroup = r.ToGroup };

                }
                else if (r.ToGroup == oldGroup)
                {
                    request = new GroupRequest { FromGroup = r.FromGroup, ToGroup = newGroup};
                }
                else
                {
                    continue;
                }
                
                _context.GroupRequests.Remove(r);
                _context.GroupRequests.Add(request);
                await _context.SaveChangesAsync();

            }

            return true;
        }




    }
}
