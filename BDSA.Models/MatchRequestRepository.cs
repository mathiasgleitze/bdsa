﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDSA.Entities;
using BDSA.Shared;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Models
{
    public class MatchRequestRepository : IMatchRequestRepository
    {
        private readonly IGroupItContext _context;

        public MatchRequestRepository(IGroupItContext context)
        {
            _context = context;
        }
        public async Task<MatchRequestDTO> CreateAsync(MatchRequestDTO request)
        {
            var entity = new MatchRequest
            {
                FromGroup = request.FromGroup,
                ToGroup = request.ToGroup
            };
            var exist = await _context.MatchRequests.FindAsync(entity.ToGroup, entity.FromGroup);
            if (exist == null)
            {
                _context.MatchRequests.Add(entity);
                try
                {
                    await _context.SaveChangesAsync();

                }
                catch (DbUpdateConcurrencyException)
                {

                    throw;
                }
                return request;
            }
            return null;
        }
        public async Task<bool> DeleteAll(int group)
        {
            var requests = _context.MatchRequests.Where(request => request.FromGroup == group || request.ToGroup == group).Select(request => request);
            if (!requests.Any())
            {
                return false;
            }
            _context.MatchRequests.RemoveRange(requests);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<bool> DeleteAsync(int fromGroup, int toGroup)
        {
            var request = await _context.MatchRequests.FindAsync(fromGroup, toGroup);

            if (request == null)
            {
                request = await _context.MatchRequests.FindAsync(toGroup, fromGroup);
                if (request == null)
                {
                    return false;
                }

            }

            _context.MatchRequests.Remove(request);

            try
            {
                await _context.SaveChangesAsync();

            }
            catch (DbUpdateConcurrencyException)
            {

                throw;
            }

            return true;
        }

        public async Task<MatchRequestDTO> FindAsync(int fromGroup, int toGroup)
        {
            var request = from r in _context.MatchRequests
                          where (r.FromGroup == fromGroup && r.ToGroup == toGroup)
                          select new MatchRequestDTO
                          {
                              FromGroup = r.FromGroup,
                              ToGroup = r.ToGroup
                          };


            return await request.FirstOrDefaultAsync();
        }

        public IQueryable<MatchRequestDTO> ReadFrom(int groupId)
        {
            var entities = from r in _context.MatchRequests
                           where r.FromGroup == groupId
                           select new MatchRequestDTO
                           {
                               FromGroup = r.FromGroup,
                               ToGroup = r.ToGroup
                           };

            return entities;
        }
        public IQueryable<MatchRequestDTO> ReadTo(int groupId)
        {
            var entities = from r in _context.MatchRequests
                           where r.ToGroup == groupId
                           select new MatchRequestDTO
                           {
                               FromGroup=r.FromGroup,
                               ToGroup=r.ToGroup
                           };
            return entities;
        }

    }
}
