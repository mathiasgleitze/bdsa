﻿using BDSA.Entities;
using BDSA.Shared;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Models
{
    public class ChatRepository : IChatRepository
    {
        private readonly IGroupItContext _context;
        public ChatRepository(IGroupItContext context)
        {
            _context = context;
        }

        // Creates a new chat instance
        public async Task<ChatDTO> CreateAsync(ChatDTO chat)
        {
            var entity = new Chat
            {
                Id = chat.Id,
            };

            _context.Chats.Add(entity);
            


            return await FindAsync(entity.Id);
        }

        // Finds chat instance from an integer represntation of a chat id
        public async Task<ChatDTO> FindAsync(int chatId)
        {
            var entity = await _context.Chats.FindAsync(chatId);
            if (entity == null)
            {
                return null;
            }

            var dto = new ChatDTO
            {
                Id = entity.Id,
            
            };

            return dto;
        }

      

        // Deletes one chat instance
        public async Task<bool> DeleteAsync(int chatId)
        {
            var chats = await _context.Chats.FindAsync(chatId);

            if (chats == null)
            {
                return false;
            }

            _context.Chats.Remove(chats);

            await _context.SaveChangesAsync();

            return true;
        }
    }
}
