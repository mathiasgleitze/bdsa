﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BDSA.Entities;
using BDSA.Shared;
using Microsoft.EntityFrameworkCore;

namespace BDSA.Models
{
    public class StudentGroupRepository : IStudentGroupRepository
    {
        private readonly IGroupItContext _context;

        public StudentGroupRepository(IGroupItContext context)
        {
            _context = context;
        }

        public IQueryable<StudentGroupDTO> Read()
        {
            return from sc in _context.StudentGroups
                   select new StudentGroupDTO
                   {
                       StudentId = sc.StudentId,
                       GroupId = sc.GroupId
                   };
        }
        public async Task<StudentGroupDTO> FindAsync(int studentId, int groupId)
        {
            var studentGroup = from c in _context.StudentGroups
                                 where c.StudentId == studentId && c.GroupId == groupId
                                 select new StudentGroupDTO
                                 {
                                     StudentId = c.StudentId,
                                     GroupId = c.GroupId
                                 };

            return await studentGroup.FirstOrDefaultAsync();
        }

        public async Task<StudentGroupDTO> CreateAsync(StudentGroupDTO studentGroup)
        {
            if (await _context.StudentGroups.FindAsync(studentGroup.StudentId, studentGroup.GroupId) != null) return null;

            var entity = new StudentGroup
            {
                StudentId = studentGroup.StudentId,
                GroupId = studentGroup.GroupId
            };

            await _context.StudentGroups.AddAsync(entity);
            await _context.SaveChangesAsync();

            return studentGroup;
        }

        public async Task<bool> DeleteAsync(int studentId,int groupId)
        {
            var entity = await _context.StudentGroups.FindAsync(studentId, groupId);

            if (entity == null)
            {
                return false;
            }

            _context.StudentGroups.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }
        //returns all students in the group with id = groupId
        public IQueryable<StudentDTO> FindStudents(int groupId)
        {
            var students = from StudentGroup in _context.StudentGroups
                         where StudentGroup.GroupId == groupId
                         select new StudentDTO
                         {
                                Id = StudentGroup.Student.Id,
                                Name = StudentGroup.Student.Name,
                                Mail = StudentGroup.Student.Mail,
                                Address = StudentGroup.Student.Address,
                                About = StudentGroup.Student.About
                         };
            return students;
        }

        public IQueryable<GroupDTO> FindGroups(int studentId)
        {
            var groups = from StudentGroup in _context.StudentGroups
                         where StudentGroup.StudentId == studentId
                         select new GroupDTO
                         {
                               Id = StudentGroup.Group.Id,
                               ProjectId = StudentGroup.Group.ProjectId,
                               ChatId = StudentGroup.Group.ChatId
                         };
            return groups;
        }
    }
}
