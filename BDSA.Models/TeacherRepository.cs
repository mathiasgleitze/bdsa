﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using BDSA.Entities;
using BDSA.Shared;

namespace BDSA.Models
{
    public class TeacherRepository : ITeacherRepository
    {
        private readonly IGroupItContext _context;

        public TeacherRepository(IGroupItContext context)
        {
            _context = context;
        }

        public async Task<TeacherDTO> CreateAsync(TeacherDTO teacher)
        {
            var entity = new Teacher
            {
                Name = teacher.Name
            };

            _context.Teachers.Add(entity);
            await _context.SaveChangesAsync();

            return await FindAsync(entity.Id);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            var entity = await _context.Teachers.FindAsync(id);

            if (entity == null)
            {
                return false;
            }

            _context.Teachers.Remove(entity);
            await _context.SaveChangesAsync();

            return true;
        }

        public async Task<TeacherDTO> FindAsync(int id)
        {
            var entity = await _context.Teachers.FindAsync(id);

            if (entity == null)
            {
                return null;
            }

            return new TeacherDTO
            {
                Id = entity.Id,
                Name = entity.Name
            };
        }

        public async Task<bool> UpdateAsync(TeacherDTO teacher)
        {
            var entity = await _context.Teachers.FindAsync(teacher.Id);

            if (entity == null)
            {
                return false;
            }

            entity.Name = teacher.Name;

            await _context.SaveChangesAsync();

            return true;
        }
    }
}
