﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class GroupDTO
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int ChatId { get; set; }
    }
}
