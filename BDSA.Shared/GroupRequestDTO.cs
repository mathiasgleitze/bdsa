﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class GroupRequestDTO
    {
        public int FromGroup { get; set; }
        public int ToGroup { get; set; }
    }
}
