﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class StudentGroupDTO
    {
        public int StudentId { get; set; }
        public int GroupId { get; set; }
    }
}
