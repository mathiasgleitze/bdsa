﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class ProjectProfileDTO
    {
        public int StudentId { get; set; }
        public int ProjectId { get; set; }
        public string About { get; set; }
        public int Ambitions { get; set; }
        public int MeetingDisciplin { get; set; }
        public int SkillLevel { get; set; }
        public int Flexibility { get; set; }
    }
}
