﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class TeacherCourseDTO
    {
        public int TeacherId { get; set; }
        public int CourseId { get; set; }
    }
}
