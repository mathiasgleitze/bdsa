﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class StudentCourseDTO
    {
        public int StudentId { get; set; }
        public int CourseId { get; set; }
    }
}
