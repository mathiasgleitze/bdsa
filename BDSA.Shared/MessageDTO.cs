﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class MessageDTO
    {
        public DateTime Time { get; set; }
        public int ChatId { get; set; }
        public string Text { get; set; }
        public int StudentId { get; set; }
    }
}
