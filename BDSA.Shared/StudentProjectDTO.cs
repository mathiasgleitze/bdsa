﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class StudentProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Mail { get; set; }
        public string Address { get; set; }
        public string AboutStudent { get; set; }
        public string AboutProject { get; set; }
        public int Ambitions { get; set; }
        public int MeetingDisciplin { get; set; }
        public int SkillLevel { get; set; }
        public int Flexibility { get; set; }
    }
}
