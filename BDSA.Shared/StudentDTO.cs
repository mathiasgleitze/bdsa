﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class StudentDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Mail { get; set; }
        public string Address { get; set; }
        public string About { get; set; }
    }
}
