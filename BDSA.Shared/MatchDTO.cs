﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class MatchDTO
    {
        public int GroupOne { get; set; }
        public int GroupTwo { get; set; }
        public int ChatId { get; set; }
    }
}
