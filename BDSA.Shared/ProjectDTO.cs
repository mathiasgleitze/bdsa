﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string CourseName { get; set; }
        public int CourseId { get; set; }
        public int MaxSize { get; set; }
    }
}
