﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BDSA.Shared
{
    public class TeacherDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
