﻿using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.Shared;
using System.Net.Http.Formatting;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class ChatRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly MockHandler mockHandler = new MockHandler();

        [Fact]
        public async Task FindAsync_given_existing_chat_id_sends_get_to_existing_uri()
        {
            var dummyChat = new ChatDTO { Id = 1 };
            var content = new ObjectContent<ChatDTO>(dummyChat, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ChatRepository(httpClient);

            var returnedChat = await repository.FindAsync(dummyChat.Id);

            Assert.Equal(dummyChat, returnedChat);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/chat/{dummyChat.Id}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_chat_id_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ChatRepository(httpClient);

            var result = await repository.FindAsync(4);

            Assert.Null(result);
        }

        [Fact]
        public async Task DeleteAsync_given_existing_chat_id_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ChatRepository(client);

            var result = await repository.DeleteAsync(3);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/chat/{3}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_chat_id_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ChatRepository(client);

            var result = await repository.DeleteAsync(3);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/chat/{3}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
        
        [Fact]
        public async Task CreateAsync_returns_chat()
        {
            var dummyChat = new ChatDTO { Id = 1 };
            var content = new ObjectContent<ChatDTO>(dummyChat, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.Created, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ChatRepository(client);
            
            var returnedChat = await repository.CreateAsync(dummyChat);
            Assert.Equal(dummyChat, returnedChat);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/chat");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
    }
}








