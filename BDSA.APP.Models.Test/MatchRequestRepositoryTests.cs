﻿using BDSA.Shared;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class MatchRequestRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly MockHandler mockHandler = new MockHandler();
        
        [Fact]
        public async Task FindAsync_given_existing_matchRequest_ids_sends_get_to_existing_uri()
        {
            var dto = new MatchRequestDTO { FromGroup = 1, ToGroup = 2 };
            var content = new ObjectContent<MatchRequestDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRequestRepository(httpClient);

            var match = await repository.FindAsync(dto.FromGroup, dto.ToGroup);

            Assert.Equal(dto, match);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/matchRequest/{dto.FromGroup}/{dto.ToGroup}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_MatchRequest_ids_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRequestRepository(httpClient);

            var result = await repository.FindAsync(3, 4);

            Assert.Null(result);
        }

        [Fact]
        public async Task GetFromAsync_given_existing_match_request_id_sends_get_to_correct_uri()
        {
            var dtos
                = new[]
            {
                 new MatchRequestDTO{FromGroup=1, ToGroup=2},
                 new MatchRequestDTO{FromGroup=1, ToGroup=3}
            };

            var content = new ObjectContent<IEnumerable<MatchRequestDTO>>(dtos, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRequestRepository(httpClient);

            var matchRequests = await repository.GetFromAsync(1);

            Assert.Equal(dtos, matchRequests);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/matchRequest/from/{1}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task GetFromAsync_given_non_existing_match_request_id_returns_empty_collection()
        {
            var emptyCollection = new List<MatchRequestDTO>();
            var content = new ObjectContent<IEnumerable<MatchRequestDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRequestRepository(httpClient);
            var matchRequests = await repository.GetFromAsync(1);

            Assert.Empty(matchRequests);
        }

        [Fact]
        public async Task GetToAsync_given_existing_match_request_id_sends_get_to_correct_uri()
        {
            var dtos = new[]
            {
                 new MatchRequestDTO{FromGroup=2, ToGroup=1},
                 new MatchRequestDTO{FromGroup=3, ToGroup=1}
            };

            var content = new ObjectContent<IEnumerable<MatchRequestDTO>>(dtos, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRequestRepository(httpClient);

            var matchRequests = await repository.GetToAsync(1);

            Assert.Equal(dtos, matchRequests);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/matchRequest/to/{1}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task GetToAsync_given_non_existing_match_request_id_returns_empty_collection()
        {
            var emptyCollection = new List<MatchRequestDTO>();
            var content = new ObjectContent<IEnumerable<MatchRequestDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRequestRepository(httpClient);
            var matchRequests = await repository.GetToAsync(1);

            Assert.Empty(matchRequests);
        }

        [Fact]
        public async Task DeleteAsync_given_existing_match_request_ids_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRequestRepository(client);

            var result = await repository.DeleteAsync(3, 1);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/matchRequest/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_match_request_ids_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRequestRepository(client);

            var result = await repository.DeleteAsync(3, 1);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/matchRequest/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task CreateAsync_returns_match_request()
        {
            var dto = new MatchRequestDTO { FromGroup = 1, ToGroup = 2 };
            var content = new ObjectContent<MatchRequestDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.Created, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRequestRepository(client);

            var matchRequest = await repository.CreateAsync(dto);
            Assert.Equal(dto, matchRequest);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/matchRequest");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAllAsync_given_existing_match_request_ids_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRequestRepository(client);

            var result = await repository.DeleteAllAsync(1);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/matchrequest/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAllAsync_given_non_existing_ids_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRequestRepository(client);

            var result = await repository.DeleteAllAsync(1);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/matchrequest/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
    }
}
