﻿using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.Shared;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class MatchRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly MockHandler mockHandler = new MockHandler();

        [Fact]
        public async Task UpdateAsync_given_existing_match_ids_sends_update()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRepository(client);

            var result = await repository.UpdateAsync(3, 1);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/match/update/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Put
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
        [Fact]
        public async Task UpdateAsync_given_non_existing_match_ids_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRepository(client);

            var result = await repository.UpdateAsync(3, 1);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/match/update/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Put
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }



        [Fact]
        public async Task FindAsync_given_existing_match_ids_sends_get_to_existing_uri()
        {
            var dummyMatch = new MatchDTO { GroupOne=1,GroupTwo=2 };
            var content = new ObjectContent<MatchDTO>(dummyMatch, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRepository(httpClient);

            var returnedMatch = await repository.FindAsync(dummyMatch.GroupOne,dummyMatch.GroupTwo);

            Assert.Equal(dummyMatch, returnedMatch);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/match/{dummyMatch.GroupOne}/{dummyMatch.GroupTwo}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_match_id_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRepository(httpClient);

            var result = await repository.FindAsync(3,4);

            Assert.Null(result);
        }

        [Fact]
        public async Task FindAllMatchesAsync_given_existing_match_ids_sends_get_to_correct_uri()
        {
            var dummyMatches = new[]
            {
                 new MatchDTO{GroupOne=1,GroupTwo=2},
                 new MatchDTO{GroupOne=1,GroupTwo=3}
            };
            var content = new ObjectContent<IEnumerable<MatchDTO>>(dummyMatches, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRepository(httpClient);

            var returnedMatches = await repository.FindAllMatchesAsync(1);

            Assert.Equal(dummyMatches, returnedMatches);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/match/{1}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAllMatchesAsync_given_non_existing_match_ids_returns_empty_collection()
        {
            var emptyCollection = new List<MatchDTO>();
            var content = new ObjectContent<IEnumerable<MatchDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRepository(httpClient);
            var matches = await repository.FindAllMatchesAsync(1);

            Assert.Empty(matches);
        }

        [Fact]
        public async Task DeleteAsync_given_existing_ids_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRepository(client);

            var result = await repository.DeleteAsync(3,1);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/match/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_ids_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRepository(client);

            var result = await repository.DeleteAsync(3,1);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/match/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task CreateAsync_returns_match()
        {
            var dummyMatch = new MatchDTO { GroupOne = 1,GroupTwo = 2 };
            var content = new ObjectContent<MatchDTO>(dummyMatch, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.Created, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MatchRepository(client);
            
            var match = await repository.CreateAsync(dummyMatch);

            Assert.Equal(dummyMatch, match);
            
            var expectedUriToBeCalled = new Uri($"{baseAddress}api/match");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
    }
}
