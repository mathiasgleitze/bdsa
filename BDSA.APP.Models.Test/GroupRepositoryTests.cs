﻿using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.Shared;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using BDSA.GroupITApp.Models.Repositories;
using System.Linq;

namespace BDSA.GroupITApp.Models.Test
{
    public class GroupRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly MockHandler mockHandler = new MockHandler();
        
        [Fact]
        public async Task FindAsync_given_existing_group_id_sends_get_to_existing_uri()
        {
            var dummyGroup = new GroupDTO { Id = 1 };
            var content = new ObjectContent<GroupDTO>(dummyGroup, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRepository(httpClient);

            var returnedGroup = await repository.FindAsync(dummyGroup.Id);

            Assert.Equal(dummyGroup, returnedGroup);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/group/{dummyGroup.Id}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_group_id_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty)); // Expect 404 (NotFound)

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRepository(httpClient);

            var result = await repository.FindAsync(4); // Non-existing id

            Assert.Null(result);
        }

        [Fact]
        public async Task ReadAsync_returns_all_Groups()
        {
            var dtos = new[] { new GroupDTO { Id = 1 }, new GroupDTO { Id = 2 } };
            var content = new ObjectContent<IEnumerable<GroupDTO>>(dtos, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };
            var repository = new GroupRepository(httpClient);
            var returnedGroups = await repository.ReadAsync();

            Assert.Equal(dtos, returnedGroups);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/group");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task ReadAsync_returns_empty_collection()
        {
            var emptyCollection = new List<GroupDTO>();
            var content = new ObjectContent<IEnumerable<GroupDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRepository(httpClient);
            var returnedGroups = await repository.ReadAsync();

            Assert.Empty(returnedGroups);
        }

        [Fact]
        public async Task DeleteAsync_given_existing_group_id_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRepository(client);

            var result = await repository.DeleteAsync(3);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/group/{3}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_id_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRepository(client);

            var result = await repository.DeleteAsync(3);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/group/{3}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task UpdateAsync_given_existing_dto_returns_no_content()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRepository(client);

            var dummygroup = new GroupDTO();

            var result = await repository.UpdateAsync(dummygroup);
            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/group/{0}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Put
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
    
        [Fact]
        public async Task UpdateAsync_given_non_existing_dto_returns_not_found()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRepository(client);

            var dummygroup = new GroupDTO();

            var result = await repository.UpdateAsync(dummygroup);
            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/group/{0}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Put
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task CreateAsync_returns_group()
        {
            var dummyGroup = new GroupDTO { Id = 1 };
            var content = new ObjectContent<GroupDTO>(dummyGroup, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.Created, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRepository(client);
            
            var returnedGroup = await repository.CreateAsync(dummyGroup);

            Assert.Equal(dummyGroup, returnedGroup);
            
            var expectedUriToBeCalled = new Uri($"{baseAddress}api/group");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
    }
}

    






