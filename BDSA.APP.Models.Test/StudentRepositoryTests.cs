﻿using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.Shared;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using BDSA.GroupITApp.Models;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class StudentRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private MockHandler mockHandler = new MockHandler();
        
        [Fact]
        public async Task FindAsync_given_existing_student_id_sends_get_to_existing_uri()
        {
            var dummyStudent = new StudentDTO { Id = 1, Name = "test" };
            var content = new ObjectContent<StudentDTO>(dummyStudent, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentRepository(httpClient);

            var returnedStudent = await repository.FindAsync(dummyStudent.Id);

            Assert.Equal(dummyStudent, returnedStudent);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/student/{dummyStudent.Id}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }
        
        [Fact]
        public async Task FindAsync_given_non_existing_student_id_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentRepository(httpClient);

            var result = await repository.FindAsync(4);

            Assert.Null(result);
        }

        [Fact]
        public async Task FindStudentsByNameAsync_given_existing_student_name_sends_get_to_existing_uri()
        {
            var dummyStudent = new[] { new StudentDTO { Id = 1, Name = "test" } };
            var content = new ObjectContent<IEnumerable<StudentDTO>>(dummyStudent, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentRepository(httpClient);

            var result = await repository.FindStudentsByNameAsync(dummyStudent[0].Name);

            Assert.Equal(dummyStudent, result);
            
            var expectedUriToBeCalled = new Uri($"{baseAddress}api/student/name/{dummyStudent[0].Name}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindStudentsByNameAsync_given_non_existing_student_name_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentRepository(httpClient);

            var result = await repository.FindStudentsByNameAsync("Student");

            Assert.Null(result);
        }

        [Fact]
        public async Task FindIdByMailAsync_given_existing_student_mail_sends_get_to_existing_uri()
        {
            var dummyStudent = new StudentDTO { Id = 1, Name = "test", Mail = "test@itu.dk" };
            var content = new ObjectContent<int>(dummyStudent.Id, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentRepository(httpClient);

            var result = await repository.FindIdByMailAsync(dummyStudent.Mail);

            Assert.Equal(dummyStudent.Id, result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/student/mail/{dummyStudent.Mail}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindIdByMailAsync_given_non_existing_student_mail_returns_0_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentRepository(httpClient);

            var result = await repository.FindIdByMailAsync("student@itu.dk");

            Assert.Equal(0, result);
        }

        [Fact]
        public async Task DeleteAsync_given_existing_student_id_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentRepository(client);

            var result = await repository.DeleteAsync(3);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/student/{3}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_student_id_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentRepository(client);

            var result = await repository.DeleteAsync(3);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/student/{3}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task UpdateAsync_given_existing_student_dto_returns_no_content()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentRepository(client);

            var dummyStudent = new StudentDTO();

            var result = await repository.UpdateAsync(dummyStudent);
            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/student/{0}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Put
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task UpdateAsync_given_non_existing_student_dto_returns_not_found()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentRepository(client);

            var dummyStudent = new StudentDTO();

            var result = await repository.UpdateAsync(dummyStudent);
            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/student/{0}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Put
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task CreateAsync_returns_student()
        {
            var dummyStudent = new StudentDTO { Id = 1 };
            var content = new ObjectContent<StudentDTO>(dummyStudent, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.Created, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentRepository(client);

            var returnedStudent = await repository.CreateAsync(dummyStudent);
            Assert.Equal(dummyStudent, returnedStudent);
            
            var expectedUriToBeCalled = new Uri($"{baseAddress}api/student");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
    }
}
