﻿using BDSA.Shared;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class TeacherCourseRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly MockHandler mockHandler = new MockHandler();
        
        [Fact]
        public async Task FindAsync_given_existing_TeacherCourse_ids_sends_get_to_existing_uri()
        {
            var dto = new TeacherCourseDTO {  TeacherId = 1, CourseId = 2 };
            var content = new ObjectContent<TeacherCourseDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherCourseRepository(httpClient);

            var teacherCourse = await repository.FindAsync(dto.TeacherId, dto.CourseId);

            Assert.Equal(dto, teacherCourse);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/teacherCourse/{dto.TeacherId}/{dto.CourseId}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_TecherCourse_ids_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherCourseRepository(httpClient);

            var result = await repository.FindAsync(3, 4);

            Assert.Null(result);
        }

        [Fact]
        public async Task FindAllTeacherCoursesAsync_sends_get_to_correct_uri()
        {
            var dtos = new[]
            {
                 new TeacherCourseDTO{TeacherId = 1, CourseId = 2},
                 new TeacherCourseDTO{TeacherId = 1, CourseId = 3}
            };

            var content = new ObjectContent<IEnumerable<TeacherCourseDTO>>(dtos, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherCourseRepository(httpClient);

            var teacherCourses = await repository.FindAllTeacherCoursesAsync(1);

            Assert.Equal(dtos, teacherCourses);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/teacherCourse/{1}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Exactly(1),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAllTeacherCoursesAsync_given_non_existing_teacher_course_id_returns_empty_collection()
        {
            var emptyCollection = new List<TeacherCourseDTO>();
            var content = new ObjectContent<IEnumerable<TeacherCourseDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherCourseRepository(httpClient);
            var result = await repository.FindAllTeacherCoursesAsync(1);

            Assert.Empty(result);
        }

        [Fact]
        public async Task DeleteAsync_given_existing_teacher_course_ids_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherCourseRepository(client);

            var result = await repository.DeleteAsync(3, 1);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/teacherCourse/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_teacher_course_ids_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherCourseRepository(client);

            var result = await repository.DeleteAsync(3, 1);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/teacherCourse/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task CreateAsync_returns_teacherCourse()
        {
            var dto = new TeacherCourseDTO { TeacherId = 1, CourseId = 2 };
            var content = new ObjectContent<TeacherCourseDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherCourseRepository(client);

            var teacherCourse = await repository.CreateAsync(dto);
            Assert.Equal(dto, teacherCourse);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/teacherCourse");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
    }
}
