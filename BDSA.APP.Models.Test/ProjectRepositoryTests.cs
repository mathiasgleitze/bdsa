﻿using BDSA.Shared;
using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class ProjectRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly string api = "api/project";
        private readonly MockHandler mockHandler = new MockHandler();
        
        [Fact]
        public async Task FindAsync_given_existing_id_sends_get()
        {
            var dto = new ProjectDTO { Id = 1 };
            var content = new ObjectContent<ProjectDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectRepository(client);

            var result = await repository.FindAsync(dto.Id);

            Assert.Equal(dto, result);

            var expectedUri = new Uri($"{baseAddress}{api}/{dto.Id}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(request =>
                    request.Method == HttpMethod.Get && request.RequestUri == expectedUri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_id_returns_null()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectRepository(client);

            var result = await repository.FindAsync(1);

            Assert.Null(result);
        }

        [Fact]
        public async Task CreateAsync_given_dto_sends_post()
        {
            var dto = new ProjectDTO { CourseId = 1, MaxSize = 6 };
            var content = new ObjectContent<ProjectDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectRepository(client);

            var result = await repository.CreateAsync(dto);

            Assert.Equal(dto, result);

            var expectedUri = new Uri($"{baseAddress}{api}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(request =>
                    request.Method == HttpMethod.Post && request.RequestUri == expectedUri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task DeleteAsync_given_existing_id_sends_delete()
        {
            var dto = new ProjectDTO { Id = 1 };
            var content = new ObjectContent<ProjectDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectRepository(client);

            var result = await repository.DeleteAsync(dto.Id);

            Assert.True(result);

            var expectedUri = new Uri($"{baseAddress}{api}/{dto.Id}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(request =>
                    request.Method == HttpMethod.Delete && request.RequestUri == expectedUri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_id_returns_false()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectRepository(client);

            var result = await repository.DeleteAsync(1);

            Assert.False(result);
        }
        
        [Fact]
        public async Task UpdateAsync_given_existing_dto_sends_update()
        {
            var dto = new ProjectDTO { Id = 1, CourseId = 1, MaxSize = 2 };
            var content = new ObjectContent<ProjectDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectRepository(client);

            var result = await repository.UpdateAsync(dto);

            Assert.True(result);

            var expectedUri = new Uri($"{baseAddress}{api}/{dto.Id}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(request =>
                    request.Method == HttpMethod.Put && request.RequestUri == expectedUri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task UpdateAsync_given_non_existing_dto_returns_false()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectRepository(client);

            var result = await repository.UpdateAsync(new ProjectDTO());

            Assert.False(result);
        }
    }
}
