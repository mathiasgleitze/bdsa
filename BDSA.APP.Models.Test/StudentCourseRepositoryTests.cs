using BDSA.Shared;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.GroupITApp.Models;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class StudentCourseRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly MockHandler mockHandler = new MockHandler();
        
        [Fact]
        public async Task FindAsync_given_existing_studentCourse_ids_sends_get_to_existing_uri()
        {
            var dto = new StudentCourseDTO { StudentId = 1, CourseId = 2 };
            var content = new ObjectContent<StudentCourseDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentCourseRepository(httpClient);

            var studentCourse = await repository.FindAsync(dto.StudentId, dto.CourseId);

            Assert.Equal(dto, studentCourse);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentCourse/{dto.StudentId}/{dto.CourseId}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_StudentCourse_ids_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty)); // Expect 404 (NotFound)

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentCourseRepository(httpClient);

            var result = await repository.FindAsync(3, 4);

            Assert.Null(result);
        }

        [Fact]
        public async Task ReadStudentsAsync_sends_get_to_correct_uri()
        {
            var dummyStudents = new[]
            {
                new StudentDTO{Id=1},
                new StudentDTO{Id=2}
            };


            var content = new ObjectContent<IEnumerable<StudentDTO>>((dummyStudents), new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentCourseRepository(httpClient);

            var returnedStudents = await repository.ReadStudentsAsync(1);

            var result = returnedStudents.FirstOrDefault();

            Assert.Equal(dummyStudents[0].Id, result.Id);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentcourse/student/{1}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task ReadStudentsAsync_given_non_existing_student_id_returns_empty_collection()
        {
            var emptyCollection = new List<StudentCourseDTO>();
            var content = new ObjectContent<IEnumerable<StudentCourseDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentCourseRepository(httpClient);
            var result = await repository.ReadStudentsAsync(1);

            Assert.Empty(result);
        }

        [Fact]
        public async Task ReadCoursesAsync_sends_get_to_correct_uri()
        {
            var dummyCourses = new[]
            {
                 new CourseDTO{Id = 2},
                 new CourseDTO{Id = 3}
            };



            var content = new ObjectContent<IEnumerable<CourseDTO>>((dummyCourses), new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentCourseRepository(httpClient);

            var returnedCourses = await repository.ReadCoursesAsync(1);

            var result = returnedCourses.FirstOrDefault();

            Assert.Equal(dummyCourses[0].Id, result.Id);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentcourse/course/{1}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task ReadCoursesAsync_given_non_existing_course_id_returns_empty_collection()
        {
            var emptyCollection = new List<StudentCourseDTO>();
            var content = new ObjectContent<IEnumerable<StudentCourseDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentCourseRepository(httpClient);
            var result = await repository.ReadCoursesAsync(1);

            Assert.Empty(result);
        }

        [Fact]
        public async Task DeleteAsync_given_existing_student_course_ids_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentCourseRepository(client);

            var result = await repository.DeleteAsync(3, 1);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentCourse/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_student_course_ids_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentCourseRepository(client);

            var result = await repository.DeleteAsync(3, 1);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentCourse/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task CreateAsync_returns_studentCourse()
        {
            var dto = new StudentCourseDTO { StudentId = 1, CourseId = 2 };
            var content = new ObjectContent<StudentCourseDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.Created, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentCourseRepository(client);

            var studentCourse = await repository.CreateAsync(dto);
            Assert.Equal(dto, studentCourse);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentCourse");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
    }
}
