﻿using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.Shared;
using System.Net.Http.Formatting;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class TeacherRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly MockHandler mockHandler = new MockHandler();
        
        [Fact]
        public async Task FindAsync_given_existing_teacher_id_sends_get_to_existing_uri()
        {
            var dummyTeacher = new TeacherDTO { Id = 1, Name = "test" };
            var content = new ObjectContent<TeacherDTO>(dummyTeacher, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherRepository(httpClient);

            var returnedTeacher = await repository.FindAsync(dummyTeacher.Id);

            Assert.Equal(dummyTeacher, returnedTeacher);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/teacher/{dummyTeacher.Id}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_teacher_id_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherRepository(httpClient);

            var result = await repository.FindAsync(4);

            Assert.Null(result);
        }

        [Fact]
        public async Task DeleteAsync_given_existing_teacher_id_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherRepository(client);

            var result = await repository.DeleteAsync(3);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/teacher/{3}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAsync_given_non_exsisting_teacher_id_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherRepository(client);

            var result = await repository.DeleteAsync(3);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/teacher/{3}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task UpdateAsync_given_existing_teacher_id_returns_no_content()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherRepository(client);

            var dummyTeacher = new TeacherDTO();

            var result = await repository.UpdateAsync(dummyTeacher);
            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/teacher/{0}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Put
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task UpdateAsync_given_non_existing_teacher_id_returns_not_found()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherRepository(client);

            var dummyTeacher = new TeacherDTO();

            var result = await repository.UpdateAsync(dummyTeacher);
            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/teacher/{0}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Put
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
        [Fact]
        public async Task CreateAsync_returns_teacher()
        {
            var dummyTeacher = new TeacherDTO { Id = 1 };
            var content = new ObjectContent<TeacherDTO>(dummyTeacher, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.Created, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new TeacherRepository(client);



            var returnedTeacher = await repository.CreateAsync(dummyTeacher);
            Assert.Equal(dummyTeacher, returnedTeacher);


            var expectedUriToBeCalled = new Uri($"{baseAddress}api/teacher");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
    }
}