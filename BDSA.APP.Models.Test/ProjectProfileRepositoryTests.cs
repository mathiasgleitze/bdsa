﻿using BDSA.Shared;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class ProjectProfileRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly MockHandler mockHandler = new MockHandler();
        
        [Fact]
        public async Task FindAsync_given_existing_ProjectProfile_ids_sends_get_to_existing_uri()
        {
            var dto = new ProjectProfileDTO {  StudentId = 1, ProjectId = 2 };
            var content = new ObjectContent<ProjectProfileDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectProfileRepository(httpClient);

            var projectProfile = await repository.FindAsync(dto.StudentId, dto.ProjectId);

            Assert.Equal(dto, projectProfile);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/projectProfile/{dto.StudentId}/{dto.ProjectId}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_ProjectProfile_ids_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectProfileRepository(httpClient);

            var result = await repository.FindAsync(3, 4);

            Assert.Null(result);
        }

        [Fact]
        public async Task FindAllProjectProfilesAsync_given_existing_project_profile_id_sends_get_to_correct_uri()
        {
            var dtos = new[]
            {
                 new ProjectProfileDTO { StudentId = 1, ProjectId = 2},
                 new ProjectProfileDTO { StudentId = 1, ProjectId = 3}
            };

            var content = new ObjectContent<IEnumerable<ProjectProfileDTO>>(dtos, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectProfileRepository(httpClient);

            var projectProfiles = await repository.FindAllProjectProfilesAsync(1);

            Assert.Equal(dtos, projectProfiles);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/projectProfile/{1}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAllProjectProfilesAsync_given_non_existing_project_profile_ids_returns_empty_collection()
        {
            var emptyCollection = new List<ProjectProfileDTO>();
            var content = new ObjectContent<IEnumerable<ProjectProfileDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectProfileRepository(httpClient);
            var projectProfiles = await repository.FindAllProjectProfilesAsync(1);

            Assert.Empty(projectProfiles);
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_project_profile_ids_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectProfileRepository(client);

            var result = await repository.DeleteAsync(3, 1);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/projectProfile/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_project_profile_ids_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectProfileRepository(client);

            var result = await repository.DeleteAsync(3, 1);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/projectProfile/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task CreateAsync_returns_projectProfile()
        {
            var dto = new ProjectProfileDTO { StudentId = 1, ProjectId = 2 };
            var content = new ObjectContent<ProjectProfileDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.Created, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectProfileRepository(client);

            var projectProfile = await repository.CreateAsync(dto);
            Assert.Equal(dto, projectProfile);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/projectProfile");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
        [Fact]
        public async Task UpdateAsync_given_existing_student_dto_returns_no_content()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectProfileRepository(client);

            var dummyProfile = new ProjectProfileDTO() {StudentId=1,ProjectId=2};

            var result = await repository.UpdateAsync(dummyProfile);
            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/projectprofile/{1}/{2}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Put
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task UpdateAsync_given_non_existing_dto_returns_false()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new ProjectProfileRepository(client);

            var result = await repository.UpdateAsync(new ProjectProfileDTO());

            Assert.False(result);
        }
    }
}
