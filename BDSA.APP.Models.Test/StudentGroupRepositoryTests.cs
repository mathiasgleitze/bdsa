using BDSA.Shared;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;
using Xunit;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class StudentGroupRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly MockHandler mockHandler = new MockHandler();
        
        [Fact]
        public async Task FindAsync_given_existing_studentGroup_ids_sends_get_to_existing_uri()
        {
            var dto = new StudentGroupDTO { StudentId = 1, GroupId = 2 };
            var content = new ObjectContent<StudentGroupDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentGroupRepository(httpClient);

            var studentGroup = await repository.FindAsync(dto.StudentId, dto.GroupId);

            Assert.Equal(dto, studentGroup);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentgroup/{dto.StudentId}/{dto.GroupId}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_student_group_ids_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentGroupRepository(httpClient);

            var result = await repository.FindAsync(3, 4);

            Assert.Null(result);
        }

        [Fact]
        public async Task ReadStudentsAsync_sends_get_to_correct_uri()
        {
            var dummyStudents = new[]
            {
                new StudentDTO{Id=1},
                new StudentDTO{Id=2}
            };


            var content = new ObjectContent<IEnumerable<StudentDTO>>((dummyStudents), new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentGroupRepository(httpClient);

            var returnedStudents = await repository.ReadStudentsAsync(1);
           
            var result = returnedStudents.FirstOrDefault();

            Assert.Equal(dummyStudents[0].Id, result.Id);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentgroup/student/{1}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task ReadStudentsAsync_given_non_existing_student_id_returns_empty_collection()
        {
            var emptyCollection = new List<StudentGroupDTO>();
            var content = new ObjectContent<IEnumerable<StudentGroupDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentGroupRepository(httpClient);
            var result = await repository.ReadStudentsAsync(1);

            Assert.Empty(result);
        }

        [Fact]
        public async Task ReadGroupsAsync_sends_get_to_correct_uri()
        {
            var dummyGroups = new[]
            {
                 new GroupDTO{Id = 2},
                 new GroupDTO{Id = 3}
            };
            


            var content = new ObjectContent<IEnumerable<GroupDTO>>((dummyGroups), new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentGroupRepository(httpClient);

            var returnedGroups = await repository.ReadGroupsAsync(1);

            var result = returnedGroups.FirstOrDefault();

            Assert.Equal(dummyGroups[0].Id, result.Id);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentgroup/group/{1}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task ReadGroupsAsync_given_non_existing_group_id_returns_empty_collection()
        {
            var emptyCollection = new List<StudentGroupDTO>();
            var content = new ObjectContent<IEnumerable<StudentGroupDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentGroupRepository(httpClient);
            var result = await repository.ReadGroupsAsync(1);

            Assert.Empty(result);
        }

        [Fact]
        public async Task DeleteAsync_given_existing_student_group_ids_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentGroupRepository(client);

            var result = await repository.DeleteAsync(3, 1);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentgroup/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_student_group_ids_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentGroupRepository(client);

            var result = await repository.DeleteAsync(3, 1);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentgroup/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task CreateAsync_returns_studentGroup()
        {
            var dto = new StudentGroupDTO { StudentId = 1, GroupId = 2 };
            var content = new ObjectContent<StudentGroupDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.Created, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new StudentGroupRepository(client);

            var studentGroup = await repository.CreateAsync(dto);
            Assert.Equal(dto, studentGroup);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/studentgroup");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }
    }
}
