﻿using BDSA.Shared;
using Moq;
using Moq.Protected;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class MessageRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly string api = "api/message";
        private readonly MockHandler mockHandler = new MockHandler();
        
        [Fact]
        public async Task FindAsync_given_existing_message_identifiers_sends_get()
        {
            var dto = new MessageDTO { ChatId = 1, StudentId = 1, Time = new DateTime(1000) };
            var content = new ObjectContent<MessageDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MessageRepository(client);

            var result = await repository.FindAsync(dto.ChatId, dto.Time);

            Assert.Equal(dto, result);

            var expectedUri = new Uri($"{baseAddress}{api}/{dto.ChatId}/{dto.Time}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(request => 
                    request.Method == HttpMethod.Get && request.RequestUri == expectedUri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_message_identifiers_returns_null()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MessageRepository(client);

            var result = await repository.FindAsync(1, new DateTime(1000));

            Assert.Null(result);
        }

        [Fact]
        public async Task CreateAsync_given_dto_sends_post()
        {
            var dto = new MessageDTO { ChatId = 1, StudentId = 1, Time = new DateTime(1000) };
            var content = new ObjectContent<MessageDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.Created, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MessageRepository(client);

            var result = await repository.CreateAsync(dto);

            Assert.Equal(dto, result);

            var expectedUri = new Uri($"{baseAddress}{api}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(request =>
                    request.Method == HttpMethod.Post && request.RequestUri == expectedUri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }
        
        [Fact]
        public async Task DeleteAsync_given_existing_message_identifiers_sends_delete()
        {
            var dto = new MessageDTO { ChatId = 1, StudentId = 1, Time = new DateTime(1000) };
            var content = new ObjectContent<MessageDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MessageRepository(client);

            var result = await repository.DeleteAsync(dto.ChatId, dto.Time);

            Assert.True(result);

            var expectedUri = new Uri($"{baseAddress}{api}/{dto.ChatId}/{dto.Time}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(request =>
                    request.Method == HttpMethod.Delete && request.RequestUri == expectedUri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_message_identifiers_returns_false()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MessageRepository(client);

            var result = await repository.DeleteAsync(1, new DateTime(1000));

            Assert.False(result);
        }

        [Fact]
        public async Task DeleteMessagesAsync_given_existing_chat_id_sends_delete()
        {
            var dto = new MessageDTO { ChatId = 1, StudentId = 1, Time = new DateTime(1000) };
            var content = new ObjectContent<MessageDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MessageRepository(client);

            var result = await repository.DeleteMessagesAsync(dto.ChatId);

            Assert.True(result);

            var expectedUri = new Uri($"{baseAddress}{api}/{dto.ChatId}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(request =>
                    request.Method == HttpMethod.Delete && request.RequestUri == expectedUri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task DeleteMessagesAsync_given_non_existing_chat_id_returns_false()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MessageRepository(client);

            var result = await repository.DeleteMessagesAsync(1);

            Assert.False(result);
        }

        [Fact]
        public async Task ReadAsync_given_existing_chat_id_sends_get()
        {
            var dtos = new[] { new MessageDTO { ChatId = 1, StudentId = 1, Time = new DateTime(1000) } };
            var content = new ObjectContent<IEnumerable<MessageDTO>>(dtos, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MessageRepository(client);

            var result = await repository.ReadAsync(dtos[0].ChatId);

            Assert.Equal(dtos, result);

            var expectedUri = new Uri($"{baseAddress}{api}/{dtos[0].ChatId}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(request =>
                    request.Method == HttpMethod.Get && request.RequestUri == expectedUri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task ReadAsync_given_non_existing_chat_id_returns_null()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MessageRepository(client);

            var result = await repository.ReadAsync(1);

            Assert.Null(result);
        }

        [Fact]
        public async Task UpdateAsync_given_existing_dto_sends_update()
        {
            var dto = new MessageDTO { ChatId = 1, StudentId = 1, Time = new DateTime(1000) };
            var content = new ObjectContent<MessageDTO>(dto, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MessageRepository(client);

            var result = await repository.UpdateAsync(dto);

            Assert.True(result);

            var expectedUri = new Uri($"{baseAddress}{api}/{dto.ChatId}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(request =>
                    request.Method == HttpMethod.Put && request.RequestUri == expectedUri
                ),
                ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task UpdateAsync_given_non_existing_dto_returns_false()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new MessageRepository(client);

            var result = await repository.UpdateAsync(new MessageDTO());

            Assert.False(result);
        }
    }
}
