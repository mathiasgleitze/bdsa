﻿using Moq;
using Moq.Protected;
using System;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using BDSA.Shared;
using System.Net.Http.Formatting;
using System.Collections.Generic;
using BDSA.GroupITApp.Models.Repositories;

namespace BDSA.GroupITApp.Models.Test
{
    public class GroupRequestRepositoryTests
    {
        private readonly Uri baseAddress = new Uri("https://localhost:44394/");
        private readonly MockHandler mockHandler = new MockHandler();
        
        [Fact]
        public async Task FindAsync_given_existing_request_ids_sends_get_to_existing_uri()
        {
            var dummyGroupRequest = new GroupRequestDTO { FromGroup=1,ToGroup=2 };
            var content = new ObjectContent<GroupRequestDTO>(dummyGroupRequest, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRequestRepository(httpClient);

            var returnedGroupRequest = await repository.FindAsync(dummyGroupRequest.FromGroup,dummyGroupRequest.ToGroup);

            Assert.Equal(dummyGroupRequest, returnedGroupRequest);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/grouprequest/{dummyGroupRequest.FromGroup}/{dummyGroupRequest.ToGroup}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task FindAsync_given_non_existing_request_ids_returns_null_after_send_of_get_to_non_existing_uri()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRequestRepository(httpClient);

            var result = await repository.FindAsync(3,4);

            Assert.Null(result);
        }

        [Fact]
        public async Task ReadFromAsync_given_existing_request_ids_sends_get_to_correct_uri()
        {
            var dummyFromGroupRequestes = new[]
            {
                 new GroupRequestDTO{FromGroup=1,ToGroup=2},
                 new GroupRequestDTO{FromGroup=1,ToGroup=3}
            };

            var content = new ObjectContent<IEnumerable<GroupRequestDTO>>(dummyFromGroupRequestes, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRequestRepository(httpClient);

            var returnedGroupRequestes =await  repository.ReadFromAsync(1);
            

            Assert.Equal(dummyFromGroupRequestes, returnedGroupRequestes);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/grouprequest/from/{1}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task ReadFromAsync_given_non_existing_request_ids_returns_empty_collection()
        {
            var emptyCollection = new List<GroupRequestDTO>();
            var content = new ObjectContent<IEnumerable<GroupRequestDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRequestRepository(httpClient);
            var groupRequests = await repository.ReadFromAsync(1);

            Assert.Empty(groupRequests);
        }

        [Fact]
        public async Task ReadToAsync_given_existing_request_ids_sends_get_to_correct_uri()
        {
            var dummyToGroupRequestes = new[]
            {
                 new GroupRequestDTO{FromGroup=1,ToGroup=2},
                 new GroupRequestDTO{FromGroup=1,ToGroup=3}
            };
            

            var content = new ObjectContent<IEnumerable<GroupRequestDTO>>(dummyToGroupRequestes, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.OK, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRequestRepository(httpClient);

            var returnedGroupRequestes = await repository.ReadToAsync(1);

            Assert.Equal(dummyToGroupRequestes, returnedGroupRequestes);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/grouprequest/to/{1}");

            handler.Protected().Verify(
               "SendAsync",
               Times.Once(),
               ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Get && req.RequestUri == expectedUriToBeCalled
               ),
               ItExpr.IsAny<CancellationToken>()
            );
        }

        [Fact]
        public async Task ReadToAsync_given_non_existing_request_ids_returns_empty_collection()
        {
            var emptyCollection = new List<GroupRequestDTO>();
            var content = new ObjectContent<IEnumerable<GroupRequestDTO>>(emptyCollection, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, content);

            var httpClient = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRequestRepository(httpClient);
            var groupRequests = await repository.ReadToAsync(1);

            Assert.Empty(groupRequests);
        }

        [Fact]
        public async Task DeleteAsync_given_existing_request_ids_sends_delete()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRequestRepository(client);

            var result = await repository.DeleteAsync(3,1);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/grouprequest/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task DeleteAsync_given_non_existing_request_ids_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRequestRepository(client);

            var result = await repository.DeleteAsync(3,1);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/grouprequest/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Delete
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task CreateAsync_returns_request()
        {
            var dummyGroupRequest = new GroupRequestDTO { FromGroup = 1,ToGroup = 2 };
            var content = new ObjectContent<GroupRequestDTO>(dummyGroupRequest, new JsonMediaTypeFormatter());
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.Created, content);

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRequestRepository(client);
            
            var returnedGroup = await repository.CreateAsync(dummyGroupRequest);
            Assert.Equal(dummyGroupRequest, returnedGroup);
            
            var expectedUriToBeCalled = new Uri($"{baseAddress}api/grouprequest");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Post
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task UpdateAsync_given_existing_request_ids_sends_update()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NoContent, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRequestRepository(client);

            var result = await repository.UpdateAsync(3, 1);

            Assert.True(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/grouprequest/update/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Put
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

        [Fact]
        public async Task UpdateAsync_given_non_existing_request_ids_returns_notfound()
        {
            var handler = mockHandler.CreateMockHandler(HttpStatusCode.NotFound, new StringContent(string.Empty));

            var client = new HttpClient(handler.Object)
            {
                BaseAddress = baseAddress
            };

            var repository = new GroupRequestRepository(client);

            var result = await repository.UpdateAsync(3, 1);

            Assert.False(result);

            var expectedUriToBeCalled = new Uri($"{baseAddress}api/grouprequest/update/{3}/{1}");

            handler.Protected().Verify(
                "SendAsync",
                Times.Once(),
                ItExpr.Is<HttpRequestMessage>(req =>
                  req.Method == HttpMethod.Put
                  && req.RequestUri == expectedUriToBeCalled
                ),
                   ItExpr.IsAny<CancellationToken>()
                );
        }

    }
}
