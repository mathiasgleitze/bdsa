﻿using Moq;
using Moq.Protected;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

namespace BDSA.GroupITApp.Models.Test
{
    public class MockHandler
    {
        public Mock<HttpMessageHandler> CreateMockHandler(HttpStatusCode expectedStatusCode, HttpContent content)
        {
            var handler = new Mock<HttpMessageHandler>();

            handler
                .Protected() // Setup the protected method to mock
                .Setup<Task<HttpResponseMessage>>(
                      "SendAsync",
                      ItExpr.IsAny<HttpRequestMessage>(),
                      ItExpr.IsAny<CancellationToken>()
                  )
                  .ReturnsAsync(new HttpResponseMessage()
                  {
                      StatusCode = expectedStatusCode,
                      Content = content
                  })
                  .Verifiable();

            return handler;
        }
    }
}
